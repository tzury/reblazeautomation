import pytest
from pages.acl_page import *
from locators.acl_page_locators import *
import time


@pytest.mark.usefixtures("test_setup", "acl_login", "acl_site")
@pytest.mark.all_modules
@pytest.mark.acl_tests
class TestAcl:

    def test_can_navigate_to_acl_page(self, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        assert "profiles" in acl_page.get_current_url()

    def test_acl_page_components_exist(self, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        assert acl_page.is_element_present(*AclMainLocators.LEFT_PROFILES_LIST)
        assert acl_page.is_element_present(*AclMainLocators.PROFILES_BTN)
        assert acl_page.is_element_present(*AclMainLocators.WAF_POLICIES_BTN)
        assert acl_page.is_element_present(*AclMainLocators.ACL_POLICIES_BTN)
        assert acl_page.is_element_present(*AclMainLocators.CUSTOM_SIGNATURE_BTN)
        assert acl_page.is_element_present(*AclMainLocators.ACL_PAGE_LABEL)
        assert acl_page.is_element_present(*AclMainLocators.CREATE_NEW_BTN)

    def test_profiles_page(self, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        assert acl_page.is_element_present(*AclMainLocators.LEFT_PROFILES_LIST)
        assert acl_page.is_element_present(By.LINK_TEXT, "ACL Default")
        assert acl_page.is_element_present(By.LINK_TEXT, "Save")
        assert acl_page.is_element_present(By.LINK_TEXT, "Duplicate")
        assert acl_page.is_element_present(By.LINK_TEXT, "Delete")
        assert acl_page.is_element_present(By.LINK_TEXT, "ACL Deny All")
        assert acl_page.is_element_present(*AclMainLocators.LINK_POLICY_SELECT)
        assert acl_page.is_element_present(*AclMainLocators.ACL_DEFAULT_INPUT)
        assert acl_page.is_element_present(*AclMainLocators.ADD_POLICY_BTN)

    def test_acl_policies_page(self, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        assert not acl_page.is_element_present(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.find_element(*AclMainLocators.ACL_POLICIES_BTN).click()
        assert acl_page.is_element_present(*AclMainLocators.LEFT_PROFILES_LIST)
        assert acl_page.is_element_present(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        assert acl_page.is_element_present(*AclMainLocators.NEW_RULE_OPERATION_SELECT)
        assert acl_page.is_element_present(*AclMainLocators.NEW_RULE_MATCH_SELECT)
        assert acl_page.is_element_present(By.LINK_TEXT, "ACL Deny All")
        assert acl_page.is_element_present(By.LINK_TEXT, "Save")
        assert acl_page.is_element_present(By.LINK_TEXT, "Duplicate")
        assert acl_page.is_element_present(By.LINK_TEXT, "Delete")
        assert acl_page.is_element_present(*AclMainLocators.ADD_POLICY_BTN)

    def test_can_duplicate_profile(self, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        text_before = acl_page.get_policies_table_text()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_save()
        acl_page.refresh_page()
        acl_page.find_element(By.LINK_TEXT, new_name).click()
        text_after = acl_page.get_policies_table_text()
        assert text_after == text_before
        assert acl_page.find_element(*AclMainLocators.ACL_DEFAULT_INPUT).get_attribute("value") == new_name

    def test_can_delete_profile(self, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_save()
        acl_page.refresh_page()
        acl_page.find_element(By.LINK_TEXT, new_name).click()
        acl_page.press_delete()
        acl_page.refresh_page()
        assert not acl_page.is_element_present(By.LINK_TEXT, new_name)

    @pytest.mark.parametrize("acl_policy", ["Deny Tor"])
    def test_can_add_policy_to_profile_by_name(self, acl_policy, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_profiles_btn()
        time.sleep(0.4)
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        text_before = acl_page.get_policies_table_text()
        assert acl_policy not in text_before
        acl_page.select_acl_policy_site_by_name(acl_policy)
        acl_page.press_add()
        acl_page.press_save()
        text_after = acl_page.get_policies_table_text()
        assert acl_policy in text_after
        assert text_after[len(text_after) - 1] == acl_policy

    @pytest.mark.parametrize("acl_policy", ["Deny Tor"])
    def test_can_delete_policy_from_profile(self, acl_policy, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        text_before = acl_page.get_policies_table_text()
        assert acl_policy not in text_before
        acl_page.select_acl_policy_site_by_name(acl_policy)
        acl_page.press_add()
        acl_page.press_save()
        text_after = acl_page.get_policies_table_text()
        assert acl_policy in text_after
        assert text_after[len(text_after) - 1] == acl_policy
        acl_page.remove_acl_policy_by_name(acl_policy)
        text_after_delete = acl_page.get_policies_table_text()
        assert acl_policy not in text_after_delete
        acl_page.press_save()
        acl_page.refresh_page()
        acl_page.click_profile_by_name(new_name)
        text_after_delete = acl_page.get_policies_table_text()
        assert acl_policy not in text_after_delete

    def test_can_add_profile_to_path_and_publish(self, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_save()
        acl_page.attach_profile_to_url_by_path_and_profile_name("acl.test", "Test", new_name)
        acl_page.refresh_page()
        acl_page.switch_to_frame()
        acl_page.expand_path_settings_by_name("Test")
        acl_page.check_site_attached_acl_profile(new_name)

    @pytest.mark.parametrize("acl_policy", ["ACL Deny All"])
    def test_add_deny_all_check_all_requests_denied(self, acl_policy, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.select_acl_policy_site_by_name(acl_policy)
        acl_page.press_add()
        acl_page.press_save()
        assert acl_page.get_response_status_code(self.data["acl_base_url"], "/test") == 200
        acl_page.attach_profile_to_url_by_path_and_profile_name("acl.test", "Test", new_name)
        acl_page.publish_planet(21)
        assert acl_page.get_response_status_code(self.data["acl_base_url"], "/test") == 403

    def test_can_duplicate_policy(self, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        text_before = acl_page.get_policies_table_text()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.press_save()
        acl_page.refresh_page()
        acl_page.click_acl_policies_btn()
        acl_page.find_element(By.LINK_TEXT, new_name).click()
        text_after = acl_page.get_policies_table_text()
        assert text_after == text_before
        assert acl_page.find_element(*AclMainLocators.ADD_POLICY_NAME_INPUT).get_attribute("value") == new_name

    def test_can_delete_policy(self, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.press_save()
        acl_page.refresh_page()
        acl_page.click_acl_policies_btn()
        acl_page.find_element(By.LINK_TEXT, new_name).click()
        acl_page.press_delete()
        acl_page.refresh_page()
        assert not acl_page.is_element_present(By.LINK_TEXT, new_name)

    def test_can_add_new_rule_to_new_policy(self, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.press_save()
        acl_page.select_new_acl_rule_operation_by_name("Deny")
        acl_page.select_new_acl_rule_match_by_name("Country")
        acl_page.fill_acl_new_rule_value("Israel")
        text_before = acl_page.get_policy_rules_list_table_text()
        assert "Israel" not in text_before
        acl_page.press_add()
        acl_page.press_save()
        acl_page.refresh_page()
        acl_page.click_acl_policies_btn()
        acl_page.find_element(By.LINK_TEXT, new_name).click()
        text_after = acl_page.get_policy_rules_list_table_text()
        assert "Israel" in text_after[0]

    def test_can_delete_new_rule_from_new_policy(self, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.find_element(By.LINK_TEXT, "Test Rules").click()
        acl_page.select_new_acl_rule_operation_by_name("Deny")
        acl_page.select_new_acl_rule_match_by_name("Country")
        acl_page.fill_acl_new_rule_value("China")
        text_before = acl_page.get_policy_rules_list_table_text()
        assert "China" not in text_before
        acl_page.press_add()
        acl_page.press_save()
        acl_page.refresh_page()
        acl_page.click_acl_policies_btn()
        acl_page.find_element(By.LINK_TEXT, "Test Rules").click()
        text_after = acl_page.get_policy_rules_list_table_text()
        assert "China" in text_after[0]
        acl_page.remove_acl_rule_by_name("China")
        acl_page.press_save()
        acl_page.refresh_page()
        acl_page.click_acl_policies_btn()
        acl_page.find_element(By.LINK_TEXT, "Test Rules").click()
        assert "China" not in text_before

    @pytest.mark.parametrize("match, value",
                             [("Country", "Israel"), ("Company", "Hot-Net internet services Ltd. | AS12849"),
                              ("Company", "Cellcom Fixed Line Communication L.P. | AS1680")])
    def test_can_publish_new_rule_in_a_profile(self, match, value, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.press_save()
        acl_page.select_new_acl_rule_operation_by_name("Deny")
        acl_page.select_new_acl_rule_match_by_name(match)
        if value == "Israel":
            country, code = acl_page.get_my_country_name_and_code()
            value = country
        acl_page.fill_acl_new_rule_value(value)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.select_acl_policy_site_by_name(new_name)
        acl_page.press_add()
        acl_page.press_save()
        assert acl_page.get_response_status_code(self.data["acl_base_url"], "/test") == 200
        acl_page.attach_profile_to_url_by_path_and_profile_name("acl.test", "Test", new_prof_name)
        acl_page.publish_planet(20)
        assert acl_page.get_response_status_code(self.data["acl_base_url"], "/test") == 403

    @pytest.mark.parametrize("policy, operation, match, value", [("ACL Deny All", "Allow", "Country", "Israel")])
    def test_can_deny_with_allow(self, policy, operation, match, value, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl1.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.find_element(By.LINK_TEXT, policy).click()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.press_save()
        acl_page.select_new_acl_rule_operation_by_name(operation)
        acl_page.select_new_acl_rule_match_by_name(match)
        if value == "Israel":
            country, code = acl_page.get_my_country_name_and_code()
            value = country
        acl_page.fill_acl_new_rule_value(value)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.select_acl_policy_site_by_name(new_name)
        acl_page.press_add()
        acl_page.press_save()
        assert acl_page.get_response_status_code(self.data["acl1.test"], "/test") == 200
        acl_page.attach_profile_to_url_by_path_and_profile_name("acl1.test", "Test", new_prof_name)
        acl_page.publish_planet(20)
        assert acl_page.get_response_status_code(self.data["acl1.test"], "/test") == 200

    # make sure the to adapt ip for your machine
    @pytest.mark.parametrize("policy, operation, match",
                             [("ACL Deny All", "Deny", "IP Address")])
    def test_can_deny_ip(self, policy, operation, match, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        value = acl_page.get_my_public_ip()
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.remove_acl_rule_by_name("0.0.0.0/0")
        acl_page.press_save()
        acl_page.select_new_acl_rule_operation_by_name(operation)
        acl_page.select_new_acl_rule_match_by_name(match)
        acl_page.fill_acl_new_rule_value(value)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.delete_all_policies_in_a_profile()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.select_acl_policy_site_by_name(new_name)
        acl_page.press_add()
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test",
                                                             {"srcip": value}) == 200
        acl_page.attach_profile_to_url_by_path_and_profile_name("acl.test", "Test", new_prof_name)
        acl_page.publish_planet(20)
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test",
                                                             {"srcip": value}) == 403

    # make sure the to adapt ip for your machine
    @pytest.mark.parametrize("policy, operation, match",
                             [("ACL Deny All", "Allow", "IP Address")])
    def test_can_allow_ip(self, policy, operation, match, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        value = acl_page.get_my_public_ip()
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.press_save()
        acl_page.select_new_acl_rule_operation_by_name(operation)
        acl_page.select_new_acl_rule_match_by_name(match)
        acl_page.fill_acl_new_rule_value(value)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.select_acl_policy_site_by_name(new_name)
        acl_page.press_add()
        acl_page.press_save()
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test",
                                                             {"srcip": value}) == 200
        acl_page.attach_profile_to_url_by_path_and_profile_name("acl.test", "Test", new_prof_name)
        acl_page.publish_planet(20)
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test",
                                                             {"srcip": value}) == 200

    @pytest.mark.parametrize("policy, operation, match, value",
                             [("ACL Deny All", "Bypass", "Custom Signature", "Test CS [84363464]"),
                              ("ACL Deny All", "Allow", "Custom Signature", "Test CS [84363464]"),
                              ("ACL Deny All", "Bypass", "Custom Signature", "Test Country [23717500]"),
                              ("ACL Deny All", "Allow", "Custom Signature", "Test Country [23717500]"),
                              ("ACL Deny All", "Bypass", "Custom Signature", "Test Method Get [70539400]"),
                              ("ACL Deny All", "Allow", "Custom Signature", "Test Method Get [70539400]"),
                              ("ACL Deny All", "Bypass", "Custom Signature", "Test Request Protocol [45397215]"),
                              ("ACL Deny All", "Allow", "Custom Signature", "Test Request Protocol [45397215]")
                              ])
    def test_can_allow_bypass_custom_signature(self, policy, operation, match, value, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        country, code = acl_page.get_my_country_name_and_code()
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.press_save()
        acl_page.select_new_acl_rule_operation_by_name(operation)
        acl_page.select_new_acl_rule_match_by_name(match)
        if value == "Test CS [84363464]" and country != "Israel":
            value = "Test CS_Instance [17042830]"
        if value == "Test Country [23717500]" and country != "Israel":
            value = "Test Country_Instance [01732380]"
        acl_page.fill_acl_new_rule_value(value)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.select_acl_policy_site_by_name(new_name)
        acl_page.press_add()
        acl_page.press_save()
        assert acl_page.get_response_status_code(self.data["acl_base_url"], "/test") == 200
        acl_page.attach_profile_to_url_by_path_and_profile_name("acl.test", "Test", new_prof_name)
        acl_page.publish_planet(20)
        assert acl_page.get_response_status_code(self.data["acl_base_url"], "/test") == 200

    @pytest.mark.parametrize("policy, operation, match, value",
                             [("ACL Deny All", "Deny", "Custom Signature", "Test Country [23717500]"),
                              ("ACL Deny All", "Deny", "Custom Signature", "Test Method Get [70539400]"),
                              ("ACL Deny All", "Deny", "Custom Signature", "Test Request Protocol [45397215]")])
    def test_can_deny_custom_signature_country_method_protocol(self, policy, operation, match, value, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.remove_acl_rule_by_name("0.0.0.0/0")
        acl_page.press_save()
        acl_page.select_new_acl_rule_operation_by_name(operation)
        acl_page.select_new_acl_rule_match_by_name(match)
        if value == "Test Country [23717500]":
            value = "Test Country_Instance [01732380]"
        acl_page.fill_acl_new_rule_value(value)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.select_acl_policy_site_by_name(new_name)
        acl_page.press_add()
        acl_page.remove_acl_policy_by_name("ACL Deny All")
        acl_page.press_save()
        assert acl_page.get_response_status_code(self.data["acl_base_url"], "/test") == 200
        acl_page.attach_profile_to_url_by_path_and_profile_name("acl.test", "Test", new_prof_name)
        acl_page.publish_planet(20)
        assert acl_page.get_response_status_code(self.data["acl_base_url"], "/test") == 403

    @pytest.mark.parametrize("policy, operation, match, value, param, name",
                             [("ACL Deny All", "Bypass", "Custom Signature", "Test Cookies [58486960]", "cookies",
                               "cookie"),
                              ("ACL Deny All", "Allow", "Custom Signature", "Test Cookies [58486960]", "cookies",
                               "cookie"),
                              ("ACL Deny All", "Bypass", "Custom Signature", "Test Headers [42572892]", "headers",
                               "header"),
                              ("ACL Deny All", "Allow", "Custom Signature", "Test Headers [42572892]", "headers",
                               "header")])
    def test_can_allow_bypass_custom_signature_headers_and_cookies(self, policy, operation, match, value, param, name,
                                                                   acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.remove_acl_rule_by_name("0.0.0.0/0")
        acl_page.press_save()
        acl_page.select_new_acl_rule_operation_by_name(operation)
        acl_page.select_new_acl_rule_match_by_name(match)
        acl_page.fill_acl_new_rule_value(value)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.select_acl_policy_site_by_name(new_name)
        acl_page.press_add()
        acl_page.press_save()
        params = {param: {"some_param": name}}
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test", params) == 200
        acl_page.attach_profile_to_url_by_path_and_profile_name("acl.test", "Test", new_prof_name)
        acl_page.publish_planet(20)
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test", params) == 200

    @pytest.mark.parametrize("policy, operation, match, value, param, val",
                             [("ACL Deny All", "Deny", "Custom Signature", "Test Cookies [58486960]", "cookies",
                               "cookie"),
                              ("ACL Deny All", "Deny", "Custom Signature", "Test Headers [42572892]", "headers",
                               "header"),
                              ])
    def test_can_deny_custom_signature_headers_cookies_by_value(self, policy, operation, match, value, param, val,
                                                                acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.press_save()
        acl_page.select_new_acl_rule_operation_by_name(operation)
        acl_page.select_new_acl_rule_match_by_name(match)
        acl_page.fill_acl_new_rule_value(value)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.select_acl_policy_site_by_name(new_name)
        acl_page.press_add()
        acl_page.remove_acl_policy_by_name("ACL Deny All")
        acl_page.press_save()
        params = {param: {"some_param": val}}
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test", params) == 200
        acl_page.attach_profile_to_url_by_path_and_profile_name("acl.test", "Test", new_prof_name)
        acl_page.publish_planet(20)
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test", params) == 403

    @pytest.mark.parametrize("policy, operation, match, value, method",
                             [("ACL Deny All", "Deny", "Custom Signature", "Test Method Put Post Delete [24602852]",
                               "PUT"),
                              ("ACL Deny All", "Deny", "Custom Signature", "Test Method Put Post Delete [24602852]",
                               "POST"),
                              ("ACL Deny All", "Deny", "Custom Signature", "Test Method Put Post Delete [24602852]",
                               "DELETE")
                              ])
    def test_can_deny_custom_signature_methods(self, policy, operation, match, value, method, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.press_save()
        acl_page.select_new_acl_rule_operation_by_name(operation)
        acl_page.select_new_acl_rule_match_by_name(match)
        acl_page.fill_acl_new_rule_value(value)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.select_acl_policy_site_by_name(new_name)
        acl_page.press_add()
        acl_page.remove_acl_policy_by_name("ACL Deny All")
        acl_page.press_save()
        params = {"method": method}
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test", params) == 200
        acl_page.attach_profile_to_url_by_path_and_profile_name("acl.test", "Test", new_prof_name)
        acl_page.publish_planet(20)
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test", params) == 403

    @pytest.mark.parametrize("policy, operation, match, value, method",
                             [("ACL Deny All", "Allow", "Custom Signature", "Test Method Put Post Delete [24602852]",
                               "PUT"),
                              ("ACL Deny All", "Allow", "Custom Signature", "Test Method Put Post Delete [24602852]",
                               "POST"),
                              ("ACL Deny All", "Allow", "Custom Signature", "Test Method Put Post Delete [24602852]",
                               "DELETE")
                              ])
    def test_can_allow_custom_signature_methods(self, policy, operation, match, value, method, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.press_save()
        acl_page.select_new_acl_rule_operation_by_name(operation)
        acl_page.select_new_acl_rule_match_by_name(match)
        acl_page.fill_acl_new_rule_value(value)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.select_acl_policy_site_by_name(new_name)
        acl_page.press_add()
        acl_page.press_save()
        params = {"method": method}
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test", params) == 200
        acl_page.attach_profile_to_url_by_path_and_profile_name("acl.test", "Test", new_prof_name)
        acl_page.publish_planet(20)
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test", params) == 200

    @pytest.mark.parametrize("policy, operation, match, value, param, name",
                             [("ACL Deny All", "Deny", "Custom Signature", "Test Cookie Name [78961064]", "cookies",
                               "cookie_name"),
                              ("ACL Deny All", "Deny", "Custom Signature", "Test Header Name [14163640]", "headers",
                               "header_name"),
                              ])
    def test_can_deny_custom_signature_headers_cookies_by_name(self, policy, operation, match, value, param, name,
                                                               acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.press_save()
        acl_page.select_new_acl_rule_operation_by_name(operation)
        acl_page.select_new_acl_rule_match_by_name(match)
        acl_page.fill_acl_new_rule_value(value)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.select_acl_policy_site_by_name(new_name)
        acl_page.press_add()
        acl_page.remove_acl_policy_by_name("ACL Deny All")
        acl_page.press_save()
        params = {param: {name: "some_value"}}
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test", params) == 200
        acl_page.attach_profile_to_url_by_path_and_profile_name("acl.test", "Test", new_prof_name)
        acl_page.publish_planet(20)
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test", params) == 403

    @pytest.mark.parametrize("policy, operation, match, value, param, name",
                             [("ACL Deny All", "Allow", "Custom Signature", "Test Cookie Name [78961064]", "cookies",
                               "cookie_name"),
                              ("ACL Deny All", "Allow", "Custom Signature", "Test Header Name [14163640]", "headers",
                               "header_name"),
                              ])
    def test_can_deny_custom_signature_headers_cookies_by_name(self, policy, operation, match, value, param, name,
                                                               acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append("acl.test")
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.press_save()
        acl_page.select_new_acl_rule_operation_by_name(operation)
        acl_page.select_new_acl_rule_match_by_name(match)
        acl_page.fill_acl_new_rule_value(value)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.select_acl_policy_site_by_name(new_name)
        acl_page.press_add()
        acl_page.press_save()
        params = {param: {name: "some_value"}}
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test", params) == 200
        acl_page.attach_profile_to_url_by_path_and_profile_name("acl.test", "Test", new_prof_name)
        acl_page.publish_planet(20)
        assert acl_page.get_response_status_code_with_params(self.data["acl_base_url"], "/test", params) == 200

    @pytest.mark.parametrize("site, url, operation, match, value, tag, ip",
                             [("acltor.test", "acltor_base_url", "Deny", "Tag", "tor", "Tor", "185.220.101.11"),
                              ("aclbot.test", "aclbot_base_url", "Deny", "Tag", "cloud", "Cloud", "27614")])
    def test_can_deny_tor_cloud(self, site, url, operation, match, value, tag, ip, acl_site):
        acl_page = AclPage(self.driver)
        acl_page.navigate_to_tab("Profiles")
        acl_site["path"].append(site)
        acl_page.wait_for_acl_page_to_load()
        acl_page.click_acl_policies_btn()
        acl_page.click_acl_deny_all_option()
        acl_page.press_duplicate()
        acl_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        acl_page.press_save()
        acl_page.select_new_acl_rule_operation_by_name(operation)
        acl_page.select_new_acl_rule_match_by_name(match)
        acl_page.fill_acl_new_rule_value(value)
        acl_page.press_add()
        acl_page.press_save()
        acl_page.click_acl_profiles_btn()
        acl_page.click_acl_default_option()
        acl_page.press_duplicate()
        acl_page.delete_all_policies_in_a_profile()
        acl_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + acl_page.generate_random_string(10)
        acl_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        acl_page.select_acl_policy_site_by_name(new_name)
        acl_page.press_add()
        acl_page.press_save()
        response = acl_page.query(self.data[url], path="/test", headers={tag: ip})
        assert response.status_code == 200
        acl_page.attach_profile_to_url_by_path_and_profile_name(site, "Test", new_prof_name)
        acl_page.publish_planet(20)
        response = acl_page.query(self.data[url], path="/test", headers={tag: ip})
        assert response.status_code == 403



