import pytest
from pages.dashboard_page import DashboardPage
from locators.dashboard_locators import DashboardLocators
from locators.dashboard_locators import BottomNavLocators
from locators.left_nav_locators import LeftNavLocators
from pages.login_page import *
import time


@pytest.mark.usefixtures("test_setup", "dashboard_login")
@pytest.mark.all_modules
@pytest.mark.dashboard_tests
class TestDashboard:
    def test_dashboard_page_elements_exist_after_login(self):
        dashboard = DashboardPage(self.driver)
        dashboard.send_requests_for_dashboard(1, self.data["dashboard_base_url"], "rate_limit", 1)
        dashboard.send_requests_for_dashboard(1, self.data["dashboard_base_url"], "acl-block", 1)
        dashboard.wait_for_dashboard_to_load()
        dashboard.explicitly_wait_for_element_to_be_clickable(10, *DashboardLocators.LATENCY)
        assert dashboard.find_element(*DashboardLocators.LATENCY).is_displayed()
        assert dashboard.find_element(*DashboardLocators.BANDWIDTH).is_displayed()
        assert dashboard.find_element(*DashboardLocators.TOP_SITES).is_displayed()
        assert dashboard.find_element(*DashboardLocators.REQUEST_COUNT).is_displayed()
        assert dashboard.find_element(*DashboardLocators.RESPONSE_STATUS).is_displayed()
        assert dashboard.find_element(*DashboardLocators.BOTTOM_NAV_TABS).is_displayed()
        assert dashboard.find_element(*DashboardLocators.PASSED_VS_BLOCKED).is_displayed()
        assert dashboard.find_element(*DashboardLocators.RIGHT_NAV_TABS).is_displayed()
        assert dashboard.find_element(*DashboardLocators.TOTAL_BANDWIDTH).is_displayed()
        assert dashboard.find_element(*DashboardLocators.UNIQUE_SESSIONS).is_displayed()

    def test_dashboard_bottom_tab_buttons_exist(self):
        dashboard = DashboardPage(self.driver)
        dashboard.wait_for_dashboard_to_load()
        dashboard.explicitly_wait_for_element_to_be_clickable(10, *BottomNavLocators.APPLICATIONS_BTN)
        assert dashboard.find_element(*BottomNavLocators.APPLICATIONS_BTN).is_displayed()
        assert dashboard.find_element(*BottomNavLocators.TOTAL_TIME_BTN).is_displayed()
        assert dashboard.find_element(*BottomNavLocators.REBLAZE_TIME_BTN).is_displayed()
        assert dashboard.find_element(*BottomNavLocators.TARGETS_BTN).is_displayed()
        assert dashboard.find_element(*BottomNavLocators.BROWSERS_BTN).is_displayed()
        assert dashboard.find_element(*BottomNavLocators.COMPANIES_BTN).is_displayed()
        assert dashboard.find_element(*BottomNavLocators.COUNTRIES_BTN).is_displayed()
        assert dashboard.find_element(*BottomNavLocators.REFERERS_BTN).is_displayed()
        assert dashboard.find_element(*BottomNavLocators.SESSIONS_BTN).is_displayed()
        assert dashboard.find_element(*BottomNavLocators.SIGNATURES_BTN).is_displayed()
        assert dashboard.find_element(*BottomNavLocators.SOURCES_BTN).is_displayed()
        assert dashboard.find_element(*BottomNavLocators.COUNTRIES_BTN).is_displayed()
        assert dashboard.find_element(*BottomNavLocators.ORIGIN_TIME_BTN).is_displayed()

    def test_daterange_default_value(self):
        dashboard = DashboardPage(self.driver)
        dashboard.wait_for_dashboard_to_load()
        screen_time_from, screen_time_to = dashboard.get_daterange_text()
        real_time_from, real_time_to = dashboard.format_dashboard_date_to_string(30)
        assert screen_time_from == str(real_time_from)
        assert screen_time_to == str(real_time_to)

    def test_passed_blocked_chart_shows_correct_range(self):
        dashboard = DashboardPage(self.driver)
        dashboard.wait_for_dashboard_to_load()
        chart_times = dashboard.get_all_passed_blocked_dates()
        assert dashboard.validate_time_from_in_range(chart_times[0], 11)
        assert dashboard.validate_time_to_in_range([chart_times[len(chart_times) - 7]][0], 11)

    def test_response_status_chart_shows_correct_range(self):
        dashboard = DashboardPage(self.driver)
        dashboard.wait_for_dashboard_to_load()
        chart_times = dashboard.get_all_response_status_dates()
        assert dashboard.validate_time_from_in_range(chart_times[0], 11)
        assert dashboard.validate_time_to_in_range([chart_times[len(chart_times) - 7]][0], 11)

    def test_total_bandwidth_chart_shows_correct_range(self):
        dashboard = DashboardPage(self.driver)
        dashboard.wait_for_dashboard_to_load()
        chart_times = dashboard.get_all_total_bandwidth_dates()
        assert dashboard.validate_time_from_in_range(chart_times[0], 11)
        assert dashboard.validate_time_to_in_range([chart_times[len(chart_times) - 7]][0], 11)

    def test_total_unique_sessions_shows_correct_range(self):
        dashboard = DashboardPage(self.driver)
        dashboard.wait_for_dashboard_to_load()
        chart_times = dashboard.get_all_unique_sessions_dates()
        assert dashboard.validate_time_from_in_range(chart_times[0], 11)
        assert dashboard.validate_time_to_in_range([chart_times[len(chart_times) - 7]][0], 11)

    def test_total_latency_shows_correct_range(self):
        dashboard = DashboardPage(self.driver)
        dashboard.wait_for_dashboard_to_load()
        chart_times = dashboard.get_all_latency_dates()
        assert dashboard.validate_time_from_in_range(chart_times[0], 30)
        assert dashboard.validate_time_to_in_range([chart_times[-3]][0], 30)

    def test_total_bandwidth_shows_correct_range(self):
        dashboard = DashboardPage(self.driver)
        dashboard.wait_for_dashboard_to_load()
        chart_times = dashboard.get_all_bandwidth_dates()
        assert dashboard.validate_time_from_in_range(chart_times[0], 25)
        assert dashboard.validate_time_to_in_range([chart_times[len(chart_times) - 7]][0], 25)

    def test_total_request_count_shows_correct_range(self):
        dashboard = DashboardPage(self.driver)
        dashboard.wait_for_dashboard_to_load()
        chart_times = dashboard.get_all_request_count_dates()
        assert dashboard.validate_time_from_in_range(chart_times[0], 25)
        assert dashboard.validate_time_to_in_range([chart_times[len(chart_times) - 7]][0], 25)

    #
    def test_passed_chart_displays_correct_data(self):
        dashboard = DashboardPage(self.driver)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        all_hits, blocked = dashboard.move_cursor_half_hour_chart()
        displayed = dashboard.get_displayed_hits_blocked_text()
        assert f"All Hits ({str(all_hits)})" == displayed[0].text.replace(',', '')
        assert f"Blocks ({str(blocked)})" == displayed[1].text

    """FLAKY"""

    # def test_passed_chart_displays_correct_data_for_last_hour(self):
    #     dashboard = DashboardPage(self.driver)
    #     dashboard.navigate_to_tab("Dashboard")
    #     dashboard.wait_for_dashboard_to_load()
    #     dashboard.click_datarange()
    #     dashboard.find_element(By.XPATH, "//li[contains(text(), 'Last Hour')]").click()
    #     time.sleep(8)
    #     all_hits, blocked = dashboard.move_cursor_hour_chart()
    #     displayed = dashboard.get_displayed_hits_blocked_text()
    #     assert f"All Hits ({str(all_hits)})" == displayed[0].text.replace(',', '')
    #     assert f"Blocks ({str(blocked)})" == displayed[1].text

    def test_status_chart_displays_correct_data(self):
        dashboard = DashboardPage(self.driver)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        passed_hits, blocked_hits = dashboard.move_status_cursor_half_hour_chart()
        displayed = dashboard.get_displayed_status_hits_text()
        assert f"200 ({str(passed_hits)})" == displayed[0].text.replace(',', '')
        assert f"403 ({str(blocked_hits)})" == displayed[1].text

    """FLAKY"""

    # def test_status_chart_displays_correct_data_for_last_hour(self):
    #     dashboard = DashboardPage(self.driver)
    #     dashboard.navigate_to_tab("Dashboard")
    #     dashboard.wait_for_dashboard_to_load()
    #     dashboard.click_datarange()
    #     dashboard.find_element(By.XPATH, "//li[contains(text(), 'Last Hour')]").click()
    #     time.sleep(6)
    #     passed_hits, blocked_hits = dashboard.move_status_cursor_hour_chart()
    #     displayed = dashboard.get_displayed_status_hits_text()
    #     assert f"200 ({str(passed_hits)})" == displayed[0].text.replace(',', '')
    #     assert f"403 ({str(blocked_hits)})" == displayed[1].text

    def test_hits_number_correct_in_all_tabs(self):
        dashboard = DashboardPage(self.driver)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        displayed = dashboard.get_displayed_hits_blocked_text()
        formatted_hits = int(
            displayed[0].text.replace(',', '').replace('All Hits ', "").replace("(", "").replace(")", ""))
        bottom_hits = dashboard.get_total_hits_in_application_tab()
        assert formatted_hits == bottom_hits

    def test_blocks_number_correct_in_all_tabs(self):
        dashboard = DashboardPage(self.driver)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        displayed = dashboard.get_displayed_hits_blocked_text()
        formatted_hits = int(
            displayed[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")", ""))
        bottom_hits = dashboard.get_total_blocks_in_application_tab()
        assert formatted_hits == bottom_hits

    def test_rate_limit_blocked_requests_displayed(self):
        dashboard = DashboardPage(self.driver)
        current_displayed = dashboard.get_displayed_hits_blocked_text()
        formatted_hits = int(
            current_displayed[0].text.replace(',', '').replace('All Hits ', "").replace("(", "").replace(")", ""))
        dashboard.send_requests_for_dashboard(70, self.data["dashboard_base_url"], "rate_limit", 150)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        all_hits, blocked = dashboard.move_cursor_half_hour_chart()
        displayed = dashboard.get_displayed_hits_blocked_text()
        formatted_hits_after = int(
            displayed[0].text.replace(',', '').replace('All Hits ', "").replace("(", "").replace(")", ""))
        assert formatted_hits + 40 <= formatted_hits_after
        assert blocked >= 35
        assert int(displayed[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")", "")) >= 35
        passed_hits, blocked_hits, rl_blocks = dashboard.move_status_cursor_half_hour_chart_with_rl_blocks()
        status_displayed = dashboard.get_displayed_status_hits_text()
        assert rl_blocks >= 35
        assert int(
            status_displayed[2].text.replace(',', '').replace('503 ', "").replace("(", "").replace(")", "")) >= 35

    def test_acl_blocked_requests_displayed(self):
        dashboard = DashboardPage(self.driver)
        dashboard.send_requests_for_dashboard(40, self.data["dashboard_base_url"], "acl-block", 100)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        all_hits, blocked = dashboard.move_cursor_half_hour_chart()
        displayed = dashboard.get_displayed_hits_blocked_text()
        assert blocked >= 30
        assert int(displayed[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")", "")) >= 30
        passed_hits, blocked_hits, rl_blocks = dashboard.move_status_cursor_half_hour_chart_with_rl_blocks()
        status_displayed = dashboard.get_displayed_status_hits_text()
        assert rl_blocks >= 30
        assert int(
            status_displayed[1].text.replace(',', '').replace('403 ', "").replace("(", "").replace(")", "")) >= 30

    def test_bottom_countries_filter(self):
        dashboard = DashboardPage(self.driver)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.find_element(*BottomNavLocators.COUNTRIES_TAB).is_displayed() is False
        dashboard.click_countries_tab()
        assert dashboard.find_element(*BottomNavLocators.COUNTRIES_TAB).is_displayed()
        displayed_blocks = dashboard.get_displayed_hits_blocked_text()
        formatted_blocks = int(
            displayed_blocks[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")", ""))
        countries_tab_total = dashboard.get_total_blocks_in_countries_tab()
        assert formatted_blocks == countries_tab_total

    def test_bottom_countries_filter_navigates_to_view_log(self):
        dashboard = DashboardPage(self.driver)
        dashboard.send_requests_for_dashboard(2, self.data["dashboard_base_url"], "test", 20)
        dashboard.navigate_to_tab("Dashboard")
        country, code = dashboard.get_my_country_name_and_code()
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.find_element(*BottomNavLocators.COUNTRIES_TAB).is_displayed() is False
        dashboard.click_countries_tab()
        assert dashboard.find_element(*BottomNavLocators.COUNTRIES_TAB).is_displayed()
        dashboard.find_element(By.PARTIAL_LINK_TEXT, country).click()
        time.sleep(4)
        assert dashboard.get_current_url() == f"https://rbzauto2.dev.app.reblaze.io/zoomlog?#by=country&with={country}"
        assert dashboard.find_element(By.ID, "dyn-filter-exp").get_attribute("value") == f"country: {country}"

    def test_bottom_sources_filter(self):
        dashboard = DashboardPage(self.driver)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.find_element(*BottomNavLocators.SOURCES_TAB).is_displayed() is False
        dashboard.click_sources_tab()
        assert dashboard.find_element(*BottomNavLocators.SOURCES_TAB).is_displayed()
        displayed_blocks = dashboard.get_displayed_hits_blocked_text()
        formatted_blocks = int(
            displayed_blocks[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")", ""))
        sources_tab_total = dashboard.get_total_blocks_in_sources_tab()
        assert formatted_blocks == sources_tab_total

    def test_bottom_sources_filter_navigates_to_view_log(self):
        dashboard = DashboardPage(self.driver)
        dashboard.send_requests_for_dashboard(2, self.data["dashboard_base_url"], "test", 20)
        dashboard.navigate_to_tab("Dashboard")
        ip = dashboard.get_my_public_ip()
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.find_element(*BottomNavLocators.SOURCES_TAB).is_displayed() is False
        dashboard.click_sources_tab()
        assert dashboard.find_element(*BottomNavLocators.SOURCES_TAB).is_displayed()
        time.sleep(4)
        dashboard.find_element(By.PARTIAL_LINK_TEXT, ip).click()
        time.sleep(4)
        assert dashboard.get_current_url() == f"https://rbzauto2.dev.app.reblaze.io/zoomlog?#by=ip&with={ip}"
        assert dashboard.find_element(By.ID, "dyn-filter-exp").get_attribute("value") == f"ip: {ip}"

    def test_bottom_sessions_filter(self):
        dashboard = DashboardPage(self.driver)
        dashboard.send_requests_for_dashboard(2, self.data["dashboard_base_url"], "test", 20)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.find_element(*BottomNavLocators.SESSIONS_TAB).is_displayed() is False
        dashboard.click_sessions_tab()
        assert dashboard.find_element(*BottomNavLocators.SESSIONS_TAB).is_displayed()
        displayed_blocks = dashboard.get_displayed_hits_blocked_text()
        formatted_blocks = int(
            displayed_blocks[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")", ""))
        total, passed, blocked = dashboard.get_total_blocks_in_sessions_tab()
        assert blocked == formatted_blocks
        assert total == passed + blocked

    def test_bottom_targets_filter(self):
        dashboard = DashboardPage(self.driver)
        dashboard.send_requests_for_dashboard(2, self.data["dashboard_base_url"], "rate_limit", 20)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.find_element(*BottomNavLocators.TARGETS_TAB).is_displayed() is False
        dashboard.click_targets_tab()
        assert dashboard.find_element(*BottomNavLocators.TARGETS_TAB).is_displayed()
        first_target = dashboard.get_first_target_text().split("/")
        dashboard.find_element(By.PARTIAL_LINK_TEXT, f"{first_target[0]}/{first_target[1]}").click()
        assert first_target[0], first_target[1] in dashboard.get_current_url()
        assert f"host: {first_target[0]}", first_target[1] in dashboard.find_element(By.ID,
                                                                                     "dyn-filter-exp").get_attribute(
            "value")

    def test_bottom_signatures_filter(self):
        dashboard = DashboardPage(self.driver)
        dashboard.send_requests_for_dashboard(3, self.data["dashboard_base_url"], "rate_limit", 20)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.find_element(*BottomNavLocators.SIGNATURES_TAB).is_displayed() is False
        dashboard.click_signatures_tab()
        assert dashboard.find_element(*BottomNavLocators.SIGNATURES_TAB).is_displayed()
        dashboard.click_datarange()
        dashboard.fill_calendar_from_to_fields(3, 0)
        first_signature = dashboard.get_first_signatures_text()
        first_signature_split = first_signature.replace("-", " ").replace(",", " ").split()
        len_sig = len(first_signature_split)
        dashboard.find_element(By.PARTIAL_LINK_TEXT, first_signature).click()
        time.sleep(4)
        for i in range(len_sig):
            assert first_signature_split[i] in dashboard.get_current_url()
        assert f"reason: {first_signature}" in dashboard.find_element(By.ID, "dyn-filter-exp").get_attribute("value")

    def test_bottom_referers_filter(self):
        dashboard = DashboardPage(self.driver)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.find_element(*BottomNavLocators.REFERERS_TAB).is_displayed() is False
        dashboard.click_referers_tab()
        assert dashboard.find_element(*BottomNavLocators.REFERERS_TAB).is_displayed()
        displayed_blocks = dashboard.get_displayed_hits_blocked_text()
        formatted_blocks = int(
            displayed_blocks[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")", ""))
        total, passed, blocked = dashboard.get_total_blocks_in_referers_tab()
        assert blocked == formatted_blocks
        assert total == passed + blocked

    def test_bottom_browsers_filter(self):
        dashboard = DashboardPage(self.driver)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.find_element(*BottomNavLocators.BROWSERS_TAB).is_displayed() is False
        dashboard.click_browsers_tab()
        time.sleep(7)
        assert dashboard.find_element(*BottomNavLocators.BROWSERS_TAB).is_displayed()
        displayed_blocks = dashboard.get_displayed_hits_blocked_text()
        formatted_blocks = int(
            displayed_blocks[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")", ""))
        total, passed, blocked, names = dashboard.get_total_blocks_in_browsers_tab()
        assert blocked == formatted_blocks
        assert total == passed + blocked
        assert "python-requests/2.26.0" in names

    def test_bottom_browsers_filter_view_log(self):
        dashboard = DashboardPage(self.driver)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.find_element(*BottomNavLocators.BROWSERS_TAB).is_displayed() is False
        dashboard.click_browsers_tab()
        assert dashboard.find_element(*BottomNavLocators.BROWSERS_TAB).is_displayed()
        first_browser = dashboard.get_first_browsers_text()
        dashboard.find_element(By.PARTIAL_LINK_TEXT, first_browser).click()
        time.sleep(4)
        assert dashboard.get_current_url() == f"https://rbzauto2.dev.app.reblaze.io/zoomlog?#by=ua&with={first_browser}"
        assert f"ua: {first_browser}" == dashboard.find_element(By.ID, "dyn-filter-exp").get_attribute("value")

    def test_bottom_companies_filter(self):
        dashboard = DashboardPage(self.driver)
        dashboard.send_requests_for_dashboard(5, self.data["dashboard_base_url"], "rate_limit", 25)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        current_asn = dashboard.get_my_asn()
        if "Google" in current_asn:
            current_asn = current_asn.upper().replace("LLC", "").rstrip(" ")
        assert dashboard.find_element(*BottomNavLocators.COMPANIES_TAB).is_displayed() is False
        dashboard.click_companies_tab()
        time.sleep(7)
        assert dashboard.find_element(*BottomNavLocators.COMPANIES_TAB).is_displayed()
        displayed_blocks = dashboard.get_displayed_hits_blocked_text()
        formatted_blocks = int(
            displayed_blocks[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")", ""))
        total, passed, blocked, names = dashboard.get_total_blocks_in_companies_tab()
        assert blocked == formatted_blocks
        assert total == passed + blocked
        assert current_asn in names

    def test_bottom_companies_filter_view_log(self):
        dashboard = DashboardPage(self.driver)
        dashboard.send_requests_for_dashboard(5, self.data["dashboard_base_url"], "rate_limit", 40)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.find_element(*BottomNavLocators.COMPANIES_TAB).is_displayed() is False
        dashboard.click_companies_tab()
        assert dashboard.find_element(*BottomNavLocators.COMPANIES_TAB).is_displayed()
        dashboard.click_datarange()
        dashboard.fill_calendar_from_to_fields(2, 0)
        first_company = dashboard.get_first_companies_text()
        first_comp_split = first_company.split()
        dashboard.find_element(By.PARTIAL_LINK_TEXT, first_company).click()
        time.sleep(4)
        for i in range(len(first_comp_split)):
            assert first_comp_split[i] in dashboard.get_current_url()
        assert f"asn: {first_company}" == dashboard.find_element(By.ID, "dyn-filter-exp").get_attribute("value")

    def test_bottom_total_time_filter(self):
        dashboard = DashboardPage(self.driver)
        rand_path = dashboard.generate_random_string(7)
        dashboard.query(self.data["dashboard_base_url"], f"/acl-block{rand_path}")
        time.sleep(40)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.find_element(*BottomNavLocators.TOTAL_TIME_TAB).is_displayed() is False
        dashboard.click_total_time_tab()
        time.sleep(2)
        assert dashboard.find_element(*BottomNavLocators.TOTAL_TIME_TAB).is_displayed()
        dashboard.click_datarange()
        dashboard.fill_calendar_from_to_fields(2, 0)
        total, passed, blocked, names = dashboard.get_total_hits_in_total_time_tab()
        assert f"dashboard-test.rbzauto2.dev.rbzdns.com/acl-block{rand_path}" in names
        first_time = dashboard.get_first_total_time_text().split("/")
        dashboard.find_element(By.PARTIAL_LINK_TEXT, f"{first_time[0]}/{first_time[1]}").click()
        assert first_time[0], first_time[1] in dashboard.get_current_url()
        assert f"host: {first_time[0]}", first_time[1] in dashboard.find_element(By.ID,
                                                                                 "dyn-filter-exp").get_attribute(
            "value")

    def test_bottom_total_rbz_time_filter(self):
        dashboard = DashboardPage(self.driver)
        rand_path = dashboard.generate_random_string(7)
        dashboard.query(self.data["dashboard_base_url"], f"/acl-block{rand_path}")
        time.sleep(32)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.find_element(*BottomNavLocators.REBLAZE_TIME_TAB).is_displayed() is False
        dashboard.click_total_rbz_time_tab()
        time.sleep(7)
        assert dashboard.find_element(*BottomNavLocators.REBLAZE_TIME_TAB).is_displayed()
        dashboard.click_datarange()
        dashboard.fill_calendar_from_to_fields(2, 0)
        total, passed, blocked, names = dashboard.get_total_hits_in_total_rbz_time_tab()
        assert f"dashboard-test.rbzauto2.dev.rbzdns.com/acl-block{rand_path}" in names
        first_reblaze_time = dashboard.get_first_total_rbz_time_text().split("/")
        dashboard.find_element(By.PARTIAL_LINK_TEXT, f"{first_reblaze_time[0]}/{first_reblaze_time[1]}").click()
        assert first_reblaze_time[0], first_reblaze_time[1] in dashboard.get_current_url()
        assert f"host: {first_reblaze_time[0]}", first_reblaze_time[1] in dashboard.find_element(By.ID,
                                                                                                 "dyn-filter-exp").get_attribute(
            "value")

    def test_bottom_origin_time_filter(self):
        dashboard = DashboardPage(self.driver)
        rand_path = dashboard.generate_random_string(7)
        dashboard.query(self.data["dashboard_base_url"], f"/rate_limit{rand_path}")
        time.sleep(30)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.find_element(*BottomNavLocators.ORIGIN_TIME_TAB).is_displayed() is False
        dashboard.click_origin_time_tab()
        time.sleep(7)
        assert dashboard.find_element(*BottomNavLocators.ORIGIN_TIME_TAB).is_displayed()
        total, passed, blocked, names = dashboard.get_total_hits_in_origin_time_tab()
        assert f"dashboard-test.rbzauto2.dev.rbzdns.com/rate_limit{rand_path}" in names
        first_origin_time = dashboard.get_first_origin_time_text().split("/")
        dashboard.find_element(By.PARTIAL_LINK_TEXT, f"{first_origin_time[0]}/{first_origin_time[1]}").click()
        assert first_origin_time[0], first_origin_time[1] in dashboard.get_current_url()
        assert f"host: {first_origin_time[0]}", first_origin_time[1] in dashboard.find_element(By.ID,
                                                                                               "dyn-filter-exp").get_attribute(
            "value")

    def test_can_filter_by_blocked(self):
        dashboard = DashboardPage(self.driver)
        dashboard.send_requests_for_dashboard(2, self.data["dashboard_base_url"], "rate_limit", 5)
        dashboard.send_requests_for_dashboard(2, self.data["dashboard_base_url"], "acl-block", 5)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        time.sleep(2)
        dashboard.send_value_to_field_press_enter("is: blocked", *DashboardLocators.SEARCH_INPUT)
        time.sleep(10)
        blocked = dashboard.move_cursor_half_hour_blocked_chart()
        displayed_blocked = dashboard.get_displayed_hits_blocked_text()
        assert int(displayed_blocked[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")",
                                                                                                              "")) == blocked
        acl_bl, rl_bl = dashboard.move_status_cursor_only_blocked_half_hour_chart_with_rl_blocks()
        displayed_status_blocked = dashboard.get_displayed_status_hits_text()
        assert f"403 ({str(acl_bl)})" == displayed_status_blocked[0].text.replace(',', '')
        assert f"503 ({str(rl_bl)})" == displayed_status_blocked[1].text

    @pytest.mark.parametrize("param", ["is: 403", "reason: acl-ip"])
    def test_can_filter_by_acl_blocked_status(self, param):
        dashboard = DashboardPage(self.driver)
        dashboard.send_requests_for_dashboard(2, self.data["dashboard_base_url"], "acl-block", 10)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        time.sleep(2)
        dashboard.send_value_to_field_press_enter(param, *DashboardLocators.SEARCH_INPUT)
        time.sleep(10)
        blocked = dashboard.move_cursor_half_hour_blocked_chart()
        displayed_blocked = dashboard.get_displayed_hits_blocked_text()
        assert int(displayed_blocked[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")",
                                                                                                              "")) == blocked
        acl_bl = dashboard.move_status_cursor_only_blocked_half_hour_chart_with_rl_blocks_or_acl_blocks()
        displayed_status_blocked = dashboard.get_displayed_status_hits_text()
        assert f"403 ({str(acl_bl)})" == displayed_status_blocked[0].text.replace(',', '')

    @pytest.mark.parametrize("param", ["is: 503", "reason: block-for-dashboard-testing, generic"])
    def test_can_filter_by_rl_blocked_status(self, param):
        dashboard = DashboardPage(self.driver)
        dashboard.send_requests_for_dashboard(5, self.data["dashboard_base_url"], "rate_limit", 15)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        time.sleep(2)
        dashboard.send_value_to_field_press_enter(param, *DashboardLocators.SEARCH_INPUT)
        time.sleep(10)
        blocked = dashboard.move_cursor_half_hour_blocked_chart()
        displayed_blocked = dashboard.get_displayed_hits_blocked_text()
        assert int(displayed_blocked[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")",
                                                                                                              "")) == blocked
        rl_bl = dashboard.move_status_cursor_only_blocked_half_hour_chart_with_rl_blocks_or_acl_blocks()
        displayed_status_blocked = dashboard.get_displayed_status_hits_text()
        assert f"503 ({str(rl_bl)})" == displayed_status_blocked[0].text

    def test_can_filter_by_ip(self):
        dashboard = DashboardPage(self.driver)
        ip = dashboard.get_my_public_ip()
        dashboard.send_requests_for_dashboard(2, self.data["dashboard_base_url"], "rate_limit", 5)
        dashboard.send_requests_for_dashboard(2, self.data["dashboard_base_url"], "acl-block", 15)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        time.sleep(2)
        dashboard.send_value_to_field_press_enter(f"ip: {ip}", *DashboardLocators.SEARCH_INPUT)
        time.sleep(12)
        blocked = dashboard.move_cursor_half_hour_blocked_chart()
        displayed_blocked = dashboard.get_displayed_hits_blocked_text()
        assert int(displayed_blocked[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")",
                                                                                                              "")) == blocked
        hits, block, rl_bl = dashboard.move_status_cursor_half_hour_chart_with_rl_blocks()
        displayed_status_blocked = dashboard.get_displayed_status_hits_text()
        assert f"503 ({str(rl_bl)})" == displayed_status_blocked[2].text
        assert f"403 ({str(block)})" == displayed_status_blocked[1].text

    def test_can_filter_by_host(self):
        dashboard = DashboardPage(self.driver)
        dashboard.send_requests_for_dashboard(2, self.data["dashboard_base_url"], "rate_limit", 5)
        dashboard.send_requests_for_dashboard(2, self.data["dashboard_base_url"], "acl-block", 15)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        time.sleep(2)
        dashboard.send_value_to_field_press_enter(f"host: dashboard-test.rbzauto2.dev.rbzdns.com",
                                                  *DashboardLocators.SEARCH_INPUT)
        time.sleep(12)
        blocked = dashboard.move_cursor_half_hour_blocked_chart()
        displayed_blocked = dashboard.get_displayed_hits_blocked_text()
        assert int(displayed_blocked[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")",
                                                                                                              "")) == blocked
        hits, block, rl_bl = dashboard.move_status_cursor_half_hour_chart_with_rl_blocks()
        displayed_status_blocked = dashboard.get_displayed_status_hits_text()
        assert f"503 ({str(rl_bl)})" == displayed_status_blocked[2].text
        assert f"403 ({str(block)})" == displayed_status_blocked[1].text
        first_app_row = dashboard.get_first_application_text()
        assert self.data["dashboard_base_url"].replace("http://", "") in first_app_row
        dashboard.click_targets_tab()
        time.sleep(5)
        first_target_row = dashboard.get_first_target_text()
        assert self.data["dashboard_base_url"].replace("http://", "") in first_target_row

    def test_can_filter_by_url(self):
        dashboard = DashboardPage(self.driver)
        rand_path = dashboard.generate_random_string(7)
        for i in range(3):
            dashboard.query(self.data["dashboard_base_url"], f"/rate_limit{rand_path}")
        time.sleep(30)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        time.sleep(2)
        dashboard.send_value_to_field_press_enter(f"url: /rate_limit{rand_path}", *DashboardLocators.SEARCH_INPUT)
        time.sleep(10)
        blocked = dashboard.move_cursor_half_hour_blocked_chart()
        displayed_blocked = dashboard.get_displayed_hits_blocked_text()
        assert int(displayed_blocked[1].text.replace(',', '').replace('Blocks ', "").replace("(", "").replace(")",
                                                                                                                "")) == blocked == 2
        assert int(displayed_blocked[0].text.replace(',', '').replace('All Hits ', "").replace("(", "").replace(")",
                                                                                                               "")) == 3
        hits, rl_bl = dashboard.move_status_cursor_half_hour_chart()
        displayed_status_blocked = dashboard.get_displayed_status_hits_text()
        assert f"503 ({str(rl_bl)})" == displayed_status_blocked[1].text == "503 (2)"
        assert f"200 ({str(hits)})" == displayed_status_blocked[0].text == "200 (1)"
        dashboard.click_targets_tab()
        time.sleep(5)
        first_target_row = dashboard.get_first_target_text()
        assert f"/rate_limit{rand_path}" in first_target_row

    def test_can_filter_by_country(self):
        dashboard = DashboardPage(self.driver)
        rand_path = dashboard.generate_random_string(7)
        country, code = dashboard.get_my_country_name_and_code()
        for i in range(3):
            dashboard.query(self.data["dashboard_base_url"], f"/test{rand_path}")
        time.sleep(20)
        dashboard.navigate_to_tab("Dashboard")
        dashboard.wait_for_dashboard_to_load()
        time.sleep(2)
        dashboard.send_value_to_field_press_enter(f"country: {country}", *DashboardLocators.SEARCH_INPUT)
        time.sleep(10)
        dashboard.click_countries_tab()
        time.sleep(5)
        first_country_row = dashboard.get_first_country_text()
        assert country == first_country_row
