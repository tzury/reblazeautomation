import time
from selenium.webdriver.remote.webdriver import *
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import *
from locators.planet_overview_locators import *
from locators.view_log_locators import *
from locators.acl_page_locators import *
import pyotp
import random
import string
import requests
from bs4 import BeautifulSoup
from core.login_data_vars import *
import logging
import subprocess
import ipinfo
from requests import get
import datetime
from selenium.webdriver.support import expected_conditions

_ACL_KEY = get_login_data("acl", "acl_key")
_ACL_HOST = get_login_data("acl", "acl_host")
_RL_KEY = get_login_data("rl", "rl_key")
_RL_HOST = get_login_data("rl", "rl_host")
_TAG_KEY = get_login_data("tag", "tag_key")
_TAG_HOST = get_login_data("tag", "tag_host")
_SSL_KEY = get_login_data("ssl", "ssl_key")
_SSL_HOST = get_login_data("ssl", "ssl_host")


class BasePage:

    def __init__(self, driver: WebDriver = None):
        self.driver = driver

    def return_rate_limit_rules_dict(self):
        return self.call("XGET", "rl", suffix="/ratelimit/list")

    # query method that supports headers, cookies, arguments, different ip and methods
    def query(
            self, planet_domain, path="", suffix="", method="GET", headers=None, srcip=None, **kwargs
    ):
        # specifying a path helps spot tests easily in the access log
        if headers is None:
            headers = {}
        if srcip is not None:
            headers["X-Forwarded-For"] = srcip
        res = requests.request(
            method=method, url=planet_domain + path + suffix, headers=headers, **kwargs
        )
        return res

    # return request status code
    def get_response_status_code(self, url, path):
        response = self.query(f"{url}{path}")
        return response.status_code

    # allows getting status code when sending headers, cookies, params, srcip
    def get_response_status_code_with_params(self, url, path, params):
        response = self.query(f"{url}{path}", **params)
        return response.status_code

    @staticmethod
    def generate_random_string(num: int) -> str:
        return ''.join(random.choice(string.ascii_lowercase) for i in range(num))

    # generates otp for login
    @staticmethod
    def get_otp(module_otp):
        otp = pyotp.TOTP(module_otp).now()
        time.sleep(0.2)
        return otp

    # find element - throws exception in case element not found
    def find_element(self, *locator):
        return self.driver.find_element(*locator)

    # find elements - returns empty list in case no elements
    def find_elements(self, *locator):
        return self.driver.find_elements(*locator)

    # checks element present on page  - bool
    def is_element_present(self, *locator) -> bool:
        self.driver.implicitly_wait(3)
        try:
            self.driver.find_element(*locator)
            return True
        except NoSuchElementException:
            return False
        finally:
            self.driver.implicitly_wait(3)

    def is_element_present_no_waits(self, *locator) -> bool:
        try:
            self.driver.find_element(*locator)
            return True
        except NoSuchElementException:
            return False

    # sends value to field - needs value and a locator tuple
    def send_value_to_field(self, value, *locator):
        element = self.find_element(*locator)
        element.send_keys(value)
        time.sleep(0.3)

    def press_enter(self):
        ActionChains(self.driver).send_keys(Keys.ENTER).perform()

    # sends value to field and presses enter
    def send_value_to_field_press_enter(self, value, *locator):
        element = self.find_element(*locator)
        element.send_keys(value)
        time.sleep(0.3)
        ActionChains(self.driver).click(element).send_keys(Keys.ENTER).perform()
        time.sleep(0.3)

    # clears field by locator
    def clear_field(self, *locator):
        self.driver.find_element(*locator).clear()
        time.sleep(0.3)

    def press_down_arrow(self):
        ActionChains(self.driver).send_keys(Keys.DOWN).perform()

    def press_up_arrow(self):
        ActionChains(self.driver).send_keys(Keys.UP).perform()

    # waits for element visible on pag - needs time and locator tuple
    def explicitly_wait_for_element(self, wait_time: float, *locator: tuple):
        try:
            WebDriverWait(self.driver, wait_time).until(EC.visibility_of_element_located(locator))
        except Exception as e:
            print(f"failed to load element {str(e)}")

    def explicitly_wait_for_element_to_be_present(self, wait_time: float, *locator: tuple):
        try:
            WebDriverWait(self.driver, wait_time).until(EC.presence_of_element_located(locator))
        except Exception as e:
            print(f"failed to load element {str(e)}")

    def explicitly_wait_for_element_to_be_clickable(self, wait_time: float, *locator: tuple):
        try:
            WebDriverWait(self.driver, wait_time).until(EC.element_to_be_clickable(locator))
        except Exception as e:
            print(f"failed to load element {str(e)}")

    # waits for multiple elements
    def explicitly_wait_for_any_elements(self, wait_time: float, *locator: tuple):
        try:
            WebDriverWait(self.driver, wait_time).until(EC.visibility_of_any_elements_located(locator))
        except Exception as e:
            print(f"failed to load element {str(e)}")

    # finds an element and clicks on it
    def find_element_hover_click_element(self, *locator):
        element = self.driver.find_element(*locator)
        hover = ActionChains(self.driver).move_to_element(element)
        hover.click().perform()

    # hovers over element (need to provide an element, not a locator here)
    def hover_click_element(self, element):
        hover = ActionChains(self.driver).move_to_element(element)
        hover.click().perform()

    # allows navigating system tabs using tab name
    def navigate_to_tab(self, tab_name):
        locator = (By.LINK_TEXT, tab_name)
        if self.is_element_present(*locator) is False:
            self.refresh_page()
        self.driver.find_element(By.LINK_TEXT, tab_name).click()
        time.sleep(2)

    # enters iframe
    def switch_to_frame(self):
        ignored_exceptions = (NoSuchElementException, StaleElementReferenceException, TimeoutException)
        WebDriverWait(self.driver, 30, ignored_exceptions=ignored_exceptions) \
            .until(expected_conditions.frame_to_be_available_and_switch_to_it((By.XPATH, "//section/iframe")))
        # self.explicitly_wait_for_element_to_be_present(20, (By.XPATH, "//section/iframe"))
        # self.explicitly_wait_for_element(20, (By.XPATH, "//section/iframe"))
        # self.explicitly_wait_for_element_to_be_clickable(20, (By.XPATH, "//section/iframe"))
        # frame = self.find_element(By.XPATH, "//section/iframe")
        # self.driver.switch_to.frame(frame)
        time.sleep(0.5)

    def switch_to_default_content(self):
        self.driver.switch_to.default_content()
        time.sleep(0.5)

    def get_current_url(self):
        return self.driver.current_url

    # publishes a planet
    def publish_planet(self, sleep: float):
        self.driver.switch_to.default_content()
        self.navigate_to_tab("Planet Overview")
        self.switch_to_frame()
        self.find_element(*PlanetOverviewLocators.PUBLISH_BTN).click()
        time.sleep(sleep)

    # in planet overview page, clicks the site you want by its name
    def click_on_site_name_by_name(self, site_name):
        names = self.find_elements(*PlanetOverviewLocators.SITE_NAME)
        for name in names:
            if name.text == site_name:
                name.click()
                time.sleep(0.3)
                break

    # in site page, expands path by its name
    def expand_path_settings_by_name(self, path_name):
        names = self.find_elements(*PlanetOverviewLocators.PATH_NAME)
        for name in names:
            if name.text == path_name:
                name.click()
                time.sleep(0.3)
                break

    # parses the html of the page and looks for the pattern of your choice
    @staticmethod
    def verify_pattern_in_html(page_content, pattern: str) -> bool:
        soup = BeautifulSoup(page_content, 'html.parser')
        return pattern in str(soup)

    # in logs view logs page, filters the logs by site name
    def select_logs_site_by_name(self, site_name):
        self.driver.switch_to.default_content()
        self.navigate_to_tab("View Log")
        self.find_element(*ViewLogLocators.SELECT_SITE_DROPDOWN).click()
        self.send_value_to_field_press_enter(site_name, *ViewLogLocators.SELECT_SITE_INPUT)
        time.sleep(4)

    def refresh_page(self):
        self.driver.refresh()
        time.sleep(1)

    # select an acl policy by the name you need
    def select_acl_policy_site_by_name(self, policy_name):
        self.find_element(*AclMainLocators.POLICY_DROPDOWN).click()
        self.send_value_to_field_press_enter(policy_name, *AclMainLocators.SELECT_POLICY_INPUT)
        time.sleep(2)

    def call(self, method, module, suffix=None, inputjson=None):
        cmd = ''
        logging.info("Calling CLI with arguments: %s")
        if module == "acl":
            cmd = ["curl", "-s", f"-{method}", "$DEBUG", f"https://{_ACL_HOST}/api/{_ACL_KEY}{suffix}"]
        elif module == "rl":
            cmd = ["curl", "-s", f"-{method}", f"https://{_RL_HOST}/api/{_RL_KEY}{suffix}"]
        elif module == "tag":
            cmd = ["curl", "-s", f"-{method}", f"https://{_TAG_HOST}/api/{_TAG_KEY}{suffix}"]
        elif module == "ssl":
            cmd = ["curl", "-s", f"-{method}", f"https://{_SSL_HOST}/api/{_SSL_KEY}{suffix}"]

        indata = None
        if inputjson:
            indata = json.dumps(inputjson).encode("utf-8")

        process = subprocess.run(
            cmd,
            shell=False,
            input=indata,
            check=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        if process.stdout:
            logging.debug("CLI output: %s", process.stdout)

            try:
                return json.loads(process.stdout.decode("utf-8"))
            except json.JSONDecodeError:
                return process.stdout.decode("utf-8")
        else:
            return []

    # gets all rate limit ids
    def get_rate_limit_ids(self):
        rl_list = self.call("XGET", "rl", suffix="/ratelimit/list")
        return [[v for k, v in rl.items() if k == "id"] for rl in rl_list]

    # returns my ip
    def get_my_public_ip(self):
        return get('https://api.ipify.org').text

    # returns my asn
    def get_my_asn(self):
        handler = ipinfo.getHandler("e33b32389d6082")
        details = handler.getDetails(self.get_my_public_ip())
        return details.all["org"]

    def get_my_country_name_and_code(self):
        handler = ipinfo.getHandler("e33b32389d6082")
        details = handler.getDetails(self.get_my_public_ip())
        return details.all["country_name"], details.all["country"]

    # converts date to utc format: Wed 10 Nov 2021 09:15:44
    @staticmethod
    def format_date_to_string():
        return datetime.datetime.today().utcnow().strftime("%a %b %d %Y %H:%M:%S")









