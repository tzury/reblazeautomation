from selenium.webdriver.common.by import By


class PlanetOverviewLocators:
    SITE_NAME = (By.XPATH, "//tr/td/span")
    PATH_NAME = (By.XPATH, "//td")
    REMOVE_BTN = (By.LINK_TEXT, "remove")
    ADD_BTN = (By.LINK_TEXT, "add")
    SAVE_BTN = (By.XPATH, "//button[@title='Save changes']")
    PUBLISH_BTN = (By.XPATH, "//button[contains(text(), 'Publish Changes')]")
    ATTACH_NEW_RULE_TO_URL_BTN = (By.XPATH, "//*[contains(@data-tooltip, 'Add a rate limit rule')]")
    ATTACH_NEW_RULE_TO_SITE_BTN = (By.XPATH, "//*[contains(@data-tooltip, 'Add a site level rate limit rule')]")
    RATE_LIMIT_RULES_DROPDOWN = (By.XPATH, "//tbody//div[contains(@class,'dropdown is')]")
    RATE_LIMIT_RULES_DROPDOWN_OPTION = (By.XPATH, "//div[@class='dropdown-content']/a")

    ACL_DEFAULT_VALUE = (By.XPATH, "//option[@value='0000']")
    WAF_DEFAULT_VALUE = (By.XPATH, "//option[@value='0001']")

    ACTIVATE_CHECKBOX = (By.XPATH, "//input[contains(text(), 'Active Mode')]")