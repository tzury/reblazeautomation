from selenium.webdriver.common.by import By


class LeftNavLocators:
    NAV_BAR = (By.XPATH, "//*[contains(@class, 'navbar navbar-inverse')]")
    VIEW_LOG_BTN = (By.LINK_TEXT, "View Log")
    DYNAMIC_RULES_BTN = (By.LINK_TEXT, "Dynamic Rules")
    QUARANTINED_BTN = (By.LINK_TEXT, "Quarantined")
    PROFILES_BTN = (By.LINK_TEXT, "Profiles")
    ARGS_ANALYSIS_BTN = (By.LINK_TEXT, "Args Analysis")
    TAG_RULES_BTN = (By.LINK_TEXT, "Tag Rules")
    RATE_LIMITING_BTN = (By.LINK_TEXT, "Rate Limiting")
    CLOUD_FUNCTIONS_BTN = (By.LINK_TEXT, "Cloud Functions")
    WEB_PROXY_BTN = (By.LINK_TEXT, "Web Proxy")
    BACKEND_SERVICES_BTN = (By.LINK_TEXT, "Backend Services")
    ERROR_PAGES_BTN = (By.LINK_TEXT, "Error Pages")
    SSL_BTN = (By.LINK_TEXT, "SSL")
    DNS_BTN = (By.LINK_TEXT, "DNS")
    PLANET_OVERVIEW_BTN = (By.LINK_TEXT, "Planet Overview")
    ACCOUNT_BTN = (By.LINK_TEXT, "Account")
    SUPPORT_BTN = (By.LINK_TEXT, "Support")
    API_BTN = (By.LINK_TEXT, "API")
    LOGOUT_BTN = (By.XPATH, "//*[@title='Sign out']")