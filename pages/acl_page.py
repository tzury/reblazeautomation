import time
from core.base_page import BasePage
from locators.acl_page_locators import *
from locators.planet_overview_locators import *
from selenium.webdriver.support.ui import Select


class AclPage(BasePage):

    def wait_for_acl_page_to_load(self):
        self.explicitly_wait_for_element(20, *AclMainLocators.LEFT_PROFILES_LIST)

    def click_acl_profiles_btn(self):
        self.explicitly_wait_for_element_to_be_clickable(20, *AclMainLocators.PROFILES_BTN)
        self.find_element(*AclMainLocators.PROFILES_BTN).click()
        time.sleep(0.4)

    def click_acl_default_option(self):
        self.find_element(By.LINK_TEXT, "ACL Default").click()

    def click_acl_deny_all_option(self):
        self.find_element(By.LINK_TEXT, "ACL Deny All").click()

    def click_acl_policies_btn(self):
        self.explicitly_wait_for_element_to_be_clickable(20, *AclMainLocators.ACL_POLICIES_BTN)
        self.find_element(*AclMainLocators.ACL_POLICIES_BTN).click()
        time.sleep(0.4)

    def click_waf_rules_btn(self):
        self.explicitly_wait_for_element_to_be_clickable(20, *AclMainLocators.WAF_POLICIES_BTN)
        self.find_element(*AclMainLocators.WAF_POLICIES_BTN).click()
        time.sleep(0.4)

    def click_custom_signatures_btn(self):
        self.explicitly_wait_for_element_to_be_clickable(20, *AclMainLocators.CUSTOM_SIGNATURE_BTN)
        self.find_element(*AclMainLocators.CUSTOM_SIGNATURE_BTN).click()
        time.sleep(0.4)

    # get the text of all policies
    def get_policies_table_text(self):
        text = self.find_element(*AclMainLocators.LINKED_POLICIES_TABLE).text.replace("\n", "").split("×")
        return list(filter(None, text))

    # gets the text of policy rules
    def get_policy_rules_list_table_text(self):
        text = self.find_element(*AclMainLocators.ACL_POLICIY_PROFILE_LIST).text.replace("\n", "").split("×")
        return list(filter(None, text))

    # get the text of all waf
    def get_waf_table_text(self):
        text = self.find_element(*AclMainLocators.WAF_RIGHT_PANE).text.replace("\n", "").split("×")
        return list(filter(None, text))

    def press_save(self):
        self.find_element(By.LINK_TEXT, "Save").click()
        time.sleep(1)

    # presses duplicate on all tabs
    def press_duplicate(self):
        self.find_element(By.LINK_TEXT, "Duplicate").click()

    # presses delete on all tabs
    def press_delete(self):
        self.find_element(By.LINK_TEXT, "Delete").click()

    # presses add on all tabs
    def press_add(self):
        self.find_element(*AclMainLocators.ADD_POLICY_BTN).click()
        time.sleep(0.7)

    # deletes acl profile by its name
    def remove_acl_policy_by_name(self, policy_name):
        policies = self.find_elements(By.XPATH, "//div[@class='profiles-list']//tr")
        for policy in policies:
            if policy.text.strip("\n×") == policy_name:
                policy.find_element(By.XPATH, ".//button").click()
                break

    # deletes acl rule by its name
    def remove_acl_rule_by_name(self, rule_name):
        rules = self.find_elements(By.XPATH, "//div[@class='profiles-list']//tr")
        for rule in rules:
            if rule_name in rule.text.strip("\n×") == rule_name:
                rule.find_element(By.XPATH, ".//button").click()
                break

    # clicks on profile name
    def click_profile_by_name(self, prof_name):
        self.find_element(By.LINK_TEXT, prof_name).click()

    # attaches a profile to site by site name an path name
    def attach_profile_to_url_by_path_and_profile_name(self, site_name, path_name, profile_name):
        self.navigate_to_tab("Planet Overview")
        self.switch_to_frame()
        self.click_on_site_name_by_name(site_name)
        self.switch_to_frame()
        self.expand_path_settings_by_name(path_name)
        time.sleep(0.5)
        self.find_element(*PlanetOverviewLocators.ACL_DEFAULT_VALUE).click()
        time.sleep(0.5)
        self.find_element(By.XPATH, f"//option[contains(text(), '{profile_name}')]").click()
        self.find_element(*PlanetOverviewLocators.SAVE_BTN).click()

    # attaches waf to site by site name an path name
    def attach_waf_to_url_by_path_and_waf_name(self, site_name, path_name, waf_name):
        self.navigate_to_tab("Planet Overview")
        self.switch_to_frame()
        self.click_on_site_name_by_name(site_name)
        self.switch_to_frame()
        self.expand_path_settings_by_name(path_name)
        time.sleep(0.5)
        self.find_element(*PlanetOverviewLocators.WAF_DEFAULT_VALUE).click()
        time.sleep(0.5)
        self.find_element(By.XPATH, f"//option[contains(text(), '{waf_name}')]").click()
        time.sleep(0.3)
        inputs = self.find_elements(By.XPATH, "//label/input")
        if inputs[0].is_selected() is False:
            inputs[0].click()
        self.find_element(*PlanetOverviewLocators.SAVE_BTN).click()

    def click_waf_active_checkbox(self, path_name):
        self.expand_path_settings_by_name(path_name)
        self.click_waf_activate_checkbox()
        self.find_element(*PlanetOverviewLocators.SAVE_BTN).click()

    def check_site_attached_acl_profile(self, profile_name):
        tables = self.find_elements(By.XPATH, "//tbody")
        assert tables[2].find_element(By.XPATH, f".//td[contains(text(), '{profile_name}')]")

    # selects rule operation by name
    def select_new_acl_rule_operation_by_name(self, op_name):
        self.find_element(*AclMainLocators.NEW_RULE_OPERATION_SELECT).click()
        self.find_element(By.XPATH, f"//option[contains(text(), '{op_name}')]").click()
        time.sleep(0.3)

    # select a match by name
    def select_new_acl_rule_match_by_name(self, match_name):
        self.find_element(*AclMainLocators.NEW_RULE_MATCH_SELECT).click()
        self.find_element(By.XPATH, f"//option[contains(text(), '{match_name}')]").click()
        time.sleep(0.3)

    # fills acl rule value
    def fill_acl_new_rule_value(self, value):
        self.send_value_to_field(value, *AclMainLocators.NEW_RULE_VALUE_TEXT)
        self.find_element(*AclMainLocators.ADD_POLICY_NAME_INPUT).click()
        time.sleep(0.3)

    def fill_custom_sig_name_in_acl_policy(self, cs_name):
        self.send_value_to_field(cs_name, *AclMainLocators.NEW_RULE_VALUE_TEXT)
        time.sleep(0.2)
        self.press_down_arrow()
        time.sleep(0.2)
        self.press_enter()
        time.sleep(0.3)

    def click_waf_activate_checkbox(self):
        inputs = self.find_elements(By.XPATH, "//label/input")
        inputs[0].click()
        time.sleep(0.5)

    def generate_params(self, num):
        params = {}
        for i in range(num):
            rnd = self.generate_random_string(6)
            params[i] = rnd
        return params

    def select_custom_sig_filter(self, value):
        element = self.find_element(*AclMainLocators.CUSTOM_SIG_FILTER_INPUT)
        element.send_keys(" ")
        time.sleep(0.3)
        self.send_value_to_field(value, *AclMainLocators.CUSTOM_SIG_FILTER_INPUT)
        self.press_down_arrow()
        self.press_enter()

    def select_second_custom_sig_filter(self, value):
        element = self.find_element(*AclMainLocators.CUSTOM_SIG_SECOND_FILTER_INPUT)
        element.send_keys(" ")
        time.sleep(0.3)
        self.send_value_to_field(value, *AclMainLocators.CUSTOM_SIG_SECOND_FILTER_INPUT)
        self.press_down_arrow()
        self.press_enter()

    def click_custom_sig_and_or_switch(self):
        self.find_element(*AclMainLocators.CUSTOM_SIG_AND_OR_SWITCH).click()
        time.sleep(0.5)

    def click_custom_sig_append_condition_btn(self):
        self.find_element(*AclMainLocators.APPEND_CONDITION_BTN).click()
        time.sleep(0.5)

    def delete_all_policies_in_a_profile(self):
        buttons = self.find_elements(*AclMainLocators.POLICIES_DELETE_BUTTON)
        for i in range(len(buttons)):
            time.sleep(0.2)
            buttons[0].click()
            buttons = self.find_elements(*AclMainLocators.POLICIES_DELETE_BUTTON)

    def delete_all_cs_rules_in_policy(self):
        buttons = self.find_elements(*AclMainLocators.CS_RULES_DELETE_BUTTON)
        for i in range(len(buttons)):
            time.sleep(0.2)
            buttons[0].click()
            buttons = self.find_elements(*AclMainLocators.CS_RULES_DELETE_BUTTON)




