import time
import math
from core.base_page import *
from locators.left_nav_locators import *
from locators.dashboard_locators import *
import datetime
from datetime import timedelta
from selenium.webdriver.common.action_chains import ActionChains


class DashboardPage(BasePage):

    def wait_for_dashboard_to_load(self):
        self.explicitly_wait_for_element(20, LeftNavLocators.NAV_BAR)

    def click_datarange(self):
        self.find_element(*DashboardLocators.DASHBOARD_RANGE_PICKER).click()
        time.sleep(0.5)

    def get_daterange_text(self):
        time_text = self.find_element(*DashboardLocators.DASHBOARD_RANGE_PICKER).text
        return time_text[2:16], time_text[19:]

    def click_countries_tab(self):
        self.find_element(*BottomNavLocators.COUNTRIES_BTN).click()
        time.sleep(0.5)

    def click_sources_tab(self):
        self.find_element(*BottomNavLocators.SOURCES_BTN).click()
        time.sleep(0.5)

    def click_sessions_tab(self):
        self.find_element(*BottomNavLocators.SESSIONS_BTN).click()
        time.sleep(0.5)

    def click_targets_tab(self):
        self.find_element(*BottomNavLocators.TARGETS_BTN).click()
        time.sleep(7)

    def click_signatures_tab(self):
        self.find_element(*BottomNavLocators.SIGNATURES_BTN).click()
        time.sleep(1)

    def click_referers_tab(self):
        self.find_element(*BottomNavLocators.REFERERS_BTN).click()
        time.sleep(1)

    def click_browsers_tab(self):
        self.find_element(*BottomNavLocators.BROWSERS_BTN).click()
        time.sleep(1)

    def click_companies_tab(self):
        self.find_element(*BottomNavLocators.COMPANIES_BTN).click()
        time.sleep(1)

    def click_total_time_tab(self):
        self.find_element(*BottomNavLocators.TOTAL_TIME_BTN).click()
        time.sleep(1)

    def click_total_rbz_time_tab(self):
        self.find_element(*BottomNavLocators.REBLAZE_TIME_BTN).click()
        time.sleep(1)

    def click_origin_time_tab(self):
        self.find_element(*BottomNavLocators.ORIGIN_TIME_BTN).click()
        time.sleep(1)

    @staticmethod
    def format_dashboard_date_to_string(gap_in_minutes):
        now = datetime.datetime.today().utcnow().strftime("%y-%m-%d %H:%M")
        time_minus_gap = datetime.datetime.today().utcnow() - timedelta(minutes=gap_in_minutes)
        return time_minus_gap.strftime("%y-%m-%d %H:%M"), now

    @staticmethod
    def validate_time_to_in_range(time_to_compare, gap):
        from_time = datetime.datetime.strptime((datetime.datetime.today().utcnow() - timedelta(minutes=gap)).strftime("%H:%M"), "%H:%M")
        to_time = datetime.datetime.strptime((datetime.datetime.today().utcnow() + timedelta(minutes=gap)).strftime("%H:%M"), "%H:%M")
        return from_time <= datetime.datetime.strptime(time_to_compare, "%H:%M") <= to_time

    @staticmethod
    def validate_time_from_in_range(time_to_compare, gap):
        from_time = datetime.datetime.strptime(
            (datetime.datetime.today().utcnow() - timedelta(minutes=30) - timedelta(minutes=gap)).strftime("%H:%M"), "%H:%M")
        to_time = datetime.datetime.strptime(
            (datetime.datetime.today().utcnow() - timedelta(minutes=30) + timedelta(minutes=gap)).strftime("%H:%M"), "%H:%M")
        return from_time <= datetime.datetime.strptime(time_to_compare, "%H:%M") <= to_time

    def get_all_passed_blocked_dates(self):
        self.explicitly_wait_for_any_elements(10, *DashboardLocators.SINGLE_HOUR_IN_PASSED_VS_BLOCKED_CHART)
        times = self.find_elements(*DashboardLocators.SINGLE_HOUR_IN_PASSED_VS_BLOCKED_CHART)
        return [a.text for a in times]

    def get_all_response_status_dates(self):
        self.explicitly_wait_for_any_elements(10, *DashboardLocators.SINGLE_HOUR_IN_RESPONSE_STATUS_CHART)
        times = self.find_elements(*DashboardLocators.SINGLE_HOUR_IN_RESPONSE_STATUS_CHART)
        return [a.text for a in times]

    def get_all_total_bandwidth_dates(self):
        self.explicitly_wait_for_any_elements(10, *DashboardLocators.SINGLE_HOUR_IN_TOTAL_BANDWIDTH_CHART)
        times = self.find_elements(*DashboardLocators.SINGLE_HOUR_IN_TOTAL_BANDWIDTH_CHART)
        return [a.text for a in times]

    def get_all_unique_sessions_dates(self):
        self.explicitly_wait_for_any_elements(10, *DashboardLocators.SINGLE_HOUR_IN_UNIQUE_SESSIONS_CHART)
        times = self.find_elements(*DashboardLocators.SINGLE_HOUR_IN_UNIQUE_SESSIONS_CHART)
        return [a.text for a in times]

    def get_all_latency_dates(self):
        self.explicitly_wait_for_any_elements(10, *DashboardLocators.SINGLE_HOUR_IN_LATENCY_CHART)
        times = self.find_elements(*DashboardLocators.SINGLE_HOUR_IN_LATENCY_CHART)
        return [a.text for a in times]

    def get_all_bandwidth_dates(self):
        self.explicitly_wait_for_any_elements(10, *DashboardLocators.SINGLE_HOUR_IN_BANDWIDTH_CHART)
        times = self.find_elements(*DashboardLocators.SINGLE_HOUR_IN_BANDWIDTH_CHART)
        return [a.text for a in times]

    def get_all_request_count_dates(self):
        self.explicitly_wait_for_any_elements(10, *DashboardLocators.SINGLE_HOUR_IN_REQUEST_COUNT_CHART)
        times = self.find_elements(*DashboardLocators.SINGLE_HOUR_IN_REQUEST_COUNT_CHART)
        return [a.text for a in times]

    def get_passed_chart_coordinates(self):
        self.explicitly_wait_for_element_to_be_clickable(10, *DashboardLocators.PASSED_VS_BLOCKED)
        chart = self.find_element(*DashboardLocators.PASSED_CHART)
        loc = chart.location
        return chart, loc

    def return_passed_chart_data(self):
        date = self.find_element(*DashboardLocators.PASSED_CHART_DATA_DATE_TEXT).text
        value = self.find_element(*DashboardLocators.PASSED_CHART_DATA_TEXT).text
        block_val = self.find_element(*DashboardLocators.PASSED_CHART_BLOCKED_TEXT).text
        return date, value, block_val

    def return_status_chart_data(self):
        date = self.find_element(*DashboardLocators.STATUS_CHART_DATA_DATE_TEXT).text
        value = self.find_element(*DashboardLocators.STATUS_CHART_DATA_TEXT).text
        block_val = self.find_element(*DashboardLocators.STATUS_CHART_BLOCKED_TEXT).text
        return date, value, block_val

    # dont ask
    def move_cursor_on_half_hour_chart(self, chart, loc):
        ActionChains(self.driver).move_to_element_with_offset(chart, loc["x"] - 190, loc["y"]).perform()
        time.sleep(0.2)
        date, value, block_val = self.return_passed_chart_data()
        hits = {}
        blocked = {}
        hits[date] = int(value)
        blocked[date] = int(block_val)
        screen_size = self.driver.get_window_size()
        t = 1
        if screen_size["width"] == 3440:
            t = 2.7
        if screen_size["width"] == 1200:
            t = 0.8
        for i in range(30):
            if i == 29:
                ActionChains(self.driver).move_by_offset(20*t, 0).perform()
                time.sleep(0.1)
                date, value, block_val = self.return_passed_chart_data()
            else:
                ActionChains(self.driver).move_by_offset(40*t, 0).perform()
                time.sleep(0.1)
                date, value, block_val = self.return_passed_chart_data()
            hits[date] = int(value)
            blocked[date] = int(block_val)
        return sum(hits.values()), sum(blocked.values())

    def move_cursor_on_one_hour_chart(self, chart, loc):
        ActionChains(self.driver).move_to_element_with_offset(chart, loc["x"] - 190, loc["y"]).perform()
        time.sleep(0.2)
        date, value, block_val = self.return_passed_chart_data()
        hits = {}
        blocked = {}
        hits[date] = int(value)
        blocked[date] = int(block_val)
        screen_size = self.driver.get_window_size()
        t = 1
        if screen_size["width"] == 3440:
            t = 2.7
        if screen_size["width"] == 1200:
            t = 0.8
        for i in range(60):
            if i == 59:
                ActionChains(self.driver).move_by_offset(19*t, 0).perform()
                time.sleep(0.1)
                date, value, block_val = self.return_passed_chart_data()
            else:
                ActionChains(self.driver).move_by_offset(19.5*t, 0).perform()
                time.sleep(0.1)
                date, value, block_val = self.return_passed_chart_data()
            hits[date] = int(value)
            blocked[date] = int(block_val)
        return sum(hits.values()), sum(blocked.values())

    def get_displayed_hits_blocked_text(self):
        time.sleep(3)
        return self.find_elements(*DashboardLocators.PASSED_CHART_HITS_BLOCKED)

    def get_displayed_status_hits_text(self):
        time.sleep(3)
        return self.find_elements(*DashboardLocators.STATUS_CHART_HITS_BLOCKED)

    def move_cursor_half_hour_chart(self):
        self.explicitly_wait_for_element_to_be_clickable(20, *DashboardLocators.PASSED_CHART_LOCATION)
        chart = self.find_element(*DashboardLocators.PASSED_CHART_LOCATION)
        sz = chart.size
        gap = int(sz["width"])/30
        math.ceil(gap)
        ActionChains(self.driver).move_to_element_with_offset(chart, 4, 0).perform()
        time.sleep(0.5)
        date, value, block_val = self.return_passed_chart_data()
        hits = {}
        blocked = {}
        hits[date] = int(value)
        blocked[date] = int(block_val)
        for i in range(31):
            ActionChains(self.driver).move_by_offset(gap, 0).perform()
            time.sleep(0.1)
            date, value, block_val = self.return_passed_chart_data()
            hits[date] = int(value)
            blocked[date] = int(block_val)
        return sum(hits.values()), sum(blocked.values())

    def move_status_cursor_half_hour_chart(self):
        time.sleep(5)
        self.explicitly_wait_for_element_to_be_clickable(20, *DashboardLocators.STATUS_CHART_LOCATION)
        chart = self.find_element(*DashboardLocators.STATUS_CHART_LOCATION)
        sz = chart.size
        gap = int(sz["width"])/30
        math.ceil(gap)
        ActionChains(self.driver).move_to_element_with_offset(chart, 8, 0).perform()
        time.sleep(0.5)
        date, value, block_val = self.return_status_chart_data()
        hits = {}
        blocked = {}
        hits[date] = int(value)
        blocked[date] = int(block_val)
        for i in range(31):
            ActionChains(self.driver).move_by_offset(gap, 0).perform()
            time.sleep(0.1)
            date, value, block_val = self.return_status_chart_data()
            hits[date] = int(value)
            blocked[date] = int(block_val)
        return sum(hits.values()), sum(blocked.values())

    def move_status_cursor_half_hour_chart_with_rl_blocks(self,):
        self.explicitly_wait_for_element_to_be_clickable(20, *DashboardLocators.STATUS_CHART_LOCATION)
        chart = self.find_element(*DashboardLocators.STATUS_CHART_LOCATION)
        sz = chart.size
        gap = int(sz["width"])/30
        math.ceil(gap)
        ActionChains(self.driver).move_to_element_with_offset(chart, 8, 0).perform()
        time.sleep(0.5)
        date, value, block_val = self.return_status_chart_data()
        rl_block_val = self.find_element(*DashboardLocators.STATUS_CHART_BLOCKED_RATE_LIMIT_TEXT).text
        hits = {}
        blocked = {}
        rl_blocked = {}
        hits[date] = int(value)
        blocked[date] = int(block_val)
        rl_blocked[date] = int(rl_block_val)
        for i in range(31):
            ActionChains(self.driver).move_by_offset(gap, 0).perform()
            time.sleep(0.1)
            date, value, block_val = self.return_status_chart_data()
            rl_block_val = self.find_element(*DashboardLocators.STATUS_CHART_BLOCKED_RATE_LIMIT_TEXT).text
            hits[date] = int(value)
            blocked[date] = int(block_val)
            rl_blocked[date] = int(rl_block_val)
        return sum(hits.values()), sum(blocked.values()), sum(rl_blocked.values())

    def move_cursor_hour_chart(self):
        self.explicitly_wait_for_element_to_be_clickable(20, *DashboardLocators.PASSED_CHART_LOCATION)
        chart = self.find_element(*DashboardLocators.PASSED_CHART_LOCATION)
        sz = chart.size
        gap = int(sz["width"])/59
        loc = chart.location
        ActionChains(self.driver).move_to_element_with_offset(chart, 0, loc["y"]).perform()
        time.sleep(0.2)
        date, value, block_val = self.return_passed_chart_data()
        hits = {}
        blocked = {}
        hits[date] = int(value)
        blocked[date] = int(block_val)
        for i in range(60):
            if i == 59:
                ActionChains(self.driver).move_by_offset(5, 0).perform()
                time.sleep(0.1)
                date, value, block_val = self.return_passed_chart_data()
            else:
                ActionChains(self.driver).move_by_offset(gap, 0).perform()
                time.sleep(0.1)
                date, value, block_val = self.return_passed_chart_data()
            hits[date] = int(value)
            blocked[date] = int(block_val)
        return sum(hits.values()), sum(blocked.values())

    def move_status_cursor_hour_chart(self):
        self.explicitly_wait_for_element_to_be_clickable(20, *DashboardLocators.STATUS_CHART_LOCATION)
        chart = self.find_element(*DashboardLocators.STATUS_CHART_LOCATION)
        sz = chart.size
        gap = int(sz["width"])/59
        ActionChains(self.driver).move_to_element_with_offset(chart, 0, 0).perform()
        time.sleep(0.5)
        date, value, block_val = self.return_status_chart_data()
        hits = {}
        blocked = {}
        hits[date] = int(value)
        blocked[date] = int(block_val)
        for i in range(60):
            if i == 59:
                ActionChains(self.driver).move_by_offset(5, 0).perform()
                for a in range(50):
                    ActionChains(self.driver).move_by_offset(a, 0).perform()
                    if self.is_element_present(*DashboardLocators.STATUS_CHART_DATA_DATE_TEXT):
                        break
                time.sleep(0.1)
                date, value, block_val = self.return_status_chart_data()
            else:
                ActionChains(self.driver).move_by_offset(gap, 0).perform()
                time.sleep(0.1)
                date, value, block_val = self.return_status_chart_data()
            hits[date] = int(value)
            blocked[date] = int(block_val)
        return sum(hits.values()), sum(blocked.values())

    # def move_request_count_cursor_half_hour_chart(self,):
    #     self.explicitly_wait_for_element_to_be_clickable(20, *DashboardLocators.REQUEST_COUNT_CHART_LOCATION)
    #     chart = self.find_element(*DashboardLocators.REQUEST_COUNT_CHART_LOCATION)
    #     sz = chart.size
    #     gap = int(sz["width"])/30
    #     math.ceil(gap)
    #     ActionChains(self.driver).move_to_element_with_offset(chart, 4, 0).perform()
    #     date = self.find_element(*DashboardLocators.REQUEST_COUNT_CHART_DATA_DATE_TEXT).text
    #     value = self.find_element(*DashboardLocators.REQUEST_COUNT_CHART_DATA_TEXT).text
    #     hits = {}
    #     hits[date] = int(value)
    #     for i in range(31):
    #         ActionChains(self.driver).move_by_offset(gap+0.14, 0).perform()
    #         time.sleep(0.1)
    #         date = self.find_element(*DashboardLocators.REQUEST_COUNT_CHART_DATA_DATE_TEXT).text
    #         value = self.find_element(*DashboardLocators.REQUEST_COUNT_CHART_DATA_TEXT).text
    #         hits[date] = int(value)
    #     return sum(hits.values())

    def move_request_count_cursor_hour_chart(self):
        self.explicitly_wait_for_element_to_be_clickable(20, *DashboardLocators.REQUEST_COUNT_CHART_LOCATION)
        chart = self.find_element(*DashboardLocators.REQUEST_COUNT_CHART_LOCATION)
        sz = chart.size
        gap = int(sz["width"])/59
        ActionChains(self.driver).move_to_element_with_offset(chart, 0, 0).perform()
        time.sleep(0.5)
        date = self.find_element(*DashboardLocators.REQUEST_COUNT_CHART_DATA_DATE_TEXT).text
        value = self.find_element(*DashboardLocators.REQUEST_COUNT_CHART_DATA_TEXT).text
        hits = {}
        hits[date] = int(value)
        for i in range(60):
            if i == 59:
                ActionChains(self.driver).move_by_offset(5, 0).perform()
                time.sleep(0.1)
                date = self.find_element(*DashboardLocators.REQUEST_COUNT_CHART_DATA_DATE_TEXT).text
                value = self.find_element(*DashboardLocators.REQUEST_COUNT_CHART_DATA_TEXT).text
            else:
                ActionChains(self.driver).move_by_offset(gap, 0).perform()
                time.sleep(0.1)
                date = self.find_element(*DashboardLocators.REQUEST_COUNT_CHART_DATA_DATE_TEXT).text
                value = self.find_element(*DashboardLocators.REQUEST_COUNT_CHART_DATA_TEXT).text
            hits[date] = int(value)
        return sum(hits.values())

    def get_total_hits_in_application_tab(self):
        rows = self.find_elements(*BottomNavLocators.BOTTOM_TAB_ROW)
        total = []
        for row in rows:
            data = row.find_elements(By.XPATH, ".//td")
            total.append(int(data[1].text.replace(",", "")))
        return sum(total)

    def get_total_blocks_in_application_tab(self):
        rows = self.find_elements(*BottomNavLocators.BOTTOM_TAB_ROW)
        total = []
        for row in rows:
            data = row.find_elements(By.XPATH, ".//td")
            total.append(int(data[2].text.replace(",", "")))
        return sum(total)

    def get_total_blocks_in_countries_tab(self):
        rows = self.find_elements(*BottomNavLocators.COUNTRIES_ROW)
        total = []
        for row in rows:
            data = row.find_elements(By.XPATH, ".//td")
            total.append(int(data[3].text.replace(",", "")))
        return sum(total)

    def get_total_blocks_in_sources_tab(self):
        rows = self.find_elements(*BottomNavLocators.SOURCES_ROW)
        total = []
        for row in rows:
            data = row.find_elements(By.XPATH, ".//td")
            total.append(int(data[5].text.replace(",", "")))
        return sum(total)

    def get_total_blocks_in_sessions_tab(self):
        rows = self.find_elements(*BottomNavLocators.SESSIONS_ROW)
        total = []
        passed = []
        blocked = []
        for row in rows:
            data = row.find_elements(By.XPATH, ".//td")
            total.append(int(data[1].text.replace(",", "")))
            passed.append(int(data[2].text.replace(",", "")))
            blocked.append(int(data[3].text.replace(",", "")))
        return sum(total), sum(passed), sum(blocked)

    def get_total_blocks_in_referers_tab(self):
        rows = self.find_elements(*BottomNavLocators.REFERERS_ROW)
        total = []
        passed = []
        blocked = []
        for row in rows:
            data = row.find_elements(By.XPATH, ".//td")
            total.append(int(data[1].text.replace(",", "")))
            passed.append(int(data[2].text.replace(",", "")))
            blocked.append(int(data[3].text.replace(",", "")))
        return sum(total), sum(passed), sum(blocked)

    def get_total_blocks_in_browsers_tab(self):
        rows = self.find_elements(*BottomNavLocators.BROWSERS_ROW)
        total = []
        passed = []
        blocked = []
        names = []
        for row in rows:
            data = row.find_elements(By.XPATH, ".//td")
            names.append(data[0].text.replace(",", ""))
            total.append(int(data[1].text.replace(",", "")))
            passed.append(int(data[2].text.replace(",", "")))
            blocked.append(int(data[3].text.replace(",", "")))
        return sum(total), sum(passed), sum(blocked), names

    def get_total_blocks_in_companies_tab(self):
        rows = self.find_elements(*BottomNavLocators.COMPANIES_ROW)
        total = []
        passed = []
        blocked = []
        names = []
        for row in rows:
            data = row.find_elements(By.XPATH, ".//td")
            names.append(data[0].text.replace(",", ""))
            total.append(int(data[1].text.replace(",", "")))
            passed.append(int(data[2].text.replace(",", "")))
            blocked.append(int(data[3].text.replace(",", "")))
        return sum(total), sum(passed), sum(blocked), names

    def get_total_hits_in_total_time_tab(self):
        rows = self.find_elements(*BottomNavLocators.TOTAL_TIME_ROW)
        total = []
        passed = []
        blocked = []
        names = []
        for row in rows:
            data = row.find_elements(By.XPATH, ".//td")
            names.append(data[0].text.replace(",", ""))
            total.append(int(data[1].text.replace(",", "")))
            passed.append(int(data[2].text.replace(",", "")))
            blocked.append(int(data[3].text.replace(",", "")))
        return sum(total), sum(passed), sum(blocked), names

    def get_total_hits_in_total_rbz_time_tab(self):
        rows = self.find_elements(*BottomNavLocators.REBLAZE_TIME_ROW)
        total = []
        passed = []
        blocked = []
        names = []
        for row in rows:
            data = row.find_elements(By.XPATH, ".//td")
            names.append(data[0].text.replace(",", ""))
            total.append(int(data[1].text.replace(",", "")))
            passed.append(int(data[2].text.replace(",", "")))
            blocked.append(int(data[3].text.replace(",", "")))
        return sum(total), sum(passed), sum(blocked), names

    def get_total_hits_in_origin_time_tab(self):
        rows = self.find_elements(*BottomNavLocators.ORIGIN_TIME_ROW)
        total = []
        passed = []
        blocked = []
        names = []
        for row in rows:
            data = row.find_elements(By.XPATH, ".//td")
            names.append(data[0].text.replace(",", ""))
            total.append(int(data[1].text.replace(",", "")))
            passed.append(int(data[2].text.replace(",", "")))
            blocked.append(int(data[3].text.replace(",", "")))
        return sum(total), sum(passed), sum(blocked), names

    def send_requests_for_dashboard(self, req_num, domain, path, ttl):
        for i in range(req_num):
            rand_path = self.generate_random_string(7)
            self.query(domain, f"/{path}{rand_path}")
        time.sleep(ttl)

    def get_first_application_text(self):
        rows = self.find_elements(*BottomNavLocators.APPLICATIONS_ROW)
        return rows[0].find_elements(By.XPATH, ".//td")[0].text.replace(",", "")

    def get_first_target_text(self):
        rows = self.find_elements(*BottomNavLocators.TARGETS_ROW)
        return rows[0].find_elements(By.XPATH, ".//td")[0].text.replace(",", "")

    def get_first_signatures_text(self):
        rows = self.find_elements(*BottomNavLocators.SIGNATURES_ROW)
        return rows[0].find_elements(By.XPATH, ".//td")[0].text

    def get_first_browsers_text(self):
        rows = self.find_elements(*BottomNavLocators.BROWSERS_ROW)
        return rows[0].find_elements(By.XPATH, ".//td")[0].text.replace(",", "")

    def get_first_country_text(self):
        rows = self.find_elements(*BottomNavLocators.COUNTRIES_ROW)
        return rows[0].find_elements(By.XPATH, ".//td")[0].text.replace(",", "")

    def get_first_companies_text(self):
        rows = self.find_elements(*BottomNavLocators.COMPANIES_ROW)
        return rows[0].find_elements(By.XPATH, ".//td")[0].text.replace(",", "")

    def get_first_total_time_text(self):
        rows = self.find_elements(*BottomNavLocators.TOTAL_TIME_ROW)
        return rows[0].find_elements(By.XPATH, ".//td")[0].text.replace(",", "")

    def get_first_total_rbz_time_text(self):
        rows = self.find_elements(*BottomNavLocators.REBLAZE_TIME_ROW)
        return rows[0].find_elements(By.XPATH, ".//td")[0].text.replace(",", "")

    def get_first_origin_time_text(self):
        rows = self.find_elements(*BottomNavLocators.ORIGIN_TIME_ROW)
        return rows[0].find_elements(By.XPATH, ".//td")[0].text.replace(",", "")

    def move_cursor_half_hour_blocked_chart(self):
        self.explicitly_wait_for_element_to_be_clickable(20, *DashboardLocators.PASSED_CHART_LOCATION)
        chart = self.find_element(*DashboardLocators.PASSED_CHART_LOCATION)
        sz = chart.size
        gap = int(sz["width"])/30
        math.ceil(gap)
        ActionChains(self.driver).move_to_element_with_offset(chart, 4, 0).perform()
        time.sleep(0.5)
        date = self.find_element(*DashboardLocators.PASSED_CHART_DATA_DATE_TEXT).text
        block_val = self.find_element(*DashboardLocators.PASSED_CHART_BLOCKED_TEXT).text
        blocked = {}
        blocked[date] = int(block_val)
        for i in range(31):
            ActionChains(self.driver).move_by_offset(gap, 0).perform()
            time.sleep(0.1)
            date = self.find_element(*DashboardLocators.PASSED_CHART_DATA_DATE_TEXT).text
            block_val = self.find_element(*DashboardLocators.PASSED_CHART_BLOCKED_TEXT).text
            blocked[date] = int(block_val)
        return sum(blocked.values())

    def move_status_cursor_only_blocked_half_hour_chart_with_rl_blocks(self):
        self.explicitly_wait_for_element_to_be_clickable(20, *DashboardLocators.STATUS_CHART_LOCATION)
        chart = self.find_element(*DashboardLocators.STATUS_CHART_LOCATION)
        sz = chart.size
        gap = int(sz["width"]) / 30
        math.ceil(gap)
        ActionChains(self.driver).move_to_element_with_offset(chart, 8, 0).perform()
        time.sleep(0.5)
        date = self.find_element(*DashboardLocators.STATUS_CHART_DATA_DATE_TEXT).text
        block_val = self.find_element(*DashboardLocators.STATUS_CHART_ONLY_BLOCKED_TEXT).text
        rl_block_val = self.find_element(*DashboardLocators.STATUS_CHART_ONLY_BLOCKED_RATE_LIMIT_TEXT).text
        blocked = {}
        rl_blocked = {}
        blocked[date] = int(block_val)
        rl_blocked[date] = int(rl_block_val)
        for i in range(31):
            ActionChains(self.driver).move_by_offset(gap, 0).perform()
            time.sleep(0.1)
            date = self.find_element(*DashboardLocators.STATUS_CHART_DATA_DATE_TEXT).text
            block_val = self.find_element(*DashboardLocators.STATUS_CHART_ONLY_BLOCKED_TEXT).text
            rl_block_val = self.find_element(*DashboardLocators.STATUS_CHART_ONLY_BLOCKED_RATE_LIMIT_TEXT).text
            blocked[date] = int(block_val)
            rl_blocked[date] = int(rl_block_val)
        return sum(blocked.values()), sum(rl_blocked.values())

    def move_status_cursor_only_blocked_half_hour_chart_with_rl_blocks_or_acl_blocks(self):
        self.explicitly_wait_for_element_to_be_clickable(20, *DashboardLocators.STATUS_CHART_LOCATION)
        chart = self.find_element(*DashboardLocators.STATUS_CHART_LOCATION)
        sz = chart.size
        gap = int(sz["width"]) / 30
        math.ceil(gap)
        ActionChains(self.driver).move_to_element_with_offset(chart, 8, 0).perform()
        time.sleep(0.5)
        date = self.find_element(*DashboardLocators.STATUS_CHART_DATA_DATE_TEXT).text
        block_val = self.find_element(*DashboardLocators.STATUS_CHART_ONLY_BLOCKED_TEXT).text
        blocked = {}
        blocked[date] = int(block_val)
        for i in range(31):
            ActionChains(self.driver).move_by_offset(gap, 0).perform()
            time.sleep(0.1)
            date = self.find_element(*DashboardLocators.STATUS_CHART_DATA_DATE_TEXT).text
            block_val = self.find_element(*DashboardLocators.STATUS_CHART_ONLY_BLOCKED_TEXT).text
            blocked[date] = int(block_val)
        return sum(blocked.values())

    def return_day_time_with_delta(self, gap):
        d = datetime.datetime.today().utcnow() - timedelta(minutes=gap)
        return d.strftime("%y-%m-%d %H:%M")

    # takes the required gaps - from today minus gap_from to today minus gap_to
    def fill_calendar_from_to_fields(self, gap_from, gap_to):
        self.clear_field(*ViewLogLocators.CALENDAR_FROM_INPUT)
        self.clear_field(*ViewLogLocators.CALENDAR_TO_INPUT)
        from_time = self.return_day_time_with_delta(gap_from)
        to_time = self.return_day_time_with_delta(gap_to)
        self.send_value_to_field(from_time, *ViewLogLocators.CALENDAR_FROM_INPUT)
        time.sleep(0.5)
        self.send_value_to_field(to_time, *ViewLogLocators.CALENDAR_TO_INPUT)
        time.sleep(20)
        self.find_element(*ViewLogLocators.CALENDAR_APPLY_BTN).click()
        time.sleep(5)









