from selenium.webdriver.support.ui import Select


from core.base_page import *
from locators.account_locators import *


class AccountPage(BasePage):
    def navigate_to_account_page(self):
        self.navigate_to_tab("Account")

    def check_users_exist_with_action_buttons(self):
        rows = self.find_elements(*AccountLocators.SINGLE_USER_ROW)
        for row in rows:
            row.find_element(By.XPATH, "//button[@title='Edit']")
            row.find_element(By.XPATH, "//button[@title='Reset password']")
            row.find_element(By.XPATH, "//button[@title='Delete']")
        return True

    def get_user_name_from_users_manager_by_name(self, user_name):
        rows = self.find_elements(*AccountLocators.SINGLE_USER_ROW)
        return rows[1].find_element(By.XPATH, f"//td[contains(@title, '{user_name}')]").text

    def check_planet_users_exist_with_action_buttons(self):
        rows = self.find_elements(*AccountLocators.SINGLE_USER_ROW)
        for row in rows:
            row.find_element(By.XPATH, "//button[@title='Edit']")
            row.find_element(By.XPATH, "//button[@title='Reset password']")
            try:
                row.find_element(By.XPATH, "//button[@title='Delete']")
                return False
            except NoSuchElementException:
                pass
        return True

    # presses UPDATE button on existing account
    def save_existing_account(self):
        self.find_element(By.XPATH, "//button[contains(text(), 'Update')]").click()
        time.sleep(1)

    def check_update_account_enabled(self):
        return self.find_element(By.XPATH, "//button[contains(text(), 'Update')]").is_enabled()

    def save_modal_window(self):
        self.find_element(*AccountLocators.SINGLE_SIGN_ON_SAVE_BTN).click()

    @staticmethod
    def generate_random_phone_number(num: int) -> str:
        return '+' + ''.join(random.choice(string.digits) for i in range(num))

    def generate_random_email(self, num):
        return self.generate_random_string(num) + "@gmail.com"

    # presses create new user (the PLUS button on users tab)
    def press_create_new_user_btn(self):
        self.find_element(*AccountLocators.ADD_USER_BTN).click()
        time.sleep(1)

    # gets users name from default user page
    def get_user_name(self):
        return self.find_element(*AccountLocators.ACCOUNT_NAME_INPUT).get_attribute("value")

    # gets users phone number from default user page
    def get_user_phone(self):
        return self.find_element(*AccountLocators.ACCOUNT_PHONE_INPUT).get_attribute("value")

    # gets users email from default user page
    def get_user_email(self):
        paragraphs = self.find_elements(By.TAG_NAME, "p")
        return paragraphs[0].text

    # gets users access level from users page (table) by user name
    def get_user_access_level(self, user_name):
        user_row = self.get_user_data_by_name(user_name)
        row_data = user_row.find_elements(By.XPATH, ".//td")
        return row_data[3].text

    # get user phone from modal popup
    def get_user_phone_from_modal(self):
        return self.find_element(*AccountLocators.MODAL_PHONE_INPUT).get_attribute("value")

    # get user name from modal popup
    def get_user_name_from_modal(self):
        return self.find_element(*AccountLocators.MODAL_NAME_INPUT).get_attribute("value")

    # get user email from modal popup
    def get_user_email_from_modal(self):
        return self.find_element(*AccountLocators.MODAL_EMAIL_INPUT).get_attribute("value")

    # get user access level from modal popup
    def get_user_access_level_from_modal(self):
        select = Select(self.driver.find_element(*AccountLocators.MODAL_ACCESS_LEVEL_DROPDOWN))
        return select.first_selected_option.text

    # get user organization from modal popup
    def get_user_organization_from_modal(self):
        select = Select(self.driver.find_element(*AccountLocators.MODAL_ORGANIZATION_DROPDOWN))
        return select.first_selected_option.text

    # gets user row from users table by user name
    def get_user_data_by_name(self, user_name):
        rows = self.find_elements(*AccountLocators.SINGLE_USER_ROW)
        for row in rows:
            row_data = row.find_elements(By.XPATH, ".//td")
            for data in row_data:
                if data.text == user_name or user_name in data.text:
                    return row
                break

    # cleanup - executes after test has ended - deletes users that were created by the tests
    # all test user begin with TEST
    def delete_user_for_cleanup(self):
        rows = self.find_elements(*AccountLocators.SINGLE_USER_ROW)
        for i in range(len(rows)):
            rows = self.find_elements(*AccountLocators.SINGLE_USER_ROW)
            time.sleep(0.2)
            for row in rows:
                if "TEST" in row.text:
                    row.find_element(By.XPATH, ".//button[@title='Delete']").click()
                    time.sleep(0.3)
                    self.confirm_user_delete()
                    self.refresh_page()
                    self.switch_to_frame()
                    self.navigate_to_tab("Users management")
                    rows = self.find_elements(*AccountLocators.SINGLE_USER_ROW)
                    time.sleep(0.3)
                    break

    # in user management, checks if phone number is correct
    def check_correct_phone_number_in_users_list(self, user_name, phone_num) -> list:
        user_row = self.get_user_data_by_name(user_name)
        row_data = user_row.find_elements(By.XPATH, ".//td")
        return [True for data in row_data if data.text == phone_num]

    # in user management, checks if user name is correct
    def check_correct_user_name_in_users_list(self, user_name) -> list:
        user_row = self.get_user_data_by_name(user_name)
        row_data = user_row.find_elements(By.XPATH, ".//td")
        return [True for data in row_data if data.text == user_name]

    # in user management, checks if email is correct
    def check_correct_email_in_users_list(self, user_name, email) -> list:
        user_row = self.get_user_data_by_name(user_name)
        row_data = user_row.find_elements(By.XPATH, ".//td")
        return [True for data in row_data if data.text == email]

    # in user management, checks if organization is correct
    def check_correct_organization_in_users_list(self, user_name, org) -> list:
        user_row = self.get_user_data_by_name(user_name)
        row_data = user_row.find_elements(By.XPATH, ".//td")
        return [True for data in row_data if data.text == org]

    # presses edit user button in users table by user name
    def press_row_edit_btn_by_name(self, user_name):
        user_row = self.get_user_data_by_name(user_name)
        user_row.find_element(By.XPATH, ".//button[@title='Edit']").click()

    # checks if user row with specified user name exists
    def verify_row_exists_by_name(self, user_name) -> bool:
        rows = self.find_elements(*AccountLocators.SINGLE_USER_ROW)
        for row in rows:
            row_data = row.find_elements(By.XPATH, ".//td")
            for data in row_data:
                if data.text == user_name or user_name in data.text:
                    return True
        return False

    # presses dlete user button in users table by user name
    def press_row_delete_btn_by_name(self, user_name):
        user_row = self.get_user_data_by_name(user_name)
        user_row.find_element(By.XPATH, ".//button[@title='Delete']").click()

    # confirms user delet modal popup
    def confirm_user_delete(self):
        self.find_element(*AccountLocators.CONFIRM_USER_DELETE_BTN).click()

    # check user warning changes cannot be saved
    def check_save_failure_notification(self):
        if self.is_element_present(By.XPATH, "//div[@class='message-body']"):
            return self.find_element(By.XPATH, "//div[@class='message-body']").text == "Unable to save changes"

    # selects required access level by its name
    def select_access_level_by_text(self, access_name):
        select = Select(self.driver.find_element(*AccountLocators.MODAL_ACCESS_LEVEL_DROPDOWN))
        select.select_by_visible_text(access_name)

    # select user organization ny its name
    def select_organization_by_text(self, org_name):
        select = Select(self.driver.find_element(*AccountLocators.MODAL_ORGANIZATION_DROPDOWN))
        select.select_by_visible_text(org_name)

    # in modal popup
    def click_save_new_user_btn(self):
        self.find_element(*AccountLocators.SAVE_NEW_USER_BTN).click()

    def check_organization_not_in_organization_list(self, org_name):
        return [org in self.find_element(*AccountLocators.MODAL_ORGANIZATION_DROPDOWN).text for org in org_name]

    def check_access_level_not_in_access_levl_list(self, access_level):
        return access_level in self.find_element(*AccountLocators.MODAL_ORGANIZATION_DROPDOWN).text

