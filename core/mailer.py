import datetime
import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.utils import formatdate
import os
from os import listdir
from os.path import sep as sep
from os.path import isfile, join
from core.login_data_vars import *

HOME_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))


def go_mail():

    today = datetime.datetime.today().strftime("%d-%m-%Y")

    # recipients = ["yulim@reblaze.com", "shai.segev@reblaze.com", "yana@reblaze.com", "olgas@reblaze.com", "itaip@reblaze.com"]
    recipients = ["yulim@reblaze.com", "yana@reblaze.com"]

    msg = MIMEMultipart()
    msg["From"] = get_login_data("global", "global_user")
    msg["To"] = ", ".join(recipients)
    msg["Date"] = formatdate(localtime=True)
    msg["Subject"] = "Reblaze automation report for: " + today

    report_path = HOME_PATH + sep + "reports"
    files = [f for f in listdir(report_path) if isfile(join(report_path, f))]
    for i, f in enumerate(files):
        filename = f

        with open(report_path + sep + filename, "rb") as file:
            part = MIMEApplication(
                file.read(),
                Name=basename(filename))

        # After the file is closed
        part["Content-Disposition"] = 'attachment; filename="%s"' % basename(filename)
        msg.attach(part)

    screenshot_path = HOME_PATH + sep + "assets" + sep + "screenshots"
    files = [f for f in listdir(screenshot_path) if isfile(join(screenshot_path, f))]
    for i, f in enumerate(files):
        filename = f

        with open(screenshot_path + sep + filename, "rb") as file:
            part = MIMEApplication(
                file.read(),
                Name=basename(filename))

        # After the file is closed
        part["Content-Disposition"] = 'attachment; filename="%s"' % basename(filename)
        msg.attach(part)

    smtp = smtplib.SMTP("smtp.gmail.com", port=587)
    smtp.starttls()
    # ignores 2fa auth
    smtp.login(get_login_data("global", "global_user"), password=get_login_data("global", "gmail_pass"))
    smtp.sendmail(msg["From"], recipients, msg.as_string())
    smtp.close()


if __name__ == "__main__":
    go_mail()
