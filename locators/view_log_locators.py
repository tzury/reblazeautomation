from selenium.webdriver.common.by import By


class ViewLogLocators:
    DOMAIN_NAME_INPUT = (By.ID, "dyn-filter-exp")
    LOG_SINGLE_LINE = (By.XPATH, "//div[@id='request-lines-container']/div")
    SITE_LOG_DROPDOWN = (By.ID, "sites_selector_chosen")
    SITE_LOG_INPUT = (By.XPATH, "//div[@id='sites_selector_chosen']//input")
    SELECT_SITE_DROPDOWN = (By.XPATH, "//div[@id='sites_selector_chosen']")
    SELECT_SITE_INPUT = (By.XPATH, "//div[@id='sites_selector_chosen']/div/div/input")

    SEARCH_BTN = (By.ID, "do-filter-btn")
    CALENDAR_BTN = (By.ID, "dyn-filter-show-calendar")
    HISTORY_BTN = (By.ID, "dyn-filter-show-history")
    EXPORT_CDN_BTN = (By.ID, "dyn-filter-export-csv")
    HELP_BTN = (By.ID, "dyn-filter-help-btn")
    CLEAR_BTN = (By.ID, "do-clear-filter")

    PANEL_BODY = (By.XPATH, "//div[@class='panel-body']/pre")
    PANEL_FOOTER = (By.XPATH, "//div[@class='panel-footer']")
    VIEW_DETAILS_BTN = (By.XPATH, "//button[@title='View Details']")

    REQUEST_DETAILS_POPUP = (By.ID, "request-details-container")
    REQUEST_DETAILS_POPUP_HEADER = (By.XPATH, "//div[@id='request-details-container']/div[1]")
    REQUEST_DETAILS_POPUP_HEADING = (By.XPATH, "//div[@id='request-details-container']/div[2]")
    REQUEST_DETAILS_POPUP_FOOTER_FIRST_ROW = (By.XPATH, "//div[@id='request-details-container']/div[3]//tr[1]")
    REQUEST_DETAILS_POPUP_FOOTER = (By.XPATH, "//div[@id='request-details-container']/div[3]//table")
    REQUEST_DETAILS_POPUP_CLOSE_BTN = (By.XPATH, "//div[@id='request-details-modal']//div[@class='modal-header']/button")
    POPUP_HEADER = (By.ID, "request-details-container")
    UPLOAD_BTN = (By.XPATH, "//span[@title='Upload']")
    DOWNLOAD_BTN = (By.XPATH, "//span[@title='Download']")

    SEARCH_HISTORY_LIST = (By.ID, "search-history-list")
    SEARCH_HISTORY_SINGLE_ROW = (By.XPATH, "//div[@id='search-history-list']//li")

    DATE_RANGE_INPUT = (By.ID, "date-range-input")
    OPEN_CALENDAR_BTN = (By.ID, "initiate-calendar-view")
    CALENDAR = (By.XPATH, "//div[contains(@class, 'daterangepicker')]")
    CALENDAR_CANCEL_BTN = (By.XPATH, "//button[contains(text(), 'Cancel')]")
    CALENDAR_FROM_INPUT = (By.XPATH, "//input[@name='daterangepicker_start']")
    CALENDAR_TO_INPUT = (By.XPATH, "//input[@name='daterangepicker_end']")
    CALENDAR_TABLE = (By.CLASS_NAME, "calendar-table")
    CALENDAR_APPLY_BTN = (By.XPATH, "//button[contains(text(), 'Apply')]")

    HELP_POPUP = (By.ID, "cert-info-modal-body")
    HELP_POPUP_CLOSE_BTN = (By.XPATH, "//div[@id='SearchInfoModal']//button[@class='close']")

    ROWS_LIMIT_DROPDOWN = (By.ID, "limit-title")
    RESULTS_NUMBER = (By.CLASS_NAME, "results-number")