from selenium.webdriver.common.by import By


class AccountLocators:
    ACCOUNT_NAME_DIV = (By.ID, "full-name-form-element")
    ACCOUNT_NAME_INPUT = (By.XPATH, "//div[@id='full-name-form-element']//input")
    ACCOUNT_EMAIL_INPUT = (By.XPATH, "//p[contains(text(), 'qaautomation@reblaze.com')]")
    ACCOUNT_PLANET_EMAIL_INPUT = (By.XPATH, "//p[contains(text(), 'rebauto1@yahoo.com')]")
    ACCOUNT_EDITOR_EMAIL_INPUT = (By.XPATH, "//p[contains(text(), 'rebauto2@yahoo.com')]")
    ACCOUNT_ORG_ADMIN_EMAIL_INPUT = (By.XPATH, "//p[contains(text(), 'rebauto3@yahoo.com')]")
    ACCOUNT_REB_ADMIN_EMAIL_INPUT = (By.XPATH, "//p[contains(text(), 'rebauto4@yahoo.com')]")
    ACCOUNT_PHONE_DIV = (By.ID, "mobile-form-element")
    ACCOUNT_PHONE_INPUT = (By.XPATH, "//div[@id='mobile-form-element']//input")
    ACCOUNT_ORGANIZATION_INPUT = (By.XPATH, "//p[contains(text(), 'Reblaze default organization')]")
    ACCOUNT_PLANET_ORGANIZATION_INPUT = (By.XPATH, "//p[contains(text(), 'rebauto1')]")
    ACCOUNT_ORG_AMIN_ORGANIZATION_INPUT = (By.XPATH, "//p[contains(text(), 'rebauto3')]")
    ACCOUNT_ADMIN_ORGANIZATION_INPUT = (By.XPATH, "//p[contains(text(), 'Reblaze default organization')]")
    ACCOUNT_EDITOR_ORGANIZATION_INPUT = (By.XPATH, "//p[contains(text(), 'rbzauto2')]")
    SHOW_QR_BTN = (By.XPATH, "//label[contains(text(), 'show QR Code')]")
    SHOW_API_KEY_BTN = (By.XPATH, "//label[contains(text(), 'show api key')]")
    HIDE_QR_BTN = (By.XPATH, "//label[contains(text(), 'hide QR Code')]")
    HIDE_API_KEY_BTN = (By.XPATH, "//label[contains(text(), 'hide api key')]")

    SINGLE_USER_ROW = (By.TAG_NAME, "tr")
    ADD_USER_BTN = (By.XPATH, "//a[@data-tooltip='Add User']")
    SAVE_NEW_USER_BTN = (By.XPATH, "//button[contains(text(), 'Add')]")
    CONFIRM_USER_DELETE_BTN = (By.XPATH, "//button[contains(text(), 'Delete User')]")

    ENABLED_BTN = (By.XPATH, "//label[contains(text(), 'Enabled')]")
    SINGLE_SIGN_ON_SAVE_BTN = (By.XPATH, "//button[contains(text(), 'Save')]")

    MODAL_PHONE_INPUT = (By.XPATH, "//div[@class='modal-card']//div[@id='mobile-form-element']//input")
    MODAL_NAME_INPUT = (By.XPATH, "//div[@class='modal-card']//div[@id='full-name-form-element']//input")
    MODAL_EMAIL_INPUT = (By.XPATH, "//div[@class='modal-card']//div[@id='email-form-element']//input")
    MODAL_ACCESS_LEVEL_DROPDOWN = (By.XPATH, "//div[@id='acl-form-element']//select")
    MODAL_ORGANIZATION_DROPDOWN = (By.XPATH, "//div[@id='org-form-element']//select")
