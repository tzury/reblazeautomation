import pytest
from pages.acl_page import *
from locators.acl_page_locators import *
import time


@pytest.mark.usefixtures("test_setup", "cs_login", "cs_site")
@pytest.mark.all_modules
@pytest.mark.custom_sig_tests
class TestCustomSignatures:

    def test_custom_signature_page(self, cs_site):
        cs_page = AclPage(self.driver)
        cs_page.navigate_to_tab("Profiles")
        cs_site["path"].append("cs.test")
        cs_page.wait_for_acl_page_to_load()
        assert not cs_page.is_element_present(*AclMainLocators.APPEND_CONDITION_BTN)
        cs_page.find_element(*AclMainLocators.CUSTOM_SIGNATURE_BTN).click()
        assert cs_page.is_element_present(By.LINK_TEXT, "Save")
        assert cs_page.is_element_present(By.LINK_TEXT, "Duplicate")
        assert cs_page.is_element_present(By.LINK_TEXT, "Delete")
        assert cs_page.is_element_present(By.LINK_TEXT, "CS Arg Value")
        assert cs_page.is_element_present(*AclMainLocators.APPEND_CONDITION_BTN)

    def test_can_duplicate_cs(self, cs_site):
        cs_page = AclPage(self.driver)
        cs_page.navigate_to_tab("Profiles")
        cs_site["path"].append("cs.test")
        cs_page.wait_for_acl_page_to_load()
        cs_page.click_custom_signatures_btn()
        cs_page.find_element(By.LINK_TEXT, "CS Arg Value").click()
        text_before = cs_page.get_waf_table_text()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        new_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_name, *AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_page.press_save()
        cs_page.refresh_page()
        cs_page.click_custom_signatures_btn()
        cs_page.find_element(By.LINK_TEXT, new_name).click()
        text_after = cs_page.get_waf_table_text()
        assert text_after == text_before
        assert cs_page.find_element(*AclMainLocators.CUSTOM_SIG_NAME_INPUT).get_attribute("value") == new_name

    def test_can_delete_cs(self, cs_site):
        cs_page = AclPage(self.driver)
        cs_page.navigate_to_tab("Profiles")
        cs_site["path"].append("cs.test")
        cs_page.wait_for_acl_page_to_load()
        cs_page.click_custom_signatures_btn()
        cs_page.find_element(By.LINK_TEXT, "CS Arg Value").click()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        new_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_name, *AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_page.press_save()
        cs_page.refresh_page()
        cs_page.click_custom_signatures_btn()
        cs_page.find_element(By.LINK_TEXT, new_name).click()
        cs_page.press_delete()
        cs_page.refresh_page()
        assert not cs_page.is_element_present(By.LINK_TEXT, new_name)

    @pytest.mark.parametrize("url, site, path,  policy, operation, match, method, fltr",
                             [("csg_base_url", "csg.test", "/csg", "ACL Deny All", "Deny", "Custom Signature", "asn", "Autonomous System Number (ASN)"),
                              ("csa_base_url", "csa.test", "/csa", "ACL Deny All", "Deny", "Custom Signature", "POST", "Request Method"),
                              ("csb_base_url", "csb.test", "/csb", "ACL Deny All", "Deny", "Custom Signature", "PUT", "Request Method"),
                              ("csc_base_url", "csc.test", "/csc", "ACL Deny All", "Deny", "Custom Signature", "GET", "Request Method"),
                              ("csd_base_url", "csd.test", "/csd", "ACL Deny All", "Deny", "Custom Signature", "DELETE", "Request Method"),
                              ("cse_base_url", "cse.test", "/cse", "ACL Deny All", "Deny", "Custom Signature", "cse", "Request URI"),
                              ("csf_base_url", "csf.test", "/csf", "ACL Deny All", "Deny", "Custom Signature", "csf-test", "Host Name")
                              ])
    def test_can_add_asn_method_host_cs_to_acl_policy(self, url, site, path, cs_site, policy, operation, match, method, fltr):
        cs_page = AclPage(self.driver)
        cs_page.navigate_to_tab("Profiles")
        asn = ""
        if "ASN" in fltr:
            asn = cs_page.get_my_asn()
            method = asn
            if "Google" in asn:
                method = "AS15169 GOOGLE"
        cs_site["path"].append(site)
        cs_page.wait_for_acl_page_to_load()
        cs_page.click_custom_signatures_btn()
        cs_page.find_element(By.LINK_TEXT, "CS Arg Value").click()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(cs_name, *AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_page.find_element(*AclMainLocators.CUSTOM_SIG_DELETE_OPTION_BTN).click()
        cs_page.select_custom_sig_filter(fltr)
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_VALUE_INPUT)
        cs_page.send_value_to_field(method, *AclMainLocators.CUSTOM_SIG_VALUE_INPUT)
        time.sleep(0.5)
        cs_page.press_save()
        cs_page.click_acl_policies_btn()
        cs_page.find_element(By.LINK_TEXT, "ACL Deny All").click()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        cs_page.delete_all_cs_rules_in_policy()
        cs_page.select_new_acl_rule_operation_by_name(operation)
        cs_page.select_new_acl_rule_match_by_name(match)
        cs_page.fill_custom_sig_name_in_acl_policy(cs_name)
        cs_page.press_add()
        cs_page.press_save()
        cs_page.find_element(*AclMainLocators.PROFILES_BTN).click()
        cs_page.find_element(By.LINK_TEXT, "ACL Default").click()
        cs_page.press_duplicate()
        cs_page.delete_all_policies_in_a_profile()
        cs_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        cs_page.select_acl_policy_site_by_name(new_name)
        cs_page.press_add()
        cs_page.press_save()
        cs_page.attach_profile_to_url_by_path_and_profile_name(site, "Test", new_prof_name)
        if method == "cse" or method == "csf-test" or "Hot" in method or "GOOGLE" in method:
            method = "GET"
        response = cs_page.query(self.data[url], path=path, method=method)
        assert response.status_code == 200
        cs_page.publish_planet(21)
        response = cs_page.query(self.data[url], path=path, method=method)
        assert response.status_code == 403

    @pytest.mark.parametrize("url, site, path,  policy, operation, match, value, fltr",
                             [("csh_base_url", "csh.test", "/csh", "ACL Deny All", "Deny", "Custom Signature", "arg",
                               "Args"),
                              ("csi_base_url", "csi.test", "/csi", "ACL Deny All", "Deny", "Custom Signature", "tst",
                               "Args Names")
                              ])
    def test_can_add_args_cs_to_acl_policy(self, url, site, path, cs_site, policy, operation, match, value, fltr):
        cs_page = AclPage(self.driver)
        cs_page.navigate_to_tab("Profiles")
        cs_site["path"].append(site)
        params = ""
        if fltr == "Args Names":
            params = {value: "bar"}
        cs_page.wait_for_acl_page_to_load()
        cs_page.click_custom_signatures_btn()
        cs_page.find_element(By.LINK_TEXT, "CS Arg Value").click()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(cs_name, *AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_page.find_element(*AclMainLocators.CUSTOM_SIG_DELETE_OPTION_BTN).click()
        element = cs_page.find_element(*AclMainLocators.CUSTOM_SIG_FILTER_INPUT)
        element.send_keys(" ")
        time.sleep(0.3)
        cs_page.send_value_to_field(fltr, *AclMainLocators.CUSTOM_SIG_FILTER_INPUT)
        cs_page.press_down_arrow()
        if fltr == "Args":
            params = {"foo": value}
            time.sleep(0.3)
            cs_page.press_up_arrow()
            time.sleep(0.3)
        cs_page.press_enter()
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_VALUE_INPUT)
        cs_page.send_value_to_field(value, *AclMainLocators.CUSTOM_SIG_VALUE_INPUT)
        time.sleep(0.5)
        cs_page.press_save()
        cs_page.click_acl_policies_btn()
        cs_page.find_element(By.LINK_TEXT, "ACL Deny All").click()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        time.sleep(0.5)
        cs_page.delete_all_cs_rules_in_policy()
        cs_page.select_new_acl_rule_operation_by_name(operation)
        cs_page.select_new_acl_rule_match_by_name(match)
        cs_page.fill_custom_sig_name_in_acl_policy(cs_name)
        cs_page.press_add()
        cs_page.press_save()
        cs_page.find_element(*AclMainLocators.PROFILES_BTN).click()
        cs_page.find_element(By.LINK_TEXT, "ACL Default").click()
        cs_page.press_duplicate()
        cs_page.delete_all_policies_in_a_profile()
        cs_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        cs_page.select_acl_policy_site_by_name(new_name)
        cs_page.press_add()
        cs_page.press_save()
        cs_page.attach_profile_to_url_by_path_and_profile_name(site, "Test", new_prof_name)
        response = cs_page.query(self.data[url], path=path, params=params)
        assert response.status_code == 200
        cs_page.publish_planet(21)
        response = cs_page.query(self.data[url], path=path, params=params)
        assert response.status_code == 403

    @pytest.mark.parametrize("url, site, path,  policy, operation, match, value, fltr1, fltr2",
                             [("csj_base_url", "csj.test", "/csj", "ACL Deny All", "Deny", "Custom Signature", "POST",
                               "Args Names", "Request Method")])
    def test_can_add_two_tags_to_cs(self, url, site, path, cs_site, policy, operation, match, value, fltr1, fltr2):
        cs_page = AclPage(self.driver)
        cs_page.navigate_to_tab("Profiles")
        cs_site["path"].append(site)
        params = ""
        if fltr1 == "Args Names":
            params = {value: "bar"}
        cs_page.wait_for_acl_page_to_load()
        cs_page.click_custom_signatures_btn()
        cs_page.find_element(By.LINK_TEXT, "CS Arg Value").click()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(cs_name, *AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_page.find_element(*AclMainLocators.CUSTOM_SIG_DELETE_OPTION_BTN).click()
        cs_page.select_custom_sig_filter(fltr1)
        cs_page.select_custom_sig_filter(fltr2)
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_VALUE_INPUT)
        cs_page.send_value_to_field(value, *AclMainLocators.CUSTOM_SIG_VALUE_INPUT)
        time.sleep(0.5)
        cs_page.press_save()
        cs_page.click_acl_policies_btn()
        cs_page.find_element(By.LINK_TEXT, "ACL Deny All").click()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        time.sleep(0.5)
        cs_page.delete_all_cs_rules_in_policy()
        cs_page.select_new_acl_rule_operation_by_name(operation)
        cs_page.select_new_acl_rule_match_by_name(match)
        cs_page.fill_custom_sig_name_in_acl_policy(cs_name)
        cs_page.press_add()
        cs_page.press_save()
        cs_page.find_element(*AclMainLocators.PROFILES_BTN).click()
        cs_page.find_element(By.LINK_TEXT, "ACL Default").click()
        cs_page.press_duplicate()
        cs_page.delete_all_policies_in_a_profile()
        cs_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        cs_page.select_acl_policy_site_by_name(new_name)
        cs_page.press_add()
        cs_page.press_save()
        cs_page.attach_profile_to_url_by_path_and_profile_name(site, "Test", new_prof_name)
        response = cs_page.query(self.data[url], path=path, params=params)
        assert response.status_code == 200
        response = cs_page.query(self.data[url], path=path, method=value)
        assert response.status_code == 200
        cs_page.publish_planet(21)
        response = cs_page.query(self.data[url], path=path, params=params)
        assert response.status_code == 403
        response = cs_page.query(self.data[url], path=path, method=value)
        assert response.status_code == 403

    @pytest.mark.parametrize("url, site, path,  policy, operation, match, value1, value2, fltr1, fltr2",
                             [("csk_base_url", "csk.test", "/csk", "ACL Deny All", "Deny", "Custom Signature", "arg_name", "POST",
                               "Args Names", "Request Method")])
    def test_and_condition_in_cs(self, url, site, path, cs_site, policy, operation, match, value1, value2, fltr1, fltr2):
        cs_page = AclPage(self.driver)
        cs_page.navigate_to_tab("Profiles")
        cs_site["path"].append(site)
        params = ""
        if fltr1 == "Args Names":
            params = {value1: "bar"}
        cs_page.wait_for_acl_page_to_load()
        cs_page.click_custom_signatures_btn()
        cs_page.find_element(By.LINK_TEXT, "CS Arg Value").click()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(cs_name, *AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_page.find_element(*AclMainLocators.CUSTOM_SIG_DELETE_OPTION_BTN).click()
        cs_page.select_custom_sig_filter(fltr1)
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_VALUE_INPUT)
        cs_page.send_value_to_field(value1, *AclMainLocators.CUSTOM_SIG_VALUE_INPUT)
        time.sleep(0.5)
        cs_page.click_custom_sig_append_condition_btn()
        cs_page.select_second_custom_sig_filter(fltr2)
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_SECOND_VALUE_INPUT)
        cs_page.send_value_to_field(value2, *AclMainLocators.CUSTOM_SIG_SECOND_VALUE_INPUT)
        time.sleep(0.5)
        cs_page.press_save()
        cs_page.click_acl_policies_btn()
        cs_page.find_element(By.LINK_TEXT, "ACL Deny All").click()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        time.sleep(0.5)
        cs_page.delete_all_cs_rules_in_policy()
        cs_page.select_new_acl_rule_operation_by_name(operation)
        cs_page.select_new_acl_rule_match_by_name(match)
        cs_page.fill_custom_sig_name_in_acl_policy(cs_name)
        cs_page.press_add()
        cs_page.press_save()
        cs_page.find_element(*AclMainLocators.PROFILES_BTN).click()
        cs_page.find_element(By.LINK_TEXT, "ACL Default").click()
        cs_page.press_duplicate()
        cs_page.delete_all_policies_in_a_profile()
        cs_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        cs_page.select_acl_policy_site_by_name(new_name)
        cs_page.press_add()
        cs_page.press_save()
        cs_page.attach_profile_to_url_by_path_and_profile_name(site, "Test", new_prof_name)
        response = cs_page.query(self.data[url], path=path, params=params)
        assert response.status_code == 200
        response = cs_page.query(self.data[url], path=path, method=value2)
        assert response.status_code == 200
        response = cs_page.query(self.data[url], path=path, method=value2, params=params)
        assert response.status_code == 200
        cs_page.publish_planet(21)
        response = cs_page.query(self.data[url], path=path, params=params)
        assert response.status_code == 200
        response = cs_page.query(self.data[url], path=path, method=value2)
        assert response.status_code == 200
        response = cs_page.query(self.data[url], path=path, method=value2, params=params)
        assert response.status_code == 403

    @pytest.mark.parametrize("url, site, path,  policy, operation, match, value1, value2, fltr1, fltr2",
                             [("csl_base_url", "csl.test", "/csl", "ACL Deny All", "Deny", "Custom Signature",
                               "arg_or", "PUT",
                               "Args Names", "Request Method")])
    def test_or_condition_in_cs(self, url, site, path, cs_site, policy, operation, match, value1, value2, fltr1,
                                 fltr2):
        cs_page = AclPage(self.driver)
        cs_page.navigate_to_tab("Profiles")
        cs_site["path"].append(site)
        params = ""
        if fltr1 == "Args Names":
            params = {value1: "bar"}
        cs_page.wait_for_acl_page_to_load()
        cs_page.click_custom_signatures_btn()
        cs_page.find_element(By.LINK_TEXT, "CS Arg Value").click()
        cs_page.press_duplicate()
        cs_page.click_custom_sig_and_or_switch()
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(cs_name, *AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_page.find_element(*AclMainLocators.CUSTOM_SIG_DELETE_OPTION_BTN).click()
        cs_page.select_custom_sig_filter(fltr1)
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_VALUE_INPUT)
        cs_page.send_value_to_field(value1, *AclMainLocators.CUSTOM_SIG_VALUE_INPUT)
        time.sleep(0.5)
        cs_page.click_custom_sig_append_condition_btn()
        cs_page.select_second_custom_sig_filter(fltr2)
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_SECOND_VALUE_INPUT)
        cs_page.send_value_to_field(value2, *AclMainLocators.CUSTOM_SIG_SECOND_VALUE_INPUT)
        time.sleep(0.5)
        cs_page.press_save()
        cs_page.click_acl_policies_btn()
        cs_page.find_element(By.LINK_TEXT, "ACL Deny All").click()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        time.sleep(0.5)
        cs_page.delete_all_cs_rules_in_policy()
        cs_page.select_new_acl_rule_operation_by_name(operation)
        cs_page.select_new_acl_rule_match_by_name(match)
        cs_page.fill_custom_sig_name_in_acl_policy(cs_name)
        cs_page.press_add()
        cs_page.press_save()
        cs_page.find_element(*AclMainLocators.PROFILES_BTN).click()
        cs_page.find_element(By.LINK_TEXT, "ACL Default").click()
        cs_page.press_duplicate()
        cs_page.delete_all_policies_in_a_profile()
        cs_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        cs_page.select_acl_policy_site_by_name(new_name)
        cs_page.press_add()
        cs_page.press_save()
        cs_page.attach_profile_to_url_by_path_and_profile_name(site, "Test", new_prof_name)
        response = cs_page.query(self.data[url], path=path, params=params)
        assert response.status_code == 200
        response = cs_page.query(self.data[url], path=path, method=value2)
        assert response.status_code == 200
        response = cs_page.query(self.data[url], path=path, method=value2, params=params)
        assert response.status_code == 200
        cs_page.publish_planet(21)
        response = cs_page.query(self.data[url], path=path, params=params)
        assert response.status_code == 403
        response = cs_page.query(self.data[url], path=path, method=value2)
        assert response.status_code == 403
        response = cs_page.query(self.data[url], path=path, method=value2, params=params)
        assert response.status_code == 403

    @pytest.mark.parametrize("url, site, path,  policy, operation, match, value, fltr",
                             [("csm_base_url", "csm.test", "/csm?aaa=nnn", "ACL Deny All", "Deny", "Custom Signature", "aaa=nnn",
                               "Query String")])
    def test_can_add_query_string_cs_to_acl_policy(self, url, site, path, cs_site, policy, operation, match, value,
                                                      fltr):
        cs_page = AclPage(self.driver)
        cs_page.navigate_to_tab("Profiles")
        cs_site["path"].append(site)
        cs_page.wait_for_acl_page_to_load()
        cs_page.click_custom_signatures_btn()
        cs_page.find_element(By.LINK_TEXT, "CS Arg Value").click()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(cs_name, *AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_page.find_element(*AclMainLocators.CUSTOM_SIG_DELETE_OPTION_BTN).click()
        cs_page.select_custom_sig_filter(fltr)
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_VALUE_INPUT)
        cs_page.send_value_to_field(value, *AclMainLocators.CUSTOM_SIG_VALUE_INPUT)
        time.sleep(0.5)
        cs_page.press_save()
        cs_page.click_acl_policies_btn()
        cs_page.find_element(By.LINK_TEXT, "ACL Deny All").click()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        cs_page.delete_all_cs_rules_in_policy()
        cs_page.select_new_acl_rule_operation_by_name(operation)
        cs_page.select_new_acl_rule_match_by_name(match)
        cs_page.fill_custom_sig_name_in_acl_policy(cs_name)
        cs_page.press_add()
        cs_page.press_save()
        cs_page.find_element(*AclMainLocators.PROFILES_BTN).click()
        cs_page.find_element(By.LINK_TEXT, "ACL Default").click()
        cs_page.press_duplicate()
        cs_page.delete_all_policies_in_a_profile()
        cs_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        cs_page.select_acl_policy_site_by_name(new_name)
        cs_page.press_add()
        cs_page.press_save()
        cs_page.attach_profile_to_url_by_path_and_profile_name(site, "Test", new_prof_name)
        response = cs_page.query(self.data[url], path=path)
        assert response.status_code == 200
        cs_page.publish_planet(21)
        response = cs_page.query(self.data[url], path=path)
        assert response.status_code == 403

    @pytest.mark.parametrize("url, site, path,  policy, operation, match, value, fltr",
                             [("csn_base_url", "csn.test", "/csn", "ACL Deny All", "Deny", "Custom Signature",
                               "user agent", "User Agent")])
    def test_can_add_user_agent_cs_to_acl_policy(self, url, site, path, cs_site, policy, operation, match, value,
                                                      fltr):
        cs_page = AclPage(self.driver)
        cs_page.navigate_to_tab("Profiles")
        response = cs_page.query(self.data[url])
        user_agent = response.request.headers["User-Agent"]
        value = user_agent
        cs_site["path"].append(site)
        cs_page.wait_for_acl_page_to_load()
        cs_page.click_custom_signatures_btn()
        cs_page.find_element(By.LINK_TEXT, "CS Arg Value").click()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(cs_name, *AclMainLocators.CUSTOM_SIG_NAME_INPUT)
        cs_page.find_element(*AclMainLocators.CUSTOM_SIG_DELETE_OPTION_BTN).click()
        cs_page.select_custom_sig_filter(fltr)
        cs_page.clear_field(*AclMainLocators.CUSTOM_SIG_VALUE_INPUT)
        cs_page.send_value_to_field(value, *AclMainLocators.CUSTOM_SIG_VALUE_INPUT)
        time.sleep(0.5)
        cs_page.press_save()
        cs_page.click_acl_policies_btn()
        cs_page.find_element(By.LINK_TEXT, "ACL Deny All").click()
        cs_page.press_duplicate()
        cs_page.clear_field(*AclMainLocators.ADD_POLICY_NAME_INPUT)
        new_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_name, *AclMainLocators.ADD_POLICY_NAME_INPUT)
        cs_page.delete_all_cs_rules_in_policy()
        cs_page.select_new_acl_rule_operation_by_name(operation)
        cs_page.select_new_acl_rule_match_by_name(match)
        cs_page.fill_custom_sig_name_in_acl_policy(cs_name)
        cs_page.press_add()
        cs_page.press_save()
        cs_page.find_element(*AclMainLocators.PROFILES_BTN).click()
        cs_page.find_element(By.LINK_TEXT, "ACL Default").click()
        cs_page.press_duplicate()
        cs_page.delete_all_policies_in_a_profile()
        cs_page.clear_field(*AclMainLocators.ACL_DEFAULT_INPUT)
        new_prof_name = "TEST" + cs_page.generate_random_string(10)
        cs_page.send_value_to_field(new_prof_name, *AclMainLocators.ACL_DEFAULT_INPUT)
        cs_page.select_acl_policy_site_by_name(new_name)
        cs_page.press_add()
        cs_page.press_save()
        cs_page.attach_profile_to_url_by_path_and_profile_name(site, "Test", new_prof_name)
        response = cs_page.query(self.data[url], path=path)
        assert response.status_code == 200
        cs_page.publish_planet(21)
        response = cs_page.query(self.data[url], path=path)
        assert response.status_code == 403
