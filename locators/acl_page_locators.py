from selenium.webdriver.common.by import By


class AclMainLocators:
    ACL_PAGE_LABEL = (By.XPATH, "//h4[contains(text(), 'Security Profiles Settings')]")
    LEFT_PROFILES_LIST = (By.ID, "profiles-list")
    LEFT_PROFILES_RULES = (By.XPATH, "//div[@id='profiles-list']//tr")
    PROFILES_BTN = (By.XPATH, "//button[contains(text(), 'Profiles')]")
    ACL_POLICIES_BTN = (By.XPATH, "//button[contains(text(), 'ACL Policies')]")
    WAF_POLICIES_BTN = (By.XPATH, "//button[contains(text(), 'WAF/IPS Policies')]")
    CUSTOM_SIGNATURE_BTN = (By.XPATH, "//button[contains(text(), 'Custom Signature')]")
    CREATE_NEW_BTN = (By.XPATH, "//button[contains(text(), 'Create New')]")
    ACL_DEFAULT_INPUT = (By.ID, "linked-profile-name")
    LINK_POLICY_SELECT = (By.ID, "new-policy-name")
    ADD_POLICY_BTN = (By.XPATH, "//button[contains(text(), 'Add')]")
    ADD_POLICY_NAME_INPUT = (By.ID, "acl-policy-name")
    NEW_RULE_OPERATION_SELECT = (By.ID, "new-rule-operation")
    NEW_RULE_MATCH_SELECT = (By.ID, "new-rule-match")
    NEW_RULE_VALUE_TEXT = (By.ID, "new-rule-value")
    WAF_POLICY_NAME_INPUT = (By.ID, "waf-policy-name")
    CUSTOM_SIG_NAME_INPUT = (By.XPATH, "//div[@id='right-pane']/div/div/input")
    WAF_RIGHT_PANE = (By.ID, "right-pane")
    WAF_MAX_NUM_ARGS = (By.ID, "max-num-args")
    WAF_MAX_NAME_LEN_ARGS = (By.ID, "max-args-name-length")
    WAF_WHITELIST_NAMES_TEXT = (By.ID, "whitelisted-args-names")
    WAF_WHITELIST_IDS_TEXT = (By.ID, "whitelisted-rule-ids")
    WAF_INJECTION_TOGGLE = (By.XPATH, "//div[@data-module-name='base-injection']")
    SIGNATURE_NAME_INPUT = (By.XPATH, "//input[@value='New Custom Signature']")
    APPEND_CONDITION_BTN = (By.XPATH, "//button[contains(text(), 'Append Condition')]")
    LINKED_POLICIES_TABLE = (By.XPATH, "//div[@id='profile-details']//tbody")
    POLICY_DROPDOWN = (By.ID, "new_policy_name_chosen")
    SELECT_POLICY_INPUT = (By.XPATH, "//div[@id='new_policy_name_chosen']/div/div/input")
    ACL_POLICIY_PROFILE_LIST = (By.CLASS_NAME, "profiles-list")
    CUSTOM_SIG_CHOSEN_CHOICES = (By.CLASS_NAME, "chosen-choices")
    CUSTOM_SIG_DELETE_OPTION_BTN = (By.CLASS_NAME, "search-choice-close")
    CUSTOM_SIG_FILTER_INPUT = (By.XPATH, "//div[@id='right-pane']/div[2]/div[2]/div//input")
    CUSTOM_SIG_SECOND_FILTER_INPUT = (By.XPATH, "//div[@id='right-pane']/div[2]/div[4]/div//input")
    CUSTOM_SIG_VALUE_INPUT = (By.XPATH, "//div[@id='right-pane']/div[2]/div[2]/textarea")
    CUSTOM_SIG_SECOND_VALUE_INPUT = (By.XPATH, "//div[@id='right-pane']/div[2]/div[4]/textarea")
    CLOSE_SUCCESS_NOTIFICATION_BTN = (By.XPATH, "//div[@class='ui-pnotify ']/div/div[@class='ui-pnotify-closer']")
    CS_RULES_DELETE_BUTTON = (By.XPATH, "//button[@title='Delete rule']")
    POLICIES_DELETE_BUTTON = (By.XPATH, "//button[@title='Delete Policy']")
    CUSTOM_SIG_AND_OR_SWITCH = (By.XPATH, "//div[contains(@class, 'switch-animate')]")
