import pytest
from pages.dashboard_page import DashboardPage
from locators.dashboard_locators import DashboardLocators
from locators.dashboard_locators import BottomNavLocators
from locators.left_nav_locators import LeftNavLocators
from pages.login_page import *
import time


@pytest.mark.usefixtures("test_setup", "general_login")
@pytest.mark.all_modules
@pytest.mark.general
class TestLogin:

    def test_can_login(self):
        dashboard = DashboardPage(self.driver)
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.is_element_present(*LeftNavLocators.NAV_BAR)

    def test_can_logout(self):
        dashboard = DashboardPage(self.driver)
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.is_element_present(*LeftNavLocators.NAV_BAR)
        assert not dashboard.is_element_present(By.XPATH, "//button[contains(text(), 'Log In')]")
        dashboard.find_element(*LeftNavLocators.LOGOUT_BTN).click()
        assert not dashboard.is_element_present(*LeftNavLocators.NAV_BAR)
        assert dashboard.is_element_present(By.XPATH, "//button[contains(text(), 'Log In')]")

    def test_can_logout_and_relogin(self):
        dashboard = DashboardPage(self.driver)
        dashboard.wait_for_dashboard_to_load()
        assert dashboard.is_element_present(*LeftNavLocators.NAV_BAR)
        assert not dashboard.is_element_present(By.XPATH, "//button[contains(text(), 'Log In')]")
        dashboard.find_element(*LeftNavLocators.LOGOUT_BTN).click()
        assert not dashboard.is_element_present(*LeftNavLocators.NAV_BAR)
        assert dashboard.is_element_present(By.XPATH, "//button[contains(text(), 'Log In')]")
        login = LoginPage(self.driver)
        login.login_with_diff_data()
        assert dashboard.is_element_present(*LeftNavLocators.NAV_BAR)
        assert not dashboard.is_element_present(By.XPATH, "//button[contains(text(), 'Log In')]")

    def test_left_nav_bar_buttons_exist(self):
        dashboard = DashboardPage(self.driver)
        dashboard.wait_for_dashboard_to_load()
        dashboard.explicitly_wait_for_element(10, *LeftNavLocators.ERROR_PAGES_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.ERROR_PAGES_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.API_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.DNS_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.SSL_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.TAG_RULES_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.ACCOUNT_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.ARGS_ANALYSIS_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.BACKEND_SERVICES_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.CLOUD_FUNCTIONS_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.DYNAMIC_RULES_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.PLANET_OVERVIEW_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.PROFILES_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.QUARANTINED_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.RATE_LIMITING_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.SUPPORT_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.VIEW_LOG_BTN)
        assert dashboard.is_element_present(*LeftNavLocators.WEB_PROXY_BTN)


@pytest.mark.usefixtures("test_setup")
@pytest.mark.all_modules
@pytest.mark.general
class TestNegativeLogin:
    def test_cannot_login_with_bad_email(self):
        login = LoginPage(self.driver)
        login.login_with_diff_data(user_name="fgsdkfg@gmail.com")
        assert login.find_element(*DashboardLocators.LOGIN_ERROR_MSG).is_displayed()
        assert login.find_element(*DashboardLocators.LOGIN_ERROR_MSG).text == "Authentication Error"
        assert "signin" in login.get_current_url()

    def test_cannot_login_with_bad_password(self):
        login = LoginPage(self.driver)
        login.login_with_diff_data(password="sdgwtye")
        assert login.find_element(*DashboardLocators.LOGIN_ERROR_MSG).is_displayed()
        assert login.find_element(*DashboardLocators.LOGIN_ERROR_MSG).text == "Authentication Error"
        assert "signin" in login.get_current_url()

    def test_cannot_login_with_bad_otp(self):
        login = LoginPage(self.driver)
        login.login_with_diff_data(otp="sdgwtye")
        assert login.find_element(*DashboardLocators.LOGIN_ERROR_MSG).is_displayed()
        assert login.find_element(*DashboardLocators.LOGIN_ERROR_MSG).text == "Authentication Error"
        assert "signin" in login.get_current_url()
