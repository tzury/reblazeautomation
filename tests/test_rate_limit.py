import pytest
from pages.rate_limit_page import *
from core.cleanup import *
from locators.rate_limit_locators import *
import time


@pytest.mark.usefixtures("test_setup", "rl_login")
@pytest.mark.all_modules
@pytest.mark.rate_limit_tests
class TestRateLimit:

    def test_can_navigate_to_rate_limit_page(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.navigate_to_tab("Rate Limiting")
        assert "ratelimit" in rl_page.get_current_url()

    def test_rate_limit_page_components_exist(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.navigate_to_tab("Rate Limiting")
        rl_page.switch_to_frame()
        assert rl_page.is_element_present(*RateLimitMainPageLocators.RATE_LIMIT_LABEL)
        assert rl_page.is_element_present(*RateLimitMainPageLocators.RULES_TAB)
        assert rl_page.is_element_present(*RateLimitMainPageLocators.REDIS_TAB)
        assert rl_page.is_element_present(*RateLimitMainPageLocators.NEW_RULE_BTN)
        assert rl_page.is_element_present(*RateLimitMainPageLocators.SEARCH_FIELD)

    def test_can_open_new_rule_form(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.switch_to_default_content()
        rl_page.navigate_to_tab("Rate Limiting")
        rl_page.switch_to_frame()
        rl_page.find_element(*RateLimitMainPageLocators.NEW_RULE_BTN).click()
        assert "/ratelimit/form" in rl_page.get_current_url()

    def test_rate_limit_form_components_exist(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        assert rl_page.is_element_present(*RateLimitFormLocators.NEW_RATE_LIMIT_LABEL)
        assert rl_page.is_element_present(*RateLimitFormLocators.INCLUDE_LABEL)
        assert rl_page.is_element_present(*RateLimitFormLocators.EXCLUDE_LABEL)
        assert rl_page.is_element_present(*RateLimitFormLocators.DETAILS_LABEL)

    def test_rate_limit_form_fields_exist(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        add_btns = rl_page.find_elements(*RateLimitFormLocators.ADD_NEW_KEY_BUTTON)
        assert len(add_btns) == 3
        rl_page.clear_field(*RateLimitFormLocators.RULE_NAME_FIELD_EMPTY)
        assert rl_page.find_element(*RateLimitFormLocators.RULE_NAME_FIELD_EMPTY).get_attribute("placeholder") == \
               'Rule\'s name'
        assert rl_page.is_element_present(*RateLimitFormLocators.RULE_DESCRIPTION_FIELD)
        assert rl_page.is_element_present(*RateLimitFormLocators.PAIR_WITH_CHECKBOX)
        assert rl_page.is_element_present(*RateLimitFormLocators.RULE_LIMIT_FIELD)
        assert rl_page.is_element_present(*RateLimitFormLocators.RULE_TIMEFRAME_FIELD)
        assert rl_page.is_element_present(*RateLimitFormLocators.SECONDS_BUTTON)

    def test_default_key(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        assert rl_page.is_element_present(*RateLimitFormLocators.KEY_FIELD)
        assert rl_page.find_element(*RateLimitFormLocators.KEY_FIELD).get_attribute("placeholder") == \
               'Header\'s name'
        assert not rl_page.find_element(*RateLimitFormLocators.KEY_FIELD).get_attribute("placeholder") == \
                   'Cookie\'s name'

    def test_all_key_options_exist_in_dropdown(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        assert rl_page.is_element_present(*RateLimitFormLocators.HEADER_KEY)
        assert rl_page.is_element_present(*RateLimitFormLocators.COOKIE_KEY)
        assert rl_page.is_element_present(*RateLimitFormLocators.ATTRIBUTES_KEY)
        assert rl_page.is_element_present(*RateLimitFormLocators.ARGUMENTS_KEY)

    def test_can_select_key_from_dropdown(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        assert all(rl_page.check_selecting_key_changes_field())

    def test_attribute_key_values(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        assert not rl_page.is_element_present(*RateLimitFormLocators.ATTRIBUTES_KEY_DROPDOWN)
        rl_page.click_on_key("attrs")
        assert rl_page.is_element_present(*RateLimitFormLocators.ATTRIBUTES_KEY_DROPDOWN)

    def test_all_attribute_values_exist(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rl_page.click_on_key("attrs")
        assert rl_page.check_all_attribute_values_exist()

    def test_attribute_key_values_clickable(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        assert all(rl_page.attribute_values_clickable())

    def test_can_save_rule(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        key_value = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 5, 15, "headers", key_value)
        rl_page.click_save_rule_btn()
        rules = rl_page.extract_required_rule(name=rule_name)
        assert rules["name"] == rule_name
        assert rules["limit"] == "5"
        assert rules["key"][0]["headers"] == key_value
        assert rules["ttl"] == "15"
        assert rules["description"] == description
        assert rules["action"]["type"] == "default"

    def test_can_publish_rule(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        key_value = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 9, "headers", key_value)
        rl_page.click_save_rule_btn()
        rl_page.publish_planet(5)
        rules = rl_page.extract_required_rule(name=rule_name)
        assert rules["name"] == rule_name
        assert rules["limit"] == "3"
        assert rules["key"][0]["headers"] == key_value
        assert rules["ttl"] == "9"
        assert rules["description"] == description
        assert rules["action"]["type"] == "default"

    def test_can_attach_rule_to_url(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        key_value = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 8, 13, "headers", key_value)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
        rl_page.publish_planet(5)
        rules = rl_page.extract_required_rule(name=rule_name)
        assert rules["name"] == rule_name
        assert rules["limit"] == "8"
        assert rules["key"][0]["headers"] == key_value
        assert rules["ttl"] == "13"
        assert rules["description"] == description
        assert rules["action"]["type"] == "default"
        rl_page.open_path_attached_rules_list("rate.test", "Test")
        assert rl_page.check_rule_attached_to_url(rule_name, 8, 13, description)

    def test_can_remove_rule(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        key_value = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 5, "headers", key_value)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("rate1.test", "Test", rule_name, description)
        rl_page.publish_planet(5)
        rl_page.open_path_attached_rules_list("rate1.test", "Test")
        assert rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
        remove_btns = rl_page.find_elements(*PlanetOverviewLocators.REMOVE_BTN)
        for btn in remove_btns:
            btn.click()
            time.sleep(0.2)
        rl_page.find_element(*PlanetOverviewLocators.SAVE_BTN).click()
        rl_page.open_path_attached_rules_list("rate1.test", "Test")
        assert not rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)

    def test_can_attach_rule_to_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        key_value = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 15, 22, "headers", key_value)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("rate2.test", rule_name, description)
        rl_page.publish_planet(5)
        rl_page.open_site_attached_rules_list("rate2.test")
        assert rl_page.check_rule_attached_to_url(rule_name, 15, 22, description)

    def test_can_remove_rule_from_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        key_value = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 16, 23, "headers", key_value)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("rate3.test", rule_name, description)
        rl_page.publish_planet(5)
        rl_page.open_site_attached_rules_list("rate3.test")
        assert rl_page.check_rule_attached_to_url(rule_name, 16, 23, description)
        remove_btns = rl_page.find_elements(*PlanetOverviewLocators.REMOVE_BTN)
        for btn in remove_btns:
            btn.click()
            time.sleep(0.2)
        rl_page.find_element(*PlanetOverviewLocators.SAVE_BTN).click()
        assert not rl_page.check_rule_attached_to_url(rule_name, 16, 23, description)

    def test_rl_503_action_attr_event_count_by_ip_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 2, "attrs", "Remote Address")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("aate.test", rule_name, description)
        rl_page.publish_planet(21)
        rl_page.check_rate_limit_action_503(self.data["rl_base_url_4"], 1, 2)

    def test_rl_503_action_attr_event_count_by_ip_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 2, "attrs", "Remote Address")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("bate.test", "Test", rule_name, description)
        rl_page.publish_planet(21)
        rl_page.check_rate_limit_action_503(self.data["rl_base_url_5"], 1, 2, "/test")
        # Cleanup(self.driver).delete_rule(rule_name)

    def test_rl_503_action_attr_event_count_by_method_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 2, "attrs", "Method")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("cate.test", "Test", rule_name, description)
        rl_page.publish_planet(21)
        rl_page.check_rate_limit_action_503(self.data["rl_base_url_6"], 1, 2, "/test")

    def test_rl_503_action_attr_event_count_by_request_uri_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 2, "attrs", "Request URI")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("date.test", "Test", rule_name, description)
        rl_page.publish_planet(21)
        rl_page.check_rate_limit_action_503(self.data["rl_base_url_7"], 1, 2, "/test")

    def test_rl_503_action_cookie_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_cookie = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "cookies", rand_cookie)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("eate.test", "Test", rule_name, description)
        rl_page.publish_planet(24)
        params = [{"cookies": {rand_cookie: "val"}} for i in range(4)]
        rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url_8"], 2, 4, params, "/test")

    def test_rl_503_action_cookie_event_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_cookie = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "cookies", rand_cookie)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("fate.test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"cookies": {rand_cookie: "val"}} for i in range(4)]
        rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url_9"], 2, 4, params)

    def test_rl_503_action_header_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_header = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "headers", rand_header)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("gate.test", "Test", rule_name, description)
        rl_page.publish_planet(24)
        params = [{"headers": {rand_header: "val"}} for i in range(4)]
        rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url_10"], 2, 4, params, "/test")

    def test_rl_503_action_header_event_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_header = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "headers", rand_header)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("hate.test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"headers": {rand_header: "val"}} for i in range(4)]
        rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url_11"], 2, 4, params)

    def test_rl_503_action_args_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_arg = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "args", rand_arg)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("iate.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"params": {rand_arg: "val"}} for i in range(4)]
        rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url_12"], 2, 4, params, "/test")

    def test_rl_503_action_args_event_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_arg = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "args", rand_arg)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("jate.test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"params": {rand_arg: "val"}} for i in range(4)]
        rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url_13"], 2, 4, params)

    def test_rl_challenge_action_attr_event_count_by_ip_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address", "challenge")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("kate.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_challenge_action(self.data["rl_base_url_14"], 2, 4, "/test")

    def test_rl_challenge_action_attr_event_count_by_ip_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address", "challenge")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("late.test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_challenge_action(self.data["rl_base_url_15"], 2, 4, "/test")

    def test_rl_challenge_action_attr_event_count_by_method_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Method", "challenge")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("mate.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_challenge_action(self.data["rl_base_url_16"], 2, 4, "/test")

    def test_rl_challenge_action_attr_event_count_by_request_uri_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Request URI", "challenge")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("nate.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_challenge_action(self.data["rl_base_url_17"], 2, 4, "/test")

    def test_rl_challenge_action_cookie_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_cookie = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "cookies", rand_cookie, "challenge")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("oate.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"cookies": {rand_cookie: "val"}} for i in range(4)]
        rl_page.check_rate_limit_challenge_action_with_params(self.data["rl_base_url_18"], 2, 4, params, "/test")

    def test_rl_challenge_action_header_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_header = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "headers", rand_header, "challenge")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("pate.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"headers": {rand_header: "val"}} for i in range(4)]
        rl_page.check_rate_limit_challenge_action_with_params(self.data["rl_base_url_19"], 2, 4, params, "/test")

    def test_rl_challenge_action_args_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_arg = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "args", rand_arg, "challenge")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("qate.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"params": {rand_arg: "val"}} for i in range(4)]
        rl_page.check_rate_limit_challenge_action_with_params(self.data["rl_base_url_20"], 2, 4, params, "/test")

    def test_rl_tag_action_attr_event_count_by_ip_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 80, "attrs", "Remote Address", "tag")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("sate.test", "Test", rule_name, description)
        rl_page.publish_planet(24)
        rl_page.check_rate_limit_tag_action(self.data["rl_base_url_21"], 1, 80, rule_name, "sate.test", "/test")

    def test_rl_tag_action_attr_event_count_by_ip_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 78, "attrs", "Remote Address", "tag")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("tate.test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_tag_action(self.data["rl_base_url_22"], 1, 78, rule_name, "tate.test")

    def test_rl_tag_action_cookie_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_cookie = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 80, "cookies", rand_cookie, "tag")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("uate.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"cookies": {rand_cookie: "val"}} for i in range(3)]
        rl_page.check_rate_limit_tag_action_with_params(self.data["rl_base_url_23"], 1, 80, rule_name, "uate.test", params,
                                                        "/test")

    def test_rl_tag_action_header_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_header = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 80, "headers", rand_header, "tag")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("vate.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"headers": {rand_header: "val1"}} for i in range(3)]
        rl_page.check_rate_limit_tag_action_with_params(self.data["rl_base_url_24"], 1, 80, rule_name, "vate.test", params,
                                                        "/test")

    def test_rl_tag_action_args_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_arg = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 80, "args", rand_arg, "tag")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("wate.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"params": {rand_arg: "val12"}} for i in range(3)]
        rl_page.check_rate_limit_tag_action_with_params(self.data["rl_base_url_25"], 1, 80, rule_name, "wate.test", params,
                                                        "/test")

    def test_rl_redirect_action_attr_event_count_by_ip_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address", "redirect")
        rl_page.send_value_to_field("https://www.yahoo.com", *RateLimitFormLocators.REDIRECT_INPUT)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("xate.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_redirect_action(self.data["rl_base_url_26"], 200, 2, 4, "https://www.yahoo.com", "/test")

    def test_rl_redirect_action_attr_event_count_by_ip_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address", "redirect")
        rl_page.send_value_to_field("https://www.yahoo.com", *RateLimitFormLocators.REDIRECT_INPUT)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("yate.test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_redirect_action(self.data["rl_base_url_27"], 200, 2, 4, "https://www.yahoo.com", "/test")

    def test_rl_redirect_action_cookie_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_cookie = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "cookies", rand_cookie, "redirect")
        rl_page.send_value_to_field("https://www.espn.com", *RateLimitFormLocators.REDIRECT_INPUT)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("zate.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"cookies": {rand_cookie: "vale"}} for i in range(4)]
        rl_page.check_rate_limit_redirect_action_with_params(self.data["rl_base_url_28"], 200, 2, 4,
                                                             "https://www.espn.com",
                                                             params, "/test")

    def test_rl_redirect_action_header_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_header = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "headers", rand_header, "redirect")
        rl_page.send_value_to_field("https://www.google.com", *RateLimitFormLocators.REDIRECT_INPUT)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("aa.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"headers": {rand_header: "val"}} for i in range(4)]
        rl_page.check_rate_limit_redirect_action_with_params(self.data["rl_base_url_29"], 200, 2, 4,
                                                             "https://www.google.com",
                                                             params, "/test")

    def test_rl_redirect_action_args_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_arg = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 5, "args", rand_arg, "redirect")
        rl_page.send_value_to_field("https://www.one.com", *RateLimitFormLocators.REDIRECT_INPUT)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("bb.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"params": {rand_arg: "val"}} for i in range(4)]
        rl_page.check_rate_limit_redirect_action_with_params(self.data["rl_base_url_30"], 200, 2, 5, "https://www.one.com",
                                                             params, "/test")

    def test_rl_request_header_action_attr_event_count_by_ip_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address", "request_header")
        rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
        rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("cc.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        rl_page.check_rate_limit_action_request_header(self.data["rl_base_url_31"], 2, 4, rand_key, rand_val, "/test")

    def test_rl_request_header_action_attr_event_count_by_ip_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address", "request_header")
        rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
        rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("dd.test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_action_request_header(self.data["rl_base_url_32"], 2, 4, rand_key, rand_val)

    def test_rl_request_header_action_cookie_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_cookie = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "cookies", rand_cookie, "request_header")
        rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
        rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("ee.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"cookies": {rand_cookie: "valsdfsdre"}} for i in range(4)]
        rl_page.check_rate_limit_action_request_header_with_params(self.data["rl_base_url_33"], 2, 4, rand_key, rand_val,
                                                                   params, "/test")

    def test_rl_request_header_action_header_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_head = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "headers", rand_head, "request_header")
        rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
        rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"headers": {rand_head: "valde"}} for i in range(4)]
        rl_page.check_rate_limit_action_request_header_with_params(self.data["rl_base_url"], 2, 4, rand_key, rand_val,
                                                                   params, "/test")

    def test_rl_request_header_action_args_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_arg = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "args", rand_arg, "request_header")
        rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
        rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("gg.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"params": {rand_arg: "vaflde"}} for i in range(4)]
        rl_page.check_rate_limit_action_request_header_with_params(self.data["rl_base_url_35"], 2, 4, rand_key, rand_val,
                                                                   params, "/test")

    def test_rl_response_action_attr_event_count_by_ip_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_response = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address", "response")
        rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("hh.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_action_response(self.data["rl_base_url_36"], 2, 4, rand_key, rand_val, "247",
                                                 rand_response, "/test")

    def test_rl_response_action_attr_event_count_by_ip_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_response = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address", "response")
        rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("ii.test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_action_response(self.data["rl_base_url_37"], 2, 4, rand_key, rand_val, "247",
                                                 rand_response)

    def test_rl_response_action_cookie_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_response = rl_page.generate_random_string(10)
        rand_cookie = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "cookies", rand_cookie, "response")
        rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("jj.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"cookies": {rand_cookie: "val"}} for i in range(4)]
        rl_page.check_rate_limit_action_response_with_params(self.data["rl_base_url_38"], 2, 4, rand_key, rand_val, "247",
                                                             rand_response, params, "/test")

    def test_rl_response_action_header_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_response = rl_page.generate_random_string(10)
        rand_head = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "headers", rand_head, "response")
        rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("kk.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"headers": {rand_head: "valr1e"}} for i in range(4)]
        rl_page.check_rate_limit_action_response_with_params(self.data["rl_base_url_39"], 2, 4, rand_key, rand_val, "247",
                                                             rand_response, params, "/test")

    def test_rl_response_action_args_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_response = rl_page.generate_random_string(10)
        rand_head = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "args", rand_head, "response")
        rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("ll.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"params": {rand_head: "vlr1e"}} for i in range(4)]
        rl_page.check_rate_limit_action_response_with_params(self.data["rl_base_url_40"], 2, 4, rand_key, rand_val, "247",
                                                             rand_response, params, "/test")

    def test_rl_ban_503_action_attr_event_count_by_ip_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 2, "attrs", "Remote Address", "ban")
        rl_page.send_value_to_field(5, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("mm.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_action_503(self.data["rl_base_url_41"], 2, 5, "/test")

    def test_rl_ban_503_action_attr_event_count_by_ip_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "attrs", "Remote Address", "ban")
        rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("nn.test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_action_503(self.data["rl_base_url_42"], 1, 3, "/test")

    def test_rl_ban_503_action_cookie_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_cookie = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 5, "cookies", rand_cookie, "ban")
        rl_page.send_value_to_field(8, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("oo.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"cookies": {rand_cookie: "valre2"}} for i in range(4)]
        rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url_43"], 2, 8, params, "/test")

    def test_rl_ban_503_action_header_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_head = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 5, "headers", rand_head, "ban")
        rl_page.send_value_to_field(8, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("pp.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"headers": {rand_head: "valr2sdye2"}} for i in range(4)]
        rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url_44"], 2, 8, params, "/test")

    def test_rl_ban_503_action_args_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_arg = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "args", rand_arg, "ban")
        rl_page.send_value_to_field(5, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("qq.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"params": {rand_arg: "val2rd2sdgdsfe2"}} for i in range(3)]
        rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url_45"], 1, 5, params, "/test")

    def test_rl_ban_challenge_action_attr_event_count_by_ip_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 2, "attrs", "Remote Address", "ban")
        rl_page.send_value_to_field(5, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("challenge")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("rr.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_challenge_action(self.data["rl_base_url_46"], 2, 5, "/test")

    def test_rl_ban_challenge_action_attr_event_count_by_ip_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "attrs", "Remote Address", "ban")
        rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("challenge")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("ss.test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_challenge_action(self.data["rl_base_url_47"], 1, 3, "/test")

    def test_rl_ban_challenge_action_cookie_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_cookie = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 2, "cookies", rand_cookie, "ban")
        rl_page.send_value_to_field(5, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("challenge")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("tt.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"cookies": {rand_cookie: "valre2"}} for i in range(3)]
        rl_page.check_rate_limit_challenge_action_with_params(self.data["rl_base_url_48"], 1, 5, params, "/test")

    def test_rl_ban_challenge_action_header_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_head = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "headers", rand_head, "ban")
        rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("challenge")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("uu.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"headers": {rand_head: "valsre2"}} for i in range(3)]
        rl_page.check_rate_limit_challenge_action_with_params(self.data["rl_base_url_49"], 1, 3, params, "/test")

    def test_rl_ban_challenge_action_args_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_arg = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "args", rand_arg, "ban")
        rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("challenge")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("vv.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"params": {rand_arg: "valfre2"}} for i in range(3)]
        rl_page.check_rate_limit_challenge_action_with_params(self.data["rl_base_url_50"], 1, 3, params, "/test")

    def test_rl_ban_redirect_action_attr_event_count_by_ip_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "attrs", "Remote Address", "ban")
        rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("redirect")
        rl_page.send_value_to_field("https://www.google.com", *RateLimitFormLocators.REDIRECT_INPUT)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("ww.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_redirect_action(self.data["rl_base_url_51"], 200, 1, 3, "https://www.google.com", "/test")

    def test_rl_ban_redirect_action_attr_event_count_by_ip_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "attrs", "Remote Address", "ban")
        rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("redirect")
        rl_page.send_value_to_field("https://www.google.com", *RateLimitFormLocators.REDIRECT_INPUT)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("xx.test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_redirect_action(self.data["rl_base_url_52"], 200, 1, 3, "https://www.google.com", "/test")

    def test_rl_ban_redirect_action_cookie_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_cookie = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 3, "cookies", rand_cookie, "ban")
        rl_page.send_value_to_field(5, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("redirect")
        rl_page.send_value_to_field("https://www.google.com", *RateLimitFormLocators.REDIRECT_INPUT)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("yy.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"cookies": {rand_cookie: "valre2"}} for i in range(4)]
        rl_page.check_rate_limit_redirect_action_with_params(self.data["rl_base_url_53"], 200, 2, 5,
                                                             "https://www.google.com",
                                                             params, "/test")

    def test_rl_ban_redirect_action_header_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_head = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 3, "headers", rand_head, "ban")
        rl_page.send_value_to_field(6, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("redirect")
        rl_page.send_value_to_field("https://www.google.com", *RateLimitFormLocators.REDIRECT_INPUT)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("zz.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"headers": {rand_head: "valscre2"}} for i in range(4)]
        rl_page.check_rate_limit_redirect_action_with_params(self.data["rl_base_url_54"], 200, 2, 6,
                                                             "https://www.google.com",
                                                             params, "/test")

    def test_rl_ban_redirect_action_args_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_arg = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "args", rand_arg, "ban")
        rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("redirect")
        rl_page.send_value_to_field("https://www.google.com", *RateLimitFormLocators.REDIRECT_INPUT)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("abc.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"params": {rand_arg: "valxfre2"}} for i in range(3)]
        rl_page.check_rate_limit_redirect_action_with_params(self.data["rl_base_url_55"], 200, 1, 3,
                                                             "https://www.google.com",
                                                             params, "/test")

    def test_rl_ban_request_header_action_attr_event_count_by_ip_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "attrs", "Remote Address", "ban")
        rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("request_header")
        rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
        rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("bcd.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_action_request_header(self.data["rl_base_url_56"], 1, 3, rand_key, rand_val, "/test")

    def test_rl_ban_request_header_action_attr_event_count_by_ip_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "attrs", "Remote Address", "ban")
        rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("request_header")
        rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
        rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("cda.test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_action_request_header(self.data["rl_base_url_57"], 1, 3, rand_key, rand_val)

    def test_rl_ban_request_header_action_cookie_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_cookie = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "cookies", rand_cookie, "ban")
        rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("request_header")
        rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
        rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("asd.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"cookies": {rand_cookie: "valre"}} for i in range(3)]
        rl_page.check_rate_limit_action_request_header_with_params(self.data["rl_base_url_58"], 1, 3, rand_key, rand_val,
                                                                   params, "/test")

    def test_rl_ban_request_header_action_header_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_head = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 3, "headers", rand_head, "ban")
        rl_page.send_value_to_field(5, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("request_header")
        rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
        rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("sdf.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"headers": {rand_head: "valde"}} for i in range(4)]
        rl_page.check_rate_limit_action_request_header_with_params(self.data["rl_base_url_59"], 2, 5, rand_key, rand_val,
                                                                   params, "/test")

    def test_rl_ban_request_header_action_args_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_arg = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 2, "args", rand_arg, "ban")
        rl_page.send_value_to_field(5, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("request_header")
        rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
        rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("dfg.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"params": {rand_arg: "vaflde"}} for i in range(4)]
        rl_page.check_rate_limit_action_request_header_with_params(self.data["rl_base_url_60"], 2, 5, rand_key, rand_val,
                                                                   params, "/test")

    def test_rl_ban_response_action_attr_event_count_by_ip_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_response = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "attrs", "Remote Address", "ban")
        rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("response")
        rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("fgh.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_action_response(self.data["rl_base_url_61"], 1, 3, rand_key, rand_val, "247",
                                                 rand_response, "/test")

    def test_rl_ban_response_action_attr_event_count_by_ip_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_response = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 3, "attrs", "Remote Address", "ban")
        rl_page.send_value_to_field(6, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("response")
        rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("ghj.test", rule_name, description)
        rl_page.publish_planet(23)
        rl_page.check_rate_limit_action_response(self.data["rl_base_url_62"], 2, 6, rand_key, rand_val, "247",
                                                 rand_response)

    def test_rl_ban_response_action_cookie_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_response = rl_page.generate_random_string(10)
        rand_cookie = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "cookies", rand_cookie, "ban")
        rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("response")
        rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("hjk.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"cookies": {rand_cookie: "vffalre"}} for i in range(3)]
        rl_page.check_rate_limit_action_response_with_params(self.data["rl_base_url_63"], 1, 3, rand_key, rand_val, "247",
                                                             rand_response, params, "/test")

    def test_rl_ban_response_action_header_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_response = rl_page.generate_random_string(10)
        rand_head = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "headers", rand_head, "ban")
        rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("response")
        rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("jkl.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"headers": {rand_head: "valrdd1e"}} for i in range(3)]
        rl_page.check_rate_limit_action_response_with_params(self.data["rl_base_url_64"], 1, 3, rand_key, rand_val, "247",
                                                             rand_response, params, "/test")

    def test_rl_ban_response_action_args_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rand_response = rl_page.generate_random_string(10)
        rand_head = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "args", rand_head, "ban")
        rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("response")
        rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("qwe.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"params": {rand_head: "vlssr1e"}} for i in range(3)]
        rl_page.check_rate_limit_action_response_with_params(self.data["rl_base_url_65"], 1, 3, rand_key, rand_val, "247",
                                                             rand_response, params, "/test")

    def test_rl_ban_tag_action_attr_event_count_by_ip_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 70, "attrs", "Remote Address", "ban")
        rl_page.send_value_to_field(78, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("tag")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("wer.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_tag_action(self.data["rl_base_url_66"], 1, 78, rule_name, "wer.test", "/test")

    def test_rl_ban_tag_action_attr_event_count_by_ip_block_by_site(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 70, "attrs", "Remote Address", "ban")
        rl_page.send_value_to_field(80, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("tag")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_site("ert.test", rule_name, description)
        rl_page.publish_planet(23)
        rl_page.check_rate_limit_tag_action(self.data["rl_base_url_67"], 1, 80, rule_name, "ert.test")

    def test_rl_ban_tag_action_cookie_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_cookie = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 70, "cookies", rand_cookie, "ban")
        rl_page.send_value_to_field(80, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("tag")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("rty.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"cookies": {rand_cookie: "val"}} for i in range(3)]
        rl_page.check_rate_limit_tag_action_with_params(self.data["rl_base_url_68"], 1, 80, rule_name, "rty.test", params,
                                                        "/test")

    def test_rl_ban_tag_action_header_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_header = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 70, "headers", rand_header, "ban")
        rl_page.send_value_to_field(78, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("tag")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("tyu.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"headers": {rand_header: "val1"}} for i in range(3)]
        rl_page.check_rate_limit_tag_action_with_params(self.data["rl_base_url_69"], 1, 78, rule_name, "tyu.test", params,
                                                        "/test")

    def test_rl_ban_tag_action_args_event_block_by_path(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_arg = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 70, "args", rand_arg, "ban")
        rl_page.send_value_to_field(78, *RateLimitFormLocators.BAN_TIME_FRAME)
        rl_page.click_on_ban_action("tag")
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("yui.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"params": {rand_arg: "valdgds1s2"}} for i in range(3)]
        rl_page.check_rate_limit_tag_action_with_params(self.data["rl_base_url_70"], 1, 78, rule_name, "yui.test", params,
                                                        "/test")

    def test_503_action_attr_key_count_by_ip_exclude_header(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 5, "attrs", "Remote Address")
        rl_page.click_add_exclude_rule_btn()
        rl_page.fill_exclude_key_value(rand_key, rand_val)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("uio.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"headers": {rand_key: rand_val}} for i in range(10)]
        rl_page.check_rate_limit_action_503(self.data["rl_base_url_71"], 2, 5, "/test")
        time.sleep(5)
        rl_page.check_rate_limits_action_503_with_params_with_exclude(self.data["rl_base_url_71"], 8, 4, params, "/test")

    def test_503_action_attr_key_count_by_ip_exclude_cookie(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 5, "attrs", "Remote Address")
        rl_page.click_add_exclude_rule_btn()
        rl_page.click_on_include_exclude_option("Cookie")
        rl_page.fill_exclude_key_value(rand_key, rand_val)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("iop.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"cookies": {rand_key: rand_val}} for i in range(12)]
        rl_page.check_rate_limit_action_503(self.data["rl_base_url_72"], 3, 5, "/test")
        time.sleep(5)
        rl_page.check_rate_limits_action_503_with_params_with_exclude(self.data["rl_base_url"], 10, 5, params, "/test")

    def test_503_action_attr_key_count_by_ip_exclude_arg(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address")
        rl_page.click_add_exclude_rule_btn()
        rl_page.click_on_include_exclude_option("Argument")
        rl_page.fill_exclude_key_value(rand_key, rand_val)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("zxc.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        params = [{"params": {rand_key: rand_val}} for i in range(8)]
        rl_page.check_rate_limit_action_503(self.data["rl_base_url_73"], 2, 4, "/test")
        time.sleep(4)
        rl_page.check_rate_limits_action_503_with_params_with_exclude(self.data["rl_base_url_73"], 6, 4, params, "/test")

    def test_503_action_attr_key_count_by_ip_exclude_attr(self):
        rl_page = RateLimitPage(self.driver)
        country_name, country_code = rl_page.get_my_country_name_and_code()
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address")
        rl_page.click_add_exclude_rule_btn()
        rl_page.click_on_include_exclude_option("Attribute")
        rl_page.send_value_to_field("Country Name", *RateLimitFormLocators.ATTRS_EXCLUDE_KEY_FIELD)
        rl_page.send_value_to_field(country_name, *RateLimitFormLocators.EXCLUDE_VALUE_INPUT)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("xcv.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limits_action_503_with_exclude(self.data["rl_base_url_74"], 2, 4, "/test")

    def test_503_action_attr_key_count_by_ip_include_header(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address")
        rl_page.click_add_include_rule_btn()
        rl_page.fill_include_key_value(rand_key, rand_val)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("cvb.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"headers": {rand_key: rand_val}} for i in range(4)]
        rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url_75"], 2, 4, params,  "/test")

    def test_503_action_attr_key_count_by_ip_include_cookie(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 2, "attrs", "Remote Address")
        rl_page.click_add_include_rule_btn()
        rl_page.click_on_include_exclude_option("Cookie")
        rl_page.fill_include_key_value(rand_key, rand_val)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("vbn.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"cookies": {rand_key: rand_val}} for i in range(5)]
        rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url_76"], 3, 2, params,  "/test")

    def test_503_action_attr_key_count_by_ip_include_arg(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rand_key = rl_page.generate_random_string(10)
        rand_val = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address")
        rl_page.click_add_include_rule_btn()
        rl_page.click_on_include_exclude_option("Argument")
        rl_page.fill_include_key_value(rand_key, rand_val)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("bnm.test", "Test", rule_name, description)
        rl_page.publish_planet(22)
        params = [{"params": {rand_key: rand_val}} for i in range(4)]
        rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url_77"], 2, 4, params,  "/test")

    def test_503_action_attr_key_count_by_ip_include_attribute(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        country_name, country_code = rl_page.get_my_country_name_and_code()
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 100, 60, "attrs", "Remote Address")
        rl_page.click_add_include_rule_btn()
        rl_page.click_on_include_exclude_option("Attribute")
        rl_page.send_value_to_field("Country Name", *RateLimitFormLocators.ATTRS_INCLUDE_KEY_FIELD)
        rl_page.send_value_to_field(country_name, *RateLimitFormLocators.INCLUDE_VALUE_INPUT)
        rl_page.click_save_rule_btn()
        rl_page.attach_rule_to_url_by_path_and_rule_name("qaz.test", "Test", rule_name, description)
        rl_page.publish_planet(23)
        rl_page.check_rate_limit_action_503(self.data["rl_base_url_78"], 100, 60,  "/test")

    def test_can_add_rule_to_url_from_form_page(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 5, "attrs", "Remote Address")
        rl_page.click_save_rule_btn()
        rl_page.switch_to_frame()
        rl_page.find_element(*RateLimitFormLocators.CHOOSE_SITE_DROPDOWN).click()
        time.sleep(0.4)
        rl_page.find_element(By.LINK_TEXT, "wsx.test").click()
        time.sleep(0.4)
        rl_page.find_element(*RateLimitFormLocators.CHOOSE_LOCATION_DROPDOWN).click()
        time.sleep(0.4)
        rl_page.find_element(By.LINK_TEXT, "Test - /test").click()
        time.sleep(0.4)
        btns = rl_page.find_elements(By.XPATH, "//*[local-name()='svg' and @data-icon='plus']")
        btns[3].click()
        rl_page.click_save_rule_btn()
        rl_page.navigate_to_tab("Planet Overview")
        rl_page.switch_to_frame()
        rl_page.click_on_site_name_by_name("wsx.test")
        rl_page.switch_to_frame()
        rl_page.expand_path_settings_by_name("Test")
        time.sleep(0.4)
        assert rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_action_503(self.data["rl_base_url_79"], 2, 5, "/test")

    def test_can_add_rule_to_all_from_form_page(self):
        rl_page = RateLimitPage(self.driver)
        rl_page.open_new_rule_form()
        rule_name = rl_page.generate_random_string(10)
        description = rl_page.generate_random_string(10)
        rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 5, "attrs", "Remote Address")
        rl_page.click_save_rule_btn()
        rl_page.switch_to_frame()
        rl_page.find_element(*RateLimitFormLocators.CHOOSE_SITE_DROPDOWN).click()
        rl_page.find_element(By.LINK_TEXT, "edc.test").click()
        rl_page.explicitly_wait_for_element_to_be_clickable(10, *RateLimitFormLocators.CHOOSE_LOCATION_DROPDOWN)
        rl_page.find_element(*RateLimitFormLocators.CHOOSE_LOCATION_DROPDOWN).click()
        time.sleep(0.6)
        rl_page.find_element(By.LINK_TEXT, "*Add to all*").click()
        btns = rl_page.find_elements(By.XPATH, "//*[local-name()='svg' and @data-icon='plus']")
        btns[3].click()
        rl_page.click_save_rule_btn()
        rl_page.switch_to_default_content()
        rl_page.navigate_to_tab("Planet Overview")
        rl_page.switch_to_frame()
        rl_page.click_on_site_name_by_name("edc.test")
        rl_page.switch_to_frame()
        rl_page.expand_path_settings_by_name("Test")
        time.sleep(0.6)
        assert rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
        rl_page.expand_path_settings_by_name("Static Files")
        time.sleep(0.6)
        assert rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
        rl_page.expand_path_settings_by_name("Default")
        time.sleep(0.4)
        assert rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
        rl_page.expand_path_settings_by_name("Root")
        time.sleep(0.4)
        assert rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
        rl_page.expand_path_settings_by_name("Default Path")
        time.sleep(0.4)
        assert rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
        rl_page.publish_planet(22)
        rl_page.check_rate_limit_action_503(self.data["rl_base_url_80"], 2, 5, "/test")

# ******************************************************************************************************************** #
# ***************************** Original rate limit tests - in case optimization fails ******************************* #
# ******************************************************************************************************************** #
#
#
#     def test_can_navigate_to_rate_limit_page(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.navigate_to_tab("Rate Limiting")
#         assert "ratelimit" in rl_page.get_current_url()
#
#     def test_rate_limit_page_components_exist(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.navigate_to_tab("Rate Limiting")
#         rl_page.switch_to_frame()
#         assert rl_page.is_element_present(*RateLimitMainPageLocators.RATE_LIMIT_LABEL)
#         assert rl_page.is_element_present(*RateLimitMainPageLocators.RULES_TAB)
#         assert rl_page.is_element_present(*RateLimitMainPageLocators.REDIS_TAB)
#         assert rl_page.is_element_present(*RateLimitMainPageLocators.NEW_RULE_BTN)
#         assert rl_page.is_element_present(*RateLimitMainPageLocators.SEARCH_FIELD)
#
#     def test_can_open_new_rule_form(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.switch_to_default_content()
#         rl_page.navigate_to_tab("Rate Limiting")
#         rl_page.switch_to_frame()
#         rl_page.find_element(*RateLimitMainPageLocators.NEW_RULE_BTN).click()
#         assert "/ratelimit/form" in rl_page.get_current_url()
#
#     def test_rate_limit_form_components_exist(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         assert rl_page.is_element_present(*RateLimitFormLocators.NEW_RATE_LIMIT_LABEL)
#         assert rl_page.is_element_present(*RateLimitFormLocators.INCLUDE_LABEL)
#         assert rl_page.is_element_present(*RateLimitFormLocators.EXCLUDE_LABEL)
#         assert rl_page.is_element_present(*RateLimitFormLocators.DETAILS_LABEL)
#
#     def test_rate_limit_form_fields_exist(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         add_btns = rl_page.find_elements(*RateLimitFormLocators.ADD_NEW_KEY_BUTTON)
#         assert len(add_btns) == 3
#         rl_page.clear_field(*RateLimitFormLocators.RULE_NAME_FIELD_EMPTY)
#         assert rl_page.find_element(*RateLimitFormLocators.RULE_NAME_FIELD_EMPTY).get_attribute("placeholder") == \
#                'Rule\'s name'
#         assert rl_page.is_element_present(*RateLimitFormLocators.RULE_DESCRIPTION_FIELD)
#         assert rl_page.is_element_present(*RateLimitFormLocators.PAIR_WITH_CHECKBOX)
#         assert rl_page.is_element_present(*RateLimitFormLocators.RULE_LIMIT_FIELD)
#         assert rl_page.is_element_present(*RateLimitFormLocators.RULE_TIMEFRAME_FIELD)
#         assert rl_page.is_element_present(*RateLimitFormLocators.SECONDS_BUTTON)
#
#     def test_default_key(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         assert rl_page.is_element_present(*RateLimitFormLocators.KEY_FIELD)
#         assert rl_page.find_element(*RateLimitFormLocators.KEY_FIELD).get_attribute("placeholder") == \
#                'Header\'s name'
#         assert not rl_page.find_element(*RateLimitFormLocators.KEY_FIELD).get_attribute("placeholder") == \
#                    'Cookie\'s name'
#
#     def test_all_key_options_exist_in_dropdown(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         assert rl_page.is_element_present(*RateLimitFormLocators.HEADER_KEY)
#         assert rl_page.is_element_present(*RateLimitFormLocators.COOKIE_KEY)
#         assert rl_page.is_element_present(*RateLimitFormLocators.ATTRIBUTES_KEY)
#         assert rl_page.is_element_present(*RateLimitFormLocators.ARGUMENTS_KEY)
#
#     def test_can_select_key_from_dropdown(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         assert all(rl_page.check_selecting_key_changes_field())
#
#     def test_attribute_key_values(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         assert not rl_page.is_element_present(*RateLimitFormLocators.ATTRIBUTES_KEY_DROPDOWN)
#         rl_page.click_on_key("attrs")
#         assert rl_page.is_element_present(*RateLimitFormLocators.ATTRIBUTES_KEY_DROPDOWN)
#
#     def test_all_attribute_values_exist(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rl_page.click_on_key("attrs")
#         assert rl_page.check_all_attribute_values_exist()
#
#     def test_attribute_key_values_clickable(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         assert all(rl_page.attribute_values_clickable())
#
#     def test_can_save_rule(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         key_value = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 5, 15, "headers", key_value)
#         rl_page.click_save_rule_btn()
#         rules = rl_page.extract_required_rule(name=rule_name)
#         assert rules["name"] == rule_name
#         assert rules["limit"] == "5"
#         assert rules["key"][0]["headers"] == key_value
#         assert rules["ttl"] == "15"
#         assert rules["description"] == description
#         assert rules["action"]["type"] == "default"
#
#     def test_can_publish_rule(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         key_value = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 9, "headers", key_value)
#         rl_page.click_save_rule_btn()
#         rl_page.publish_planet(5)
#         rules = rl_page.extract_required_rule(name=rule_name)
#         assert rules["name"] == rule_name
#         assert rules["limit"] == "3"
#         assert rules["key"][0]["headers"] == key_value
#         assert rules["ttl"] == "9"
#         assert rules["description"] == description
#         assert rules["action"]["type"] == "default"
#
#     def test_can_attach_rule_to_url(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         key_value = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 8, 13, "headers", key_value)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(5)
#         rules = rl_page.extract_required_rule(name=rule_name)
#         assert rules["name"] == rule_name
#         assert rules["limit"] == "8"
#         assert rules["key"][0]["headers"] == key_value
#         assert rules["ttl"] == "13"
#         assert rules["description"] == description
#         assert rules["action"]["type"] == "default"
#         rl_page.open_path_attached_rules_list("rate.test", "Test")
#         assert rl_page.check_rule_attached_to_url(rule_name, 8, 13, description)
#
#     def test_can_remove_rule(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         key_value = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 5, "headers", key_value)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(5)
#         rl_page.open_path_attached_rules_list("rate.test", "Test")
#         assert rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
#         remove_btns = rl_page.find_elements(*PlanetOverviewLocators.REMOVE_BTN)
#         for btn in remove_btns:
#             btn.click()
#             time.sleep(0.2)
#         rl_page.find_element(*PlanetOverviewLocators.SAVE_BTN).click()
#         rl_page.open_path_attached_rules_list("rate.test", "Test")
#         assert not rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
#
#     def test_can_attach_rule_to_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         key_value = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 15, 22, "headers", key_value)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(5)
#         rl_page.open_site_attached_rules_list("rate.test")
#         assert rl_page.check_rule_attached_to_url(rule_name, 15, 22, description)
#
#     def test_can_remove_rule_from_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         key_value = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 16, 23, "headers", key_value)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(5)
#         rl_page.open_site_attached_rules_list("rate.test")
#         assert rl_page.check_rule_attached_to_url(rule_name, 16, 23, description)
#         remove_btns = rl_page.find_elements(*PlanetOverviewLocators.REMOVE_BTN)
#         for btn in remove_btns:
#             btn.click()
#             time.sleep(0.2)
#         rl_page.find_element(*PlanetOverviewLocators.SAVE_BTN).click()
#         assert not rl_page.check_rule_attached_to_url(rule_name, 16, 23, description)
#
#     def test_rl_503_action_attr_event_count_by_ip_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_503(self.data["rl_base_url"], 2, 4)
#
#     def test_rl_503_action_attr_event_count_by_ip_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 4, "attrs", "Remote Address")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_503(self.data["rl_base_url"], 3, 4, "/test")
#
#     def test_rl_503_action_attr_event_count_by_method_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 3, "attrs", "Method")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_503(self.data["rl_base_url"], 2, 3, "/test")
#
#     def test_rl_503_action_attr_event_count_by_request_uri_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 4, "attrs", "Request URI")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_503(self.data["rl_base_url"], 3, 4, "/test")
#
#     def test_rl_503_action_cookie_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_cookie = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 5, "cookies", rand_cookie)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"cookies": {rand_cookie: "val"}} for i in range(5)]
#         rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url"], 3, 5, params, "/test")
#
#     def test_rl_503_action_cookie_event_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_cookie = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "cookies", rand_cookie)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"cookies": {rand_cookie: "val"}} for i in range(4)]
#         rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url"], 2, 4, params)
#
#     def test_rl_503_action_header_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_header = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 6, 4, "headers", rand_header)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"headers": {rand_header: "val"}} for i in range(8)]
#         rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url"], 6, 4, params, "/test")
#
#     def test_rl_503_action_header_event_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_header = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 8, 4, "headers", rand_header)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"headers": {rand_header: "val"}} for i in range(10)]
#         rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url"], 8, 4, params)
#
#     def test_rl_503_action_args_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_arg = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 4, 4, "args", rand_arg)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"params": {rand_arg: "val"}} for i in range(6)]
#         rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url"], 4, 4, params, "/test")
#
#     def test_rl_503_action_args_event_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_arg = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 5, 5, "args", rand_arg)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"params": {rand_arg: "val"}} for i in range(7)]
#         rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url"], 5, 5, params)
#
#     def test_rl_challenge_action_attr_event_count_by_ip_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 5, "attrs", "Remote Address", "challenge")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_challenge_action(self.data["rl_base_url"], 2, 5, "/test")
#
#     def test_rl_challenge_action_attr_event_count_by_ip_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 4, "attrs", "Remote Address", "challenge")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_challenge_action(self.data["rl_base_url"], 3, 4, "/test")
#
#     def test_rl_challenge_action_attr_event_count_by_method_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 5, "attrs", "Method", "challenge")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_challenge_action(self.data["rl_base_url"], 3, 5, "/test")
#
#     def test_rl_challenge_action_attr_event_count_by_request_uri_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 4, 5, "attrs", "Request URI", "challenge")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_challenge_action(self.data["rl_base_url"], 4, 5, "/test")
#
#     def test_rl_challenge_action_cookie_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_cookie = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 5, "cookies", rand_cookie, "challenge")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"cookies": {rand_cookie: "val"}} for i in range(5)]
#         rl_page.check_rate_limit_challenge_action_with_params(self.data["rl_base_url"], 3, 5, params, "/test")
#
#     def test_rl_challenge_action_header_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_header = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 10, 5, "headers", rand_header, "challenge")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"headers": {rand_header: "val"}} for i in range(12)]
#         rl_page.check_rate_limit_challenge_action_with_params(self.data["rl_base_url"], 10, 7, params, "/test")
#
#     def test_rl_challenge_action_args_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_arg = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 11, 5, "args", rand_arg, "challenge")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"params": {rand_arg: "val"}} for i in range(13)]
#         rl_page.check_rate_limit_challenge_action_with_params(self.data["rl_base_url"], 11, 5, params, "/test")
#
#     def test_rl_tag_action_attr_event_count_by_ip_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 80, "attrs", "Remote Address", "tag")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_tag_action(self.data["rl_base_url"], 1, 80, rule_name, "rate.test", "/test")
#
#     def test_rl_tag_action_attr_event_count_by_ip_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 78, "attrs", "Remote Address", "tag")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(21)
#         rl_page.check_rate_limit_tag_action(self.data["rl_base_url"], 1, 78, rule_name, "rate.test")
#
#     def test_rl_tag_action_cookie_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_cookie = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 78, "cookies", rand_cookie, "tag")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(21)
#         params = [{"cookies": {rand_cookie: "val"}} for i in range(3)]
#         rl_page.check_rate_limit_tag_action_with_params(self.data["rl_base_url"], 1, 78, rule_name, "rate.test", params,
#                                                         "/test")
#
#     def test_rl_tag_action_header_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_header = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 78, "headers", rand_header, "tag")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(21)
#         params = [{"headers": {rand_header: "val1"}} for i in range(3)]
#         rl_page.check_rate_limit_tag_action_with_params(self.data["rl_base_url"], 1, 78, rule_name, "rate.test", params,
#                                                         "/test")
#
#     def test_rl_tag_action_args_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_arg = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 78, "args", rand_arg, "tag")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(21)
#         params = [{"params": {rand_arg: "val12"}} for i in range(3)]
#         rl_page.check_rate_limit_tag_action_with_params(self.data["rl_base_url"], 1, 78, rule_name, "rate.test", params,
#                                                         "/test")
#
#     def test_rl_redirect_action_attr_event_count_by_ip_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 2, "attrs", "Remote Address", "redirect")
#         rl_page.send_value_to_field("https://www.yahoo.com", *RateLimitFormLocators.REDIRECT_INPUT)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_redirect_action(self.data["rl_base_url"], 200, 2, 2, "https://www.yahoo.com", "/test")
#
#     def test_rl_redirect_action_attr_event_count_by_ip_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 3, "attrs", "Remote Address", "redirect")
#         rl_page.send_value_to_field("https://www.yahoo.com", *RateLimitFormLocators.REDIRECT_INPUT)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_redirect_action(self.data["rl_base_url"], 200, 3, 3, "https://www.yahoo.com", "/test")
#
#     def test_rl_redirect_action_cookie_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_cookie = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 4, 6, "cookies", rand_cookie, "redirect")
#         rl_page.send_value_to_field("https://www.espn.com", *RateLimitFormLocators.REDIRECT_INPUT)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"cookies": {rand_cookie: "vale"}} for i in range(6)]
#         rl_page.check_rate_limit_redirect_action_with_params(self.data["rl_base_url"], 200, 4, 6,
#                                                              "https://www.espn.com",
#                                                              params, "/test")
#
#     def test_rl_redirect_action_header_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_header = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 5, "headers", rand_header, "redirect")
#         rl_page.send_value_to_field("https://www.google.com", *RateLimitFormLocators.REDIRECT_INPUT)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"headers": {rand_header: "val"}} for i in range(6)]
#         rl_page.check_rate_limit_redirect_action_with_params(self.data["rl_base_url"], 200, 3, 5,
#                                                              "https://www.google.com",
#                                                              params, "/test")
#
#     def test_rl_redirect_action_args_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_arg = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 5, 3, "args", rand_arg, "redirect")
#         rl_page.send_value_to_field("https://www.one.com", *RateLimitFormLocators.REDIRECT_INPUT)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"params": {rand_arg: "val"}} for i in range(7)]
#         rl_page.check_rate_limit_redirect_action_with_params(self.data["rl_base_url"], 200, 5, 3, "https://www.one.com",
#                                                              params, "/test")
#
#     def test_rl_request_header_action_attr_event_count_by_ip_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 8, 5, "attrs", "Remote Address", "request_header")
#         rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
#         rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_request_header(self.data["rl_base_url"], 8, 5, rand_key, rand_val, "/test")
#
#     def test_rl_request_header_action_attr_event_count_by_ip_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address", "request_header")
#         rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
#         rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_request_header(self.data["rl_base_url"], 2, 4, rand_key, rand_val)
#
#     def test_rl_request_header_action_cookie_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_cookie = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 5, "cookies", rand_cookie, "request_header")
#         rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
#         rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"cookies": {rand_cookie: "valsdfsdre"}} for i in range(5)]
#         rl_page.check_rate_limit_action_request_header_with_params(self.data["rl_base_url"], 3, 5, rand_key, rand_val,
#                                                                    params, "/test")
#
#     def test_rl_request_header_action_header_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_head = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 6, 4, "headers", rand_head, "request_header")
#         rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
#         rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"headers": {rand_head: "valde"}} for i in range(8)]
#         rl_page.check_rate_limit_action_request_header_with_params(self.data["rl_base_url"], 6, 4, rand_key, rand_val,
#                                                                    params, "/test")
#
#     def test_rl_request_header_action_args_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_arg = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 4, 5, "args", rand_arg, "request_header")
#         rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
#         rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"params": {rand_arg: "vaflde"}} for i in range(6)]
#         rl_page.check_rate_limit_action_request_header_with_params(self.data["rl_base_url"], 4, 5, rand_key, rand_val,
#                                                                    params, "/test")
#
#     def test_rl_response_action_attr_event_count_by_ip_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_response = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 5, "attrs", "Remote Address", "response")
#         rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_response(self.data["rl_base_url"], 3, 5, rand_key, rand_val, "247",
#                                                  rand_response, "/test")
#
#     def test_rl_response_action_attr_event_count_by_ip_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_response = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address", "response")
#         rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_response(self.data["rl_base_url"], 2, 4, rand_key, rand_val, "247",
#                                                  rand_response)
#
#     def test_rl_response_action_cookie_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_response = rl_page.generate_random_string(10)
#         rand_cookie = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "cookies", rand_cookie, "response")
#         rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"cookies": {rand_cookie: "val"}} for i in range(4)]
#         rl_page.check_rate_limit_action_response_with_params(self.data["rl_base_url"], 2, 4, rand_key, rand_val, "247",
#                                                              rand_response, params, "/test")
#
#     def test_rl_response_action_header_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_response = rl_page.generate_random_string(10)
#         rand_head = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 4, "headers", rand_head, "response")
#         rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"headers": {rand_head: "valr1e"}} for i in range(5)]
#         rl_page.check_rate_limit_action_response_with_params(self.data["rl_base_url"], 3, 4, rand_key, rand_val, "247",
#                                                              rand_response, params, "/test")
#
#     def test_rl_response_action_args_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_response = rl_page.generate_random_string(10)
#         rand_head = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 4, 4, "args", rand_head, "response")
#         rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"params": {rand_head: "vlr1e"}} for i in range(6)]
#         rl_page.check_rate_limit_action_response_with_params(self.data["rl_base_url"], 4, 4, rand_key, rand_val, "247",
#                                                              rand_response, params, "/test")
#
#     def test_rl_ban_503_action_attr_event_count_by_ip_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 3, "attrs", "Remote Address", "ban")
#         rl_page.send_value_to_field(6, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_503(self.data["rl_base_url"], 2, 6, "/test")
#
#     def test_rl_ban_503_action_attr_event_count_by_ip_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 3, "attrs", "Remote Address", "ban")
#         rl_page.send_value_to_field(7, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_503(self.data["rl_base_url"], 3, 7, "/test")
#
#     def test_rl_ban_503_action_cookie_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_cookie = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 3, "cookies", rand_cookie, "ban")
#         rl_page.send_value_to_field(6, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"cookies": {rand_cookie: "valre2"}} for i in range(4)]
#         rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url"], 2, 6, params, "/test")
#
#     def test_rl_ban_503_action_header_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_head = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 4, "headers", rand_head, "ban")
#         rl_page.send_value_to_field(6, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"headers": {rand_head: "valr2e2"}} for i in range(5)]
#         rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url"], 3, 7, params, "/test")
#
#     def test_rl_ban_503_action_args_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_arg = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 4, 5, "args", rand_arg, "ban")
#         rl_page.send_value_to_field(6, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"params": {rand_arg: "val2r2e2"}} for i in range(6)]
#         rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url"], 4, 7, params, "/test")
#
#     def test_rl_ban_challenge_action_attr_event_count_by_ip_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 3, "attrs", "Remote Address", "ban")
#         rl_page.send_value_to_field(6, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("challenge")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_challenge_action(self.data["rl_base_url"], 2, 6, "/test")
#
#     def test_rl_ban_challenge_action_attr_event_count_by_ip_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 3, "attrs", "Remote Address", "ban")
#         rl_page.send_value_to_field(7, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("challenge")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_challenge_action(self.data["rl_base_url"], 3, 7, "/test")
#
#     def test_rl_ban_challenge_action_cookie_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_cookie = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "cookies", rand_cookie, "ban")
#         rl_page.send_value_to_field(6, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("challenge")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"cookies": {rand_cookie: "valre2"}} for i in range(4)]
#         rl_page.check_rate_limit_challenge_action_with_params(self.data["rl_base_url"], 2, 6, params, "/test")
#
#     def test_rl_ban_challenge_action_header_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_head = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 5, "headers", rand_head, "ban")
#         rl_page.send_value_to_field(6, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("challenge")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"headers": {rand_head: "valsre2"}} for i in range(4)]
#         rl_page.check_rate_limit_challenge_action_with_params(self.data["rl_base_url"], 2, 7, params, "/test")
#
#     def test_rl_ban_challenge_action_args_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_arg = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "args", rand_arg, "ban")
#         rl_page.send_value_to_field(6, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("challenge")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"params": {rand_arg: "valfre2"}} for i in range(4)]
#         rl_page.check_rate_limit_challenge_action_with_params(self.data["rl_base_url"], 2, 6, params, "/test")
#
#     def test_rl_ban_redirect_action_attr_event_count_by_ip_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 5, 5, "attrs", "Remote Address", "ban")
#         rl_page.send_value_to_field(8, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("redirect")
#         rl_page.send_value_to_field("https://www.google.com", *RateLimitFormLocators.REDIRECT_INPUT)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_redirect_action(self.data["rl_base_url"], 200, 5, 8, "https://www.google.com", "/test")
#
#     def test_rl_ban_redirect_action_attr_event_count_by_ip_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 4, "attrs", "Remote Address", "ban")
#         rl_page.send_value_to_field(9, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("redirect")
#         rl_page.send_value_to_field("https://www.google.com", *RateLimitFormLocators.REDIRECT_INPUT)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_redirect_action(self.data["rl_base_url"], 200, 3, 9, "https://www.google.com", "/test")
#
#     def test_rl_ban_redirect_action_cookie_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_cookie = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 3, "cookies", rand_cookie, "ban")
#         rl_page.send_value_to_field(5, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("redirect")
#         rl_page.send_value_to_field("https://www.google.com", *RateLimitFormLocators.REDIRECT_INPUT)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"cookies": {rand_cookie: "valre2"}} for i in range(5)]
#         rl_page.check_rate_limit_redirect_action_with_params(self.data["rl_base_url"], 200, 3, 5,
#                                                              "https://www.google.com",
#                                                              params, "/test")
#
#     def test_rl_ban_redirect_action_header_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_head = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 4, 5, "headers", rand_head, "ban")
#         rl_page.send_value_to_field(7, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("redirect")
#         rl_page.send_value_to_field("https://www.google.com", *RateLimitFormLocators.REDIRECT_INPUT)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"headers": {rand_head: "valscre2"}} for i in range(6)]
#         rl_page.check_rate_limit_redirect_action_with_params(self.data["rl_base_url"], 200, 4, 7,
#                                                              "https://www.google.com",
#                                                              params, "/test")
#
#     def test_rl_ban_redirect_action_args_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_arg = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 4, "args", rand_arg, "ban")
#         rl_page.send_value_to_field(6, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("redirect")
#         rl_page.send_value_to_field("https://www.google.com", *RateLimitFormLocators.REDIRECT_INPUT)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"params": {rand_arg: "valxfre2"}} for i in range(5)]
#         rl_page.check_rate_limit_redirect_action_with_params(self.data["rl_base_url"], 200, 3, 6,
#                                                              "https://www.google.com",
#                                                              params, "/test")
#
#     def test_rl_ban_request_header_action_attr_event_count_by_ip_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 9, 4, "attrs", "Remote Address", "ban")
#         rl_page.send_value_to_field(6, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("request_header")
#         rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
#         rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_request_header(self.data["rl_base_url"], 9, 6, rand_key, rand_val, "/test")
#
#     def test_rl_ban_request_header_action_attr_event_count_by_ip_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 5, 5, "attrs", "Remote Address", "ban")
#         rl_page.send_value_to_field(7, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("request_header")
#         rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
#         rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_request_header(self.data["rl_base_url"], 5, 7, rand_key, rand_val)
#
#     def test_rl_ban_request_header_action_cookie_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_cookie = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 1, "cookies", rand_cookie, "ban")
#         rl_page.send_value_to_field(3, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("request_header")
#         rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
#         rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"cookies": {rand_cookie: "valre"}} for i in range(3)]
#         rl_page.check_rate_limit_action_request_header_with_params(self.data["rl_base_url"], 1, 3, rand_key, rand_val,
#                                                                    params, "/test")
#
#     def test_rl_ban_request_header_action_header_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_head = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 8, 4, "headers", rand_head, "ban")
#         rl_page.send_value_to_field(8, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("request_header")
#         rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
#         rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"headers": {rand_head: "valde"}} for i in range(10)]
#         rl_page.check_rate_limit_action_request_header_with_params(self.data["rl_base_url"], 8, 8, rand_key, rand_val,
#                                                                    params, "/test")
#
#     def test_rl_ban_request_header_action_args_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_arg = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 4, 5, "args", rand_arg, "ban")
#         rl_page.send_value_to_field(7, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("request_header")
#         rl_page.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
#         rl_page.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"params": {rand_arg: "vaflde"}} for i in range(6)]
#         rl_page.check_rate_limit_action_request_header_with_params(self.data["rl_base_url"], 4, 7, rand_key, rand_val,
#                                                                    params, "/test")
#
#     def test_rl_ban_response_action_attr_event_count_by_ip_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_response = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 5, "attrs", "Remote Address", "ban")
#         rl_page.send_value_to_field(7, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("response")
#         rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_response(self.data["rl_base_url"], 3, 7, rand_key, rand_val, "247",
#                                                  rand_response, "/test")
#
#     def test_rl_ban_response_action_attr_event_count_by_ip_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_response = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 5, "attrs", "Remote Address", "ban")
#         rl_page.send_value_to_field(7, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("response")
#         rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_response(self.data["rl_base_url"], 2, 7, rand_key, rand_val, "247",
#                                                  rand_response)
#
#     def test_rl_ban_response_action_cookie_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_response = rl_page.generate_random_string(10)
#         rand_cookie = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "cookies", rand_cookie, "ban")
#         rl_page.send_value_to_field(6, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("response")
#         rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"cookies": {rand_cookie: "vffalre"}} for i in range(4)]
#         rl_page.check_rate_limit_action_response_with_params(self.data["rl_base_url"], 2, 6, rand_key, rand_val, "247",
#                                                              rand_response, params, "/test")
#
#     def test_rl_ban_response_action_header_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_response = rl_page.generate_random_string(10)
#         rand_head = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 2, "headers", rand_head, "ban")
#         rl_page.send_value_to_field(4, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("response")
#         rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"headers": {rand_head: "valrdd1e"}} for i in range(5)]
#         rl_page.check_rate_limit_action_response_with_params(self.data["rl_base_url"], 3, 4, rand_key, rand_val, "247",
#                                                              rand_response, params, "/test")
#
#     def test_rl_ban_response_action_args_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rand_response = rl_page.generate_random_string(10)
#         rand_head = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 4, 5, "args", rand_head, "ban")
#         rl_page.send_value_to_field(7, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("response")
#         rl_page.fill_response_action_fields(rand_key, rand_val, rand_response)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"params": {rand_head: "vlssr1e"}} for i in range(6)]
#         rl_page.check_rate_limit_action_response_with_params(self.data["rl_base_url"], 4, 7, rand_key, rand_val, "247",
#                                                              rand_response, params, "/test")
#
#     def test_rl_ban_tag_action_attr_event_count_by_ip_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 70, "attrs", "Remote Address", "ban")
#         rl_page.send_value_to_field(78, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("tag")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(21)
#         rl_page.check_rate_limit_tag_action(self.data["rl_base_url"], 1, 78, rule_name, "rate.test", "/test")
#
#     def test_rl_ban_tag_action_attr_event_count_by_ip_block_by_site(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 70, "attrs", "Remote Address", "ban")
#         rl_page.send_value_to_field(79, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("tag")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_site("rate.test", rule_name, description)
#         rl_page.publish_planet(21)
#         rl_page.check_rate_limit_tag_action(self.data["rl_base_url"], 1, 79, rule_name, "rate.test")
#
#     def test_rl_ban_tag_action_cookie_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_cookie = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 70, "cookies", rand_cookie, "ban")
#         rl_page.send_value_to_field(79, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("tag")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(21)
#         params = [{"cookies": {rand_cookie: "val"}} for i in range(3)]
#         rl_page.check_rate_limit_tag_action_with_params(self.data["rl_base_url"], 1, 79, rule_name, "rate.test", params,
#                                                         "/test")
#
#     def test_rl_ban_tag_action_header_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_header = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 70, "headers", rand_header, "ban")
#         rl_page.send_value_to_field(75, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("tag")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(21)
#         params = [{"headers": {rand_header: "val1"}} for i in range(3)]
#         rl_page.check_rate_limit_tag_action_with_params(self.data["rl_base_url"], 1, 75, rule_name, "rate.test", params,
#                                                         "/test")
#
#     def test_rl_ban_tag_action_args_event_block_by_path(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_arg = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 1, 70, "args", rand_arg, "ban")
#         rl_page.send_value_to_field(78, *RateLimitFormLocators.BAN_TIME_FRAME)
#         rl_page.click_on_ban_action("tag")
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(21)
#         params = [{"params": {rand_arg: "val1s2"}} for i in range(3)]
#         rl_page.check_rate_limit_tag_action_with_params(self.data["rl_base_url"], 1, 78, rule_name, "rate.test", params,
#                                                         "/test")
#
#     def test_503_action_attr_key_count_by_ip_exclude_header(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address")
#         rl_page.click_add_exclude_rule_btn()
#         rl_page.fill_exclude_key_value(rand_key, rand_val)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"headers": {rand_key: rand_val}} for i in range(10)]
#         rl_page.check_rate_limit_action_503(self.data["rl_base_url"], 2, 4, "/test")
#         time.sleep(4)
#         rl_page.check_rate_limits_action_503_with_params_with_exclude(self.data["rl_base_url"], 8, 4, params, "/test")
#
#     def test_503_action_attr_key_count_by_ip_exclude_cookie(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 5, "attrs", "Remote Address")
#         rl_page.click_add_exclude_rule_btn()
#         rl_page.click_on_include_exclude_option("Cookie")
#         rl_page.fill_exclude_key_value(rand_key, rand_val)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"cookies": {rand_key: rand_val}} for i in range(12)]
#         rl_page.check_rate_limit_action_503(self.data["rl_base_url"], 3, 5, "/test")
#         time.sleep(5)
#         rl_page.check_rate_limits_action_503_with_params_with_exclude(self.data["rl_base_url"], 10, 5, params, "/test")
#
#     def test_503_action_attr_key_count_by_ip_exclude_arg(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 2, "attrs", "Remote Address")
#         rl_page.click_add_exclude_rule_btn()
#         rl_page.click_on_include_exclude_option("Argument")
#         rl_page.fill_exclude_key_value(rand_key, rand_val)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"params": {rand_key: rand_val}} for i in range(8)]
#         rl_page.check_rate_limit_action_503(self.data["rl_base_url"], 2, 2, "/test")
#         time.sleep(2)
#         rl_page.check_rate_limits_action_503_with_params_with_exclude(self.data["rl_base_url"], 6, 2, params, "/test")
#
#     def test_503_action_attr_key_count_by_ip_exclude_attr(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 4, 4, "attrs", "Remote Address")
#         rl_page.click_add_exclude_rule_btn()
#         rl_page.click_on_include_exclude_option("Attribute")
#         rl_page.send_value_to_field("Country Name", *RateLimitFormLocators.ATTRS_EXCLUDE_KEY_FIELD)
#         rl_page.send_value_to_field("Israel", *RateLimitFormLocators.EXCLUDE_VALUE_INPUT)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limits_action_503_with_exclude(self.data["rl_base_url"], 4, 4, "/test")
#
#     def test_503_action_attr_key_count_by_ip_include_header(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 4, "attrs", "Remote Address")
#         rl_page.click_add_include_rule_btn()
#         rl_page.fill_include_key_value(rand_key, rand_val)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"headers": {rand_key: rand_val}} for i in range(4)]
#         rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url"], 2, 4, params,  "/test")
#
#     def test_503_action_attr_key_count_by_ip_include_cookie(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 3, 2, "attrs", "Remote Address")
#         rl_page.click_add_include_rule_btn()
#         rl_page.click_on_include_exclude_option("Cookie")
#         rl_page.fill_include_key_value(rand_key, rand_val)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"cookies": {rand_key: rand_val}} for i in range(5)]
#         rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url"], 3, 2, params,  "/test")
#
#     def test_503_action_attr_key_count_by_ip_include_arg(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rand_key = rl_page.generate_random_string(10)
#         rand_val = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 5, 3, "attrs", "Remote Address")
#         rl_page.click_add_include_rule_btn()
#         rl_page.click_on_include_exclude_option("Argument")
#         rl_page.fill_include_key_value(rand_key, rand_val)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         params = [{"params": {rand_key: rand_val}} for i in range(7)]
#         rl_page.check_rate_limits_action_503_with_params(self.data["rl_base_url"], 5, 3, params,  "/test")
#
#     def test_503_action_attr_key_count_by_ip_include_attribute(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 100, 30, "attrs", "Remote Address")
#         rl_page.click_add_include_rule_btn()
#         rl_page.click_on_include_exclude_option("Attribute")
#         rl_page.send_value_to_field("Country Name", *RateLimitFormLocators.ATTRS_INCLUDE_KEY_FIELD)
#         rl_page.send_value_to_field("Israel", *RateLimitFormLocators.INCLUDE_VALUE_INPUT)
#         rl_page.click_save_rule_btn()
#         rl_page.attach_rule_to_url_by_path_and_rule_name("rate.test", "Test", rule_name, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_503(self.data["rl_base_url"], 100, 30,  "/test")
#
#     def test_can_add_rule_to_url_from_form_page(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 5, "attrs", "Remote Address")
#         rl_page.click_save_rule_btn()
#         rl_page.switch_to_frame()
#         rl_page.find_element(*RateLimitFormLocators.CHOOSE_SITE_DROPDOWN).click()
#         rl_page.find_element(By.LINK_TEXT, "rate.test").click()
#         time.sleep(0.4)
#         rl_page.find_element(*RateLimitFormLocators.CHOOSE_LOCATION_DROPDOWN).click()
#         time.sleep(0.4)
#         rl_page.find_element(By.LINK_TEXT, "Test - /test").click()
#         btns = rl_page.find_elements(By.XPATH, "//*[local-name()='svg' and @data-icon='plus']")
#         btns[3].click()
#         rl_page.click_save_rule_btn()
#         rl_page.navigate_to_tab("Planet Overview")
#         rl_page.switch_to_frame()
#         rl_page.click_on_site_name_by_name("rate.test")
#         rl_page.switch_to_frame()
#         rl_page.expand_path_settings_by_name("Test")
#         time.sleep(0.4)
#         assert rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_503(self.data["rl_base_url"], 2, 5, "/test")
#
#     def test_can_add_rule_to_all_from_form_page(self):
#         rl_page = RateLimitPage(self.driver)
#         rl_page.open_new_rule_form()
#         rule_name = rl_page.generate_random_string(10)
#         description = rl_page.generate_random_string(10)
#         rl_page.fill_basic_rate_limit_rule(rule_name, description, 2, 5, "attrs", "Remote Address")
#         rl_page.click_save_rule_btn()
#         rl_page.switch_to_frame()
#         rl_page.find_element(*RateLimitFormLocators.CHOOSE_SITE_DROPDOWN).click()
#         rl_page.find_element(By.LINK_TEXT, "rate.test").click()
#         time.sleep(0.4)
#         rl_page.find_element(*RateLimitFormLocators.CHOOSE_LOCATION_DROPDOWN).click()
#         time.sleep(0.4)
#         rl_page.find_element(By.LINK_TEXT, "*Add to all*").click()
#         btns = rl_page.find_elements(By.XPATH, "//*[local-name()='svg' and @data-icon='plus']")
#         btns[3].click()
#         rl_page.click_save_rule_btn()
#         rl_page.navigate_to_tab("Planet Overview")
#         rl_page.switch_to_frame()
#         rl_page.click_on_site_name_by_name("rate.test")
#         rl_page.switch_to_frame()
#         rl_page.expand_path_settings_by_name("Test")
#         time.sleep(0.4)
#         assert rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
#         rl_page.expand_path_settings_by_name("Static Files")
#         time.sleep(0.4)
#         assert rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
#         rl_page.expand_path_settings_by_name("Default")
#         time.sleep(0.4)
#         assert rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
#         rl_page.expand_path_settings_by_name("Root")
#         time.sleep(0.4)
#         assert rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
#         rl_page.expand_path_settings_by_name("Default Path")
#         time.sleep(0.4)
#         assert rl_page.check_rule_attached_to_url(rule_name, 2, 5, description)
#         rl_page.publish_planet(20)
#         rl_page.check_rate_limit_action_503(self.data["rl_base_url"], 2, 5, "/test")
#
#
