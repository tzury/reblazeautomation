import time
from core.base_page import BasePage
from pages.rate_limit_page import RateLimitPage
from locators.rate_limit_locators import RateLimitFormLocators
from locators.planet_overview_locators import PlanetOverviewLocators
from locators.acl_page_locators import *
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from pages.account_page import AccountPage


class Cleanup(BasePage):

    def get_btns_len(self):
        return len(self.get_rate_limit_ids())

    def find_edit_buttons(self):
        return self.find_elements(*RateLimitFormLocators.EDIT_RULE_BTN)

    def check_delete_rule_btn_exists(self):
        return self.is_element_present(*RateLimitFormLocators.DELETE_RULE_BUTTON)

    def click_delete_rule_confirm_delete(self):
        self.explicitly_wait_for_element_to_be_clickable(10, *RateLimitFormLocators.DELETE_RULE_BUTTON)
        self.find_element(*RateLimitFormLocators.DELETE_RULE_BUTTON).click()
        self.explicitly_wait_for_element_to_be_clickable(10, *RateLimitFormLocators.CONFIRM_RATE_LIMIT_DELETE_BUTTON)
        conf_delete = self.find_element(*RateLimitFormLocators.CONFIRM_RATE_LIMIT_DELETE_BUTTON)
        conf_delete.click()
        time.sleep(0.5)
        self.switch_to_frame()
        self.close_rate_limit_save_success_notification()
        if self.is_element_present_no_waits(*RateLimitFormLocators.RULE_NAME_FIELD_EMPTY):
            if self.find_element(*RateLimitFormLocators.RULE_NAME_FIELD_EMPTY).get_attribute(
                    "value") == "New rate limit":
                self.navigate_to_tab("Rate Limiting")
        self.explicitly_wait_for_element(10, (By.XPATH, "//section/iframe"))

    def click_unattach_rule_from_path(self):
        while self.is_element_present(*RateLimitFormLocators.REMOVE_RULE_BTN):
            self.find_element(*RateLimitFormLocators.REMOVE_RULE_BTN).click()
            RateLimitPage(self.driver).click_save_rule_btn()
            time.sleep(0.5)
            self.switch_to_frame()
            self.close_rate_limit_save_success_notification()
            if self.is_element_present_no_waits(*RateLimitFormLocators.RULE_NAME_FIELD_EMPTY):
                if self.find_element(*RateLimitFormLocators.RULE_NAME_FIELD_EMPTY).get_attribute(
                        "value") == "New rate limit":
                    self.navigate_to_tab("Rate Limiting")
            self.explicitly_wait_for_element(10, (By.XPATH, "//section/iframe"))

    def close_rate_limit_save_success_notification(self):
        if self.is_element_present_no_waits(By.XPATH, "//div[@id='page-root']/div/article/div[@class='message-body']"):
            if "successfully" in self.find_element(By.XPATH, "//div[@class='message-body']").text:
                self.find_element(By.XPATH, "//div[@id='page-root']/div/article/button[@class='delete']").click()
                time.sleep(0.2)

    def delete_current_rule(self):
        self.switch_to_frame()
        if self.check_delete_rule_btn_exists():
            self.click_delete_rule_confirm_delete()
        else:
            self.click_unattach_rule_from_path()
            self.click_delete_rule_confirm_delete()

    def delete_all_rate_limit_rules(self):
        self.driver.switch_to.default_content()
        self.navigate_to_tab("Rate Limiting")
        self.switch_to_frame()
        btns_len = self.get_btns_len()
        for i in range(btns_len):
            edit_buttons = self.find_edit_buttons()
            for btn in edit_buttons:
                btn.click()
                self.switch_to_frame()
                if self.check_delete_rule_btn_exists():
                    self.click_delete_rule_confirm_delete()
                    self.explicitly_wait_for_element(10, *RateLimitFormLocators.RULE_NAME_FIELD_EMPTY)
                    btns_len = self.get_btns_len()
                    break
                else:
                    self.click_unattach_rule_from_path()
                    self.click_delete_rule_confirm_delete()
                    self.explicitly_wait_for_element(10, *RateLimitFormLocators.RULE_NAME_FIELD_EMPTY)
                    btns_len = self.get_btns_len()
                    break

    def delete_acl_profiles_and_policies(self, acl_site):
        self.driver.switch_to.default_content()
        self.navigate_to_tab("Planet Overview")
        time.sleep(0.3)
        self.switch_to_frame()
        self.click_on_site_name_by_name(acl_site["path"][0])
        time.sleep(0.3)
        self.switch_to_frame()
        self.expand_path_settings_by_name("Test")
        self.find_element(*PlanetOverviewLocators.ACL_DEFAULT_VALUE).click()
        time.sleep(0.3)
        self.find_element(By.XPATH, f"//option[contains(text(), 'ACL Default')]").click()
        time.sleep(0.3)
        self.find_element(*PlanetOverviewLocators.SAVE_BTN).click()
        time.sleep(0.3)
        self.navigate_to_tab("Profiles")
        self.find_element(*AclMainLocators.PROFILES_BTN).click()
        self.delete_rules()
        self.find_element(*AclMainLocators.ACL_POLICIES_BTN).click()
        self.delete_rules()
        self.publish_planet(3)

    def delete_rule(self, rule_name):
        rule = RateLimitPage(self.driver).extract_required_rule(rule_name)
        self.driver.get("https://rbzauto1.dev.app.reblaze.io/ratelimit/form?id=" + rule["id"])
        self.delete_current_rule()

    def delete_waf_rules(self, waf_site):
        self.driver.switch_to.default_content()
        self.navigate_to_tab("Planet Overview")
        time.sleep(0.3)
        self.switch_to_frame()
        self.click_on_site_name_by_name(waf_site["path"][0])
        time.sleep(0.3)
        self.switch_to_frame()
        self.expand_path_settings_by_name("Test")
        self.find_element(*PlanetOverviewLocators.WAF_DEFAULT_VALUE).click()
        time.sleep(0.3)
        self.find_element(By.XPATH, f"//option[contains(text(), 'WAF Default')]").click()
        time.sleep(0.3)
        self.find_element(*PlanetOverviewLocators.SAVE_BTN).click()
        time.sleep(0.3)
        self.navigate_to_tab("Profiles")
        self.find_element(*AclMainLocators.WAF_POLICIES_BTN).click()
        self.delete_rules()
        self.publish_planet(3)

    def delete_test_accounts(self):
        self.driver.switch_to.default_content()
        self.navigate_to_tab("Account")
        self.switch_to_frame()
        self.navigate_to_tab("Users management")
        AccountPage(self.driver).delete_user_for_cleanup()

    def delete_rules(self):
        rules = self.find_elements(*AclMainLocators.LEFT_PROFILES_RULES)
        to_delete = [a.text for a in rules if "TEST" in a.text]
        ignored_exceptions = (NoSuchElementException, StaleElementReferenceException)
        for i in range(len(to_delete)):
            row = WebDriverWait(self.driver, 40, ignored_exceptions=ignored_exceptions) \
                             .until(expected_conditions.element_to_be_clickable((By.LINK_TEXT, to_delete[i])))
            time.sleep(0.3)
            self.find_element(By.LINK_TEXT, to_delete[i]).click()
            time.sleep(0.2)
            self.find_element(By.LINK_TEXT, "Delete").click()
            time.sleep(0.1)
            self.find_element(*AclMainLocators.CLOSE_SUCCESS_NOTIFICATION_BTN).click()

    def clear_acls_and_custom_sigs(self, cs_site):
        self.driver.switch_to.default_content()
        self.navigate_to_tab("Planet Overview")
        time.sleep(0.3)
        self.switch_to_frame()
        self.click_on_site_name_by_name(cs_site["path"][0])
        time.sleep(0.3)
        self.switch_to_frame()
        self.expand_path_settings_by_name("Test")
        self.find_element(*PlanetOverviewLocators.ACL_DEFAULT_VALUE).click()
        time.sleep(0.3)
        self.find_element(By.XPATH, f"//option[contains(text(), 'ACL Default')]").click()
        time.sleep(0.3)
        self.find_element(*PlanetOverviewLocators.SAVE_BTN).click()
        time.sleep(0.3)
        self.navigate_to_tab("Profiles")
        self.find_element(*AclMainLocators.PROFILES_BTN).click()
        self.delete_rules()
        self.find_element(*AclMainLocators.ACL_POLICIES_BTN).click()
        self.delete_rules()
        self.find_element(*AclMainLocators.CUSTOM_SIGNATURE_BTN).click()
        time.sleep(0.3)
        self.delete_rules()
        self.publish_planet(3)




    # def delete_all_rate_limit_rules(self):
    #     rule_ids = self.get_rate_limit_ids()
    #     for rl_id in rule_ids:
    #         # delete = self.call("XDELETE", "acl", suffix=f"ratelimit/delete?id={str(rl_id[0])}" + " -w %{http_code}")
    #         res = requests.request(
    #             method="delete",
    #             url=f"https://rbzqanew.dev.app.reblaze.io/api/42c36fb38ae98875ec80d73b8e3282c52f7f3aaab9196011f47edf647ab03442/ratelimit/delete?id={rl_id[0]}"
    #
    #         )

    # def publish_planet(self):
    #     publish = self.call("XPOST", "acl", suffix="system/publish -w % {http_code}")

    # curl - s - XPOST $DEBUG "https://rbzauto4.dev.app.reblaze.io/api/8e7f2243f4411cfa42249fd67623ec86c3187d14d102762c72ba1d1fde847529/sites/remove?canonicalname=first.site" DELETE SITE BY NAME

    # curl -s -XPOST $DEBUG https://rbzauto4.dev.app.reblaze.io/api/8e7f2243f4411cfa42249fd67623ec86c3187d14d102762c72ba1d1fde847529/sites/duplicate -d "canonicalname=dup.test&newdomain=dup1.test" -w %{http_code} DUPLICATE SITE

    # curl -s -XPOST $DEBUG  https://rbzauto4.dev.app.reblaze.io/api/8e7f2243f4411cfa42249fd67623ec86c3187d14d102762c72ba1d1fde847529/sites/setnames  -d "canonicalname=dup1.test&names=dup1-test.rbzauto4.dev.rbzdns.com" -w %{http_code} SET SITE DOMAIN
