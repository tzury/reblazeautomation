import pytest
from pages.acl_page import *
from locators.acl_page_locators import *
import time


@pytest.mark.usefixtures("test_setup", "waf_login", "waf_site")
@pytest.mark.all_modules
@pytest.mark.waf_tests
class TestWaf:

    def test_waf_ips_policies_page(self, waf_site):
        waf_page = AclPage(self.driver)
        waf_page.navigate_to_tab("Profiles")
        waf_site["path"].append("waf.test")
        waf_page.wait_for_acl_page_to_load()
        assert not waf_page.is_element_present(*AclMainLocators.WAF_POLICY_NAME_INPUT)
        waf_page.click_waf_rules_btn()
        assert waf_page.is_element_present(*AclMainLocators.LEFT_PROFILES_LIST)
        assert waf_page.is_element_present(*AclMainLocators.WAF_POLICY_NAME_INPUT)
        assert waf_page.is_element_present(*AclMainLocators.WAF_MAX_NUM_ARGS)
        assert waf_page.is_element_present(*AclMainLocators.WAF_MAX_NAME_LEN_ARGS)
        assert waf_page.is_element_present(*AclMainLocators.WAF_RIGHT_PANE)
        assert waf_page.is_element_present(*AclMainLocators.WAF_WHITELIST_NAMES_TEXT)
        assert waf_page.is_element_present(*AclMainLocators.WAF_WHITELIST_IDS_TEXT)
        assert waf_page.is_element_present(By.LINK_TEXT, "WAF Default")
        assert waf_page.is_element_present(By.LINK_TEXT, "Save")
        assert waf_page.is_element_present(By.LINK_TEXT, "Duplicate")
        assert waf_page.is_element_present(By.LINK_TEXT, "Delete")

    def test_can_duplicate_waf(self, waf_site):
        waf_page = AclPage(self.driver)
        waf_page.navigate_to_tab("Profiles")
        waf_site["path"].append("waf.test")
        waf_page.wait_for_acl_page_to_load()
        waf_page.click_waf_rules_btn()
        waf_page.find_element(By.LINK_TEXT, "WAF Default").click()
        text_before = waf_page.get_waf_table_text()
        waf_page.press_duplicate()
        waf_page.clear_field(*AclMainLocators.WAF_POLICY_NAME_INPUT)
        new_name = "TEST" + waf_page.generate_random_string(10)
        waf_page.send_value_to_field(new_name, *AclMainLocators.WAF_POLICY_NAME_INPUT)
        waf_page.press_save()
        waf_page.refresh_page()
        waf_page.click_waf_rules_btn()
        waf_page.find_element(By.LINK_TEXT, new_name).click()
        text_after = waf_page.get_waf_table_text()
        assert text_after == text_before
        assert waf_page.find_element(*AclMainLocators.WAF_POLICY_NAME_INPUT).get_attribute("value") == new_name

    def test_can_delete_waf_rule(self, waf_site):
        waf_page = AclPage(self.driver)
        waf_page.navigate_to_tab("Profiles")
        waf_site["path"].append("waf.test")
        waf_page.wait_for_acl_page_to_load()
        waf_page.click_waf_rules_btn()
        waf_page.find_element(By.LINK_TEXT, "WAF Default").click()
        waf_page.press_duplicate()
        waf_page.clear_field(*AclMainLocators.WAF_POLICY_NAME_INPUT)
        new_name = "TEST" + waf_page.generate_random_string(10)
        waf_page.send_value_to_field(new_name, *AclMainLocators.WAF_POLICY_NAME_INPUT)
        waf_page.press_save()
        waf_page.refresh_page()
        waf_page.click_waf_rules_btn()
        waf_page.find_element(By.LINK_TEXT, new_name).click()
        waf_page.press_delete()
        waf_page.refresh_page()
        assert not waf_page.is_element_present(By.LINK_TEXT, new_name)

    def test_can_add_waf_to_path_and_publish(self, waf_site):
        waf_page = AclPage(self.driver)
        waf_page.navigate_to_tab("Profiles")
        waf_site["path"].append("waf.test")
        waf_page.wait_for_acl_page_to_load()
        waf_page.click_waf_rules_btn()
        waf_page.find_element(By.LINK_TEXT, "WAF Default").click()
        waf_page.press_duplicate()
        waf_page.clear_field(*AclMainLocators.WAF_POLICY_NAME_INPUT)
        new_name = "TEST" + waf_page.generate_random_string(10)
        waf_page.send_value_to_field(new_name, *AclMainLocators.WAF_POLICY_NAME_INPUT)
        waf_page.press_save()
        waf_page.attach_waf_to_url_by_path_and_waf_name("waf.test", "Test", new_name)
        waf_page.refresh_page()
        waf_page.switch_to_frame()
        waf_page.expand_path_settings_by_name("Test")
        waf_page.check_site_attached_acl_profile(new_name)

    @pytest.mark.parametrize("suffix", ["/?item=gift'--", "/?item=gift%22--", "/search.asp?q=<script>",
                                        "/search?q=flowers+%3Cscript%3Eevil_script()%3C/script%3E"])
    def test_waf_blocks_sql_xss_injection(self, waf_site, suffix):
        waf_page = AclPage(self.driver)
        waf_page.navigate_to_tab("Profiles")
        waf_site["path"].append("waf.test")
        waf_page.wait_for_acl_page_to_load()
        waf_page.click_waf_rules_btn()
        waf_page.find_element(By.LINK_TEXT, "WAF Default").click()
        waf_page.press_duplicate()
        waf_page.clear_field(*AclMainLocators.WAF_POLICY_NAME_INPUT)
        new_name = "TEST" + waf_page.generate_random_string(10)
        waf_page.send_value_to_field(new_name, *AclMainLocators.WAF_POLICY_NAME_INPUT)
        waf_page.press_save()
        waf_page.attach_waf_to_url_by_path_and_waf_name("waf.test", "Test", new_name)
        waf_page.publish_planet(21)
        response = waf_page.query(self.data["waf_base_url"], path="/test", suffix=suffix)
        assert response.status_code != 200
        assert response.status_code == 403
        response = waf_page.query(self.data["waf_base_url"], path="/test")
        assert response.status_code == 200

    def test_inactivate_activate_waf_rule(self, waf_site):
        waf_page = AclPage(self.driver)
        waf_page.navigate_to_tab("Profiles")
        waf_site["path"].append("wafactive.test")
        waf_page.wait_for_acl_page_to_load()
        waf_page.click_waf_rules_btn()
        waf_page.find_element(By.LINK_TEXT, "WAF Default").click()
        waf_page.press_duplicate()
        waf_page.clear_field(*AclMainLocators.WAF_POLICY_NAME_INPUT)
        new_name = "TEST" + waf_page.generate_random_string(10)
        waf_page.send_value_to_field(new_name, *AclMainLocators.WAF_POLICY_NAME_INPUT)
        waf_page.press_save()
        waf_page.attach_waf_to_url_by_path_and_waf_name("wafactive.test", "Test", new_name)
        waf_page.click_waf_active_checkbox("Test")
        waf_page.publish_planet(21)
        response = waf_page.query(self.data["wafactive_base_url"], path="/test", suffix="/?item=gift'--")
        assert response.status_code == 200
        response = waf_page.query(self.data["wafactive_base_url"], path="/test")
        assert response.status_code == 200
        waf_page.navigate_to_tab("Planet Overview")
        waf_page.switch_to_frame()
        waf_page.click_on_site_name_by_name("wafactive.test")
        waf_page.switch_to_frame()
        waf_page.click_waf_active_checkbox("Test")
        waf_page.publish_planet(21)
        response = waf_page.query(self.data["wafactive_base_url"], path="/test", suffix="/?item=gift'--")
        assert response.status_code != 200
        assert response.status_code == 403
        response = waf_page.query(self.data["wafactive_base_url"], path="/test")
        assert response.status_code == 200

    # def test_argument_number_limit(self, waf_site):
    #     waf_page = AclPage(self.driver)
    #     waf_page.navigate_to_tab("Profiles")
    #     waf_site["path"].append("waf.test")
    #     waf_page.wait_for_acl_page_to_load()
    #     waf_page.click_waf_rules_btn()
    #     waf_page.find_element(By.LINK_TEXT, "WAF Default").click()
    #     waf_page.press_duplicate()
    #     waf_page.clear_field(*AclMainLocators.WAF_POLICY_NAME_INPUT)
    #     new_name = waf_page.generate_random_string(10)
    #     waf_page.send_value_to_field(new_name, *AclMainLocators.WAF_POLICY_NAME_INPUT)
    #     waf_page.press_save()
    #     waf_page.attach_waf_to_url_by_path_and_waf_name("waf.test", "Test", new_name)
    #     waf_page.publish_planet(21)
    #     params = waf_page.generate_params(1024)
    #     response = waf_page.query(self.data["waf_base_url"], path="/test", params=f"/{params}")
    #
    # def test_argument_name_length_limit(self, waf_site):
    #     waf_page = AclPage(self.driver)
    #     waf_page.navigate_to_tab("Profiles")
    #     waf_site["path"].append("waf.test")
    #     response = waf_page.query(self.data["waf_base_url"], path="/test", method="/?item=gift'--")
    #     waf_page.wait_for_acl_page_to_load()

    def test_can_cancel_injection_blockage(self, waf_site):
        waf_page = AclPage(self.driver)
        waf_page.navigate_to_tab("Profiles")
        waf_site["path"].append("waf.test")
        waf_page.wait_for_acl_page_to_load()
        waf_page.click_waf_rules_btn()
        waf_page.find_element(By.LINK_TEXT, "WAF Default").click()
        waf_page.press_duplicate()
        waf_page.clear_field(*AclMainLocators.WAF_POLICY_NAME_INPUT)
        new_name = "TEST" + waf_page.generate_random_string(10)
        waf_page.send_value_to_field(new_name, *AclMainLocators.WAF_POLICY_NAME_INPUT)
        waf_page.find_element(*AclMainLocators.WAF_INJECTION_TOGGLE).click()
        waf_page.press_save()
        waf_page.attach_waf_to_url_by_path_and_waf_name("waf.test", "Test", new_name)
        waf_page.publish_planet(21)
        response = waf_page.query(self.data["waf_base_url"], path="/test", suffix="/?item=gift'--")
        assert response.status_code == 200
        response = waf_page.query(self.data["waf_base_url"], path="/test",
                                  suffix="/search?q=flowers+%3Cscript%3Eevil_script()%3C/script%3E")
        assert response.status_code == 200

    def test_can_edit_waf_rule(self,waf_site):
        waf_page = AclPage(self.driver)
        waf_page.navigate_to_tab("Profiles")
        waf_site["path"].append("waf.test")
        waf_page.wait_for_acl_page_to_load()
        waf_page.click_waf_rules_btn()
        waf_page.find_element(By.LINK_TEXT, "WAF Default").click()
        waf_page.press_duplicate()
        waf_page.clear_field(*AclMainLocators.WAF_POLICY_NAME_INPUT)
        new_name = "TEST" + waf_page.generate_random_string(10)
        waf_page.send_value_to_field(new_name, *AclMainLocators.WAF_POLICY_NAME_INPUT)
        waf_page.find_element(*AclMainLocators.WAF_INJECTION_TOGGLE).click()
        waf_page.press_save()
        waf_page.attach_waf_to_url_by_path_and_waf_name("waf.test", "Test", new_name)
        waf_page.publish_planet(21)
        response = waf_page.query(self.data["waf_base_url"], path="/test", suffix="/?item=gift'--")
        assert response.status_code == 200
        waf_page.navigate_to_tab("Profiles")
        waf_page.click_waf_rules_btn()
        waf_page.find_element(By.LINK_TEXT, new_name).click()
        waf_page.find_element(*AclMainLocators.WAF_INJECTION_TOGGLE).click()
        waf_page.press_save()
        waf_page.publish_planet(23)
        response = waf_page.query(self.data["waf_base_url"], path="/test", suffix="/?item=gift'--")
        assert response.status_code == 403

    def test_can_remove_and_add_white_list_ids(self, waf_site):
        waf_page = AclPage(self.driver)
        waf_page.navigate_to_tab("Profiles")
        waf_site["path"].append("waf.test")
        waf_page.wait_for_acl_page_to_load()
        waf_page.click_waf_rules_btn()
        waf_page.find_element(By.LINK_TEXT, "WAF Default").click()
        waf_page.press_duplicate()
        waf_page.clear_field(*AclMainLocators.WAF_POLICY_NAME_INPUT)
        new_name = "TEST" + waf_page.generate_random_string(10)
        waf_page.send_value_to_field(new_name, *AclMainLocators.WAF_POLICY_NAME_INPUT)
        waf_page.clear_field(*AclMainLocators.WAF_WHITELIST_IDS_TEXT)
        waf_page.send_value_to_field("310005\n100024", *AclMainLocators.WAF_WHITELIST_IDS_TEXT)
        waf_page.press_save()
        waf_page.attach_waf_to_url_by_path_and_waf_name("waf.test", "Test", new_name)
        waf_page.publish_planet(25)
        response = waf_page.query(self.data["waf_base_url"], path="/test/.w/")
        assert response.status_code == 403
        waf_page.navigate_to_tab("Profiles")
        waf_page.click_waf_rules_btn()
        waf_page.find_element(By.LINK_TEXT, new_name).click()
        waf_page.clear_field(*AclMainLocators.WAF_WHITELIST_IDS_TEXT)
        waf_page.send_value_to_field("310005\n100024\n300005\n300072", *AclMainLocators.WAF_WHITELIST_IDS_TEXT)
        waf_page.press_save()
        waf_page.publish_planet(25)
        response = waf_page.query(self.data["waf_base_url"], path="/test/.w/")
        assert response.status_code == 200


