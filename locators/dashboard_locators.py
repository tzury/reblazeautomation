from selenium.webdriver.common.by import By


class DashboardLocators:
    SEARCH_INPUT = (By.XPATH, "//form[@id='filter-form']//input")
    SEARCH_BTN = (By.XPATH, "//form[@id='filter-form']//button")
    PASSED_VS_BLOCKED = (By.ID, "passed-blocked-timeline")
    SINGLE_HOUR_IN_PASSED_VS_BLOCKED_CHART = (By.XPATH, "//div[@id='passed-blocked-timeline']//*[name()='svg']/*[name()='g']/*[name()='text']")
    PASSED_CHART_DATA = (By.XPATH, "//div[@id='passed-blocked-timeline']//*[name()='svg']/*[name()='g'][1]/*[name()='path']")
    PASSED_CHART = (By.XPATH, "//div[@id='passed-blocked-timeline']//*[name()='svg']/*[name()='g'][5]/*[name()='g']")
    PASSED_CHART_LOCATION = (By.XPATH, "//div[@id='passed-blocked-timeline']//*[name()='svg']/*[name()='g'][5]/*[name()='g']/*[name()='path']")
    PASSED_CHART_DATA_DATE_TEXT = (By.XPATH, "//div[@id='passed-blocked-timeline']//*[name()='svg']/*[name()='g'][9]/*[name()='text']/*[name()='tspan'][1]")
    PASSED_CHART_DATA_TEXT = (By.XPATH, "//div[@id='passed-blocked-timeline']//*[name()='svg']/*[name()='g'][9]/*[name()='text']/*[name()='tspan'][4]")
    PASSED_CHART_BLOCKED_TEXT = (By.XPATH, "//div[@id='passed-blocked-timeline']//*[name()='svg']/*[name()='g'][9]/*[name()='text']/*[name()='tspan'][7]")
    PASSED_CHART_HITS_BLOCKED = (By.XPATH, "//div[@id='passed-blocked-timeline']//*[name()='svg']/*[name()='g'][6]//*[name()='g']//*[name()='tspan']")

    RESPONSE_STATUS = (By.ID, "status-timeline")
    SINGLE_HOUR_IN_RESPONSE_STATUS_CHART = (By.XPATH, "//div[@id='status-timeline']//*[name()='svg']/*[name()='g']/*[name()='text']")
    STATUS_CHART_LOCATION = (By.XPATH, "//div[@id='status-timeline']//*[name()='svg']/*[name()='g'][5]/*[name()='g']/*[name()='path']")
    STATUS_CHART_DATA_DATE_TEXT = (By.XPATH, "//div[@id='status-timeline']//*[name()='svg']/*[name()='g'][9]/*[name()='text']/*[name()='tspan'][1]")

    STATUS_CHART_DATA_TEXT = (By.XPATH,
                              "//div[@id='status-timeline']//*[name()='svg']/*[name()='g'][9]/*[name()='text']/*[name()='tspan'][4]")
    STATUS_CHART_BLOCKED_TEXT = (By.XPATH,
                                 "//div[@id='status-timeline']//*[name()='svg']/*[name()='g'][9]/*[name()='text']/*[name()='tspan'][7]")
    STATUS_CHART_ONLY_BLOCKED_TEXT = (By.XPATH,
                                 "//div[@id='status-timeline']//*[name()='svg']/*[name()='g'][9]/*[name()='text']/*[name()='tspan'][4]")
    STATUS_CHART_BLOCKED_RATE_LIMIT_TEXT = (By.XPATH,
                                 "//div[@id='status-timeline']//*[name()='svg']/*[name()='g'][9]/*[name()='text']/*[name()='tspan'][10]")
    STATUS_CHART_ONLY_BLOCKED_RATE_LIMIT_TEXT = (By.XPATH,
                                            "//div[@id='status-timeline']//*[name()='svg']/*[name()='g'][9]/*[name()='text']/*[name()='tspan'][7]")
    STATUS_CHART_HITS_BLOCKED = (By.XPATH,
                                 "//div[@id='status-timeline']//*[name()='svg']/*[name()='g'][6]//*[name()='g']//*[name()='tspan']")

    TOTAL_BANDWIDTH = (By.ID, "global-bandwidth-chart")
    SINGLE_HOUR_IN_TOTAL_BANDWIDTH_CHART = (By.XPATH, "//div[@id='global-bandwidth-chart']//*[name()='svg']/*[name()='g']/*[name()='text']")

    UNIQUE_SESSIONS = (By.ID, "global-unique-sessions-count-chart")
    SINGLE_HOUR_IN_UNIQUE_SESSIONS_CHART = (By.XPATH, "//div[@id='global-unique-sessions-count-chart']//*[name()='svg']/*[name()='g']/*[name()='text']")



    LATENCY = (By.ID, "traffic-flow-latency-chart")
    SINGLE_HOUR_IN_LATENCY_CHART = (By.XPATH, "//div[@id='traffic-flow-latency-chart']//*[name()='svg']/*[name()='g'][6]/*[name()='text']")



    BANDWIDTH = (By.ID, "traffic-flow-bandwidth-chart")
    SINGLE_HOUR_IN_BANDWIDTH_CHART = (By.XPATH, "//div[@id='traffic-flow-bandwidth-chart']//*[name()='svg']/*[name()='g']/*[name()='text']")



    REQUEST_COUNT = (By.ID, "traffic-flow-requests-count-chart")
    SINGLE_HOUR_IN_REQUEST_COUNT_CHART = (By.XPATH, "//div[@id='traffic-flow-requests-count-chart']//*[name()='svg']/*[name()='g']/*[name()='text']")
    REQUEST_COUNT_CHART_LOCATION = (By.XPATH, "//div[@id='traffic-flow-requests-count-chart']//*[name()='svg']/*[name()='g'][5]/*[name()='g']/*[name()='path']")
    REQUEST_COUNT_CHART_DATA_DATE_TEXT = (By.XPATH, "//div[@id='traffic-flow-requests-count-chart']//*[name()='svg']/*[name()='g'][8]/*[name()='text']/*[name()='tspan'][1]")
    REQUEST_COUNT_CHART_DATA_TEXT = (By.XPATH, "//div[@id='traffic-flow-requests-count-chart']//*[name()='svg']/*[name()='g'][8]/*[name()='text']/*[name()='tspan'][4]")
    REQUEST_COUNT_CHART_BLOCKED_TEXT = (By.XPATH, "//div[@id='traffic-flow-requests-count-chart']//*[name()='svg']/*[name()='g'][8]/*[name()='text']/*[name()='tspan'][7]")
    REQUEST_COUNT_CHART_HITS_BLOCKED = (By.XPATH, "//div[@id='traffic-flow-requests-count-chart']//*[name()='svg']/*[name()='g'][6]//*[name()='g']//*[name()='tspan']")


    RIGHT_NAV_TABS = (By.XPATH, "//*[contains(@class, 'navbar navbar-inverse')]")
    BOTTOM_NAV_TABS = (By.XPATH, "//ul[contains(@class, 'nav nav-tabs')]")
    TOP_SITES = (By.ID, "top-sites-tab")
    LOGIN_ERROR_MSG = (By.ID, "msg")

    DASHBOARD_RANGE_PICKER = (By.ID, "reportrange")


class BottomNavLocators:
    APPLICATIONS_BTN = (By.LINK_TEXT, "Applications")
    APPLICATIONS_TAB = (By.ID, "top-sites-frame")
    APPLICATIONS_ROW = (By.XPATH, "//div[@id='top-sites-frame']//tbody/tr")

    COUNTRIES_BTN = (By.LINK_TEXT, "Countries")
    COUNTRIES_TAB = (By.ID, "top-country-tab")
    COUNTRIES_ROW = (By.XPATH, "//div[@id='top-country-hits']//tbody/tr")

    SOURCES_BTN = (By.LINK_TEXT, "Sources")
    SOURCES_TAB = (By.ID, "top-ip-tab")
    SOURCES_ROW = (By.XPATH, "//div[@id='top-ip-tab']//tbody/tr")

    SESSIONS_BTN = (By.LINK_TEXT, "Sessions")
    SESSIONS_TAB = (By.ID, "top-rbzid-hits")
    SESSIONS_ROW = (By.XPATH, "//div[@id='top-rbzid-hits']//tbody/tr")

    TARGETS_BTN = (By.LINK_TEXT, "Targets")
    TARGETS_TAB = (By.ID, "top-uri-tab")
    TARGETS_ROW = (By.XPATH, "//div[@id='top-uri-tab']//tbody/tr")

    SIGNATURES_BTN = (By.LINK_TEXT, "Signatures")
    SIGNATURES_TAB = (By.ID, "top-sig-hits")
    SIGNATURES_ROW = (By.XPATH, "//div[@id='top-sig-hits']//tbody/tr")

    REFERERS_BTN = (By.LINK_TEXT, "Referers")
    REFERERS_TAB = (By.ID, "top-referer-tab")
    REFERERS_ROW = (By.XPATH, "//div[@id='top-referer-tab']//tbody/tr")

    BROWSERS_BTN = (By.LINK_TEXT, "Browsers")
    BROWSERS_TAB = (By.ID, "top-ua-tab")
    BROWSERS_ROW = (By.XPATH, "//div[@id='top-ua-tab']//tbody/tr")

    COMPANIES_BTN = (By.LINK_TEXT, "Companies")
    COMPANIES_TAB = (By.ID, "top-org-tab")
    COMPANIES_ROW = (By.XPATH, "//div[@id='top-org-tab']//tbody/tr")

    TOTAL_TIME_BTN = (By.LINK_TEXT, "Total Time")
    TOTAL_TIME_TAB = (By.ID, "top-total-latency-tab")
    TOTAL_TIME_ROW = (By.XPATH, "//div[@id='top-total-latency-tab']//tbody/tr")

    REBLAZE_TIME_BTN = (By.LINK_TEXT, "Reblaze Time")
    REBLAZE_TIME_TAB = (By.ID, "top-rbz-latency")
    REBLAZE_TIME_ROW = (By.XPATH, "//div[@id='top-rbz-latency']//tbody/tr")

    ORIGIN_TIME_BTN = (By.LINK_TEXT, "Origin Time")
    ORIGIN_TIME_TAB = (By.ID, "top-upstream-latency")
    ORIGIN_TIME_ROW = (By.XPATH, "//div[@id='top-upstream-latency']//tbody/tr")

    BOTTOM_TAB_ROW = (By.XPATH, "//div[@id='top-sites-tab']//tbody/tr")

