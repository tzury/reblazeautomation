import time
from datetime import timedelta
from core.base_page import *
import datetime
from selenium.webdriver.common.action_chains import ActionChains


class ViewLogPage(BasePage):
    def navigate_to_view_log_page(self):
        self.navigate_to_tab("View Log")

    def press_clear_filter_btn(self):
        self.find_element(*ViewLogLocators.CLEAR_BTN).click()
        time.sleep(0.5)

    def press_search_btn(self):
        self.find_element(*ViewLogLocators.SEARCH_BTN).click()
        time.sleep(6)

    def press_calendar_btn(self):
        self.find_element(*ViewLogLocators.CALENDAR_BTN).click()
        time.sleep(0.5)

    def press_open_calendar_button(self):
        self.find_element(*ViewLogLocators.OPEN_CALENDAR_BTN).click()
        time.sleep(0.5)

    def press_calendar_cancel_button(self):
        self.find_element(*ViewLogLocators.CALENDAR_CANCEL_BTN).click()
        time.sleep(0.5)

    def press_calendar_apply_btn(self):
        self.find_element(*ViewLogLocators.CALENDAR_APPLY_BTN).click()
        time.sleep(0.5)

    def press_download_btn(self):
        self.find_element(*ViewLogLocators.DOWNLOAD_BTN).click()
        time.sleep(0.5)

    def press_help_btn(self):
        self.find_element(*ViewLogLocators.HELP_BTN).click()
        time.sleep(0.5)

    def press_limit_dropdown(self):
        self.find_element(*ViewLogLocators.ROWS_LIMIT_DROPDOWN).click()
        time.sleep(0.5)

    def select_limit_max_by_text(self, text):
        self.find_element(By.LINK_TEXT, text).click()
        time.sleep(10)

    def return_length_of_log_rows(self):
        return len(self.find_elements(*ViewLogLocators.LOG_SINGLE_LINE))

    def return_displayed_length_of_log_rows(self):
        text = self.find_element(*ViewLogLocators.RESULTS_NUMBER).text
        return int(''.join(a for a in text if a.isdigit()))

    def get_this_log_row_by_path(self, path):
        rows = self.find_elements(*ViewLogLocators.LOG_SINGLE_LINE)
        this_row = None
        for row in rows:
            if path in row.text:
                this_row = row
                break
        return this_row

    def get_all_log_lines(self):
        return self.find_elements(*ViewLogLocators.LOG_SINGLE_LINE)

    def check_log_row_contains_correct_details(self, path, ip=None, url=None, date=None, method=None, response=None):
        this_row = self.get_this_log_row_by_path(path)
        if ip is not None:
            return ip in this_row.text
        if url is not None:
            return url.replace("http://", "") in this_row.text
        if date is not None:
            return datetime.datetime.today().strftime("%Y-%m-%d") in this_row.text
        if method is not None:
            return "GET" in this_row.text
        if response is not None:
            return response in this_row.text

    def click_on_log_row_by_path(self, path):
        rows = self.find_elements(*ViewLogLocators.LOG_SINGLE_LINE)
        for row in rows:
            if path in row.text:
                row.click()
                break
        time.sleep(2.5)

    def check_log_footer_contains_correct_details(self, ip=None, country=None, code=None,
                                                  method=None, response=None, asn=None, http=None, acl=None, rl=None):
        row_footer = self.find_element(*ViewLogLocators.PANEL_FOOTER).text
        if ip is not None:
            return ip in row_footer
        if country is not None:
            return country in row_footer
        if code is not None:
            return code in row_footer
        if method is not None:
            return "GET" in row_footer
        if response is not None:
            return response in row_footer
        if asn is not None:
            return asn in row_footer
        if http is not None:
            return "HTTP/1.1" in row_footer
        if acl is not None:
            return acl in row_footer
        if rl is not None:
            return rl in row_footer

    def open_log_details_popup(self, path):
        self.click_on_log_row_by_path(path)
        self.click_on_view_details_btn()

    def close_details_popup(self):
        self.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_CLOSE_BTN).click()
        time.sleep(0.5)

    def open_search_history_popup(self):
        self.find_element(*ViewLogLocators.HISTORY_BTN).click()
        time.sleep(0.5)

    def click_on_view_details_btn(self):
        self.explicitly_wait_for_element(20, *ViewLogLocators.VIEW_DETAILS_BTN)
        self.explicitly_wait_for_element_to_be_clickable(20, *ViewLogLocators.VIEW_DETAILS_BTN)
        self.find_element(*ViewLogLocators.VIEW_DETAILS_BTN).click()
        time.sleep(1)

    def check_popup_header_contains_correct_data(self, method=None, http=None, response=None,
                                                 country=None, code=None, txt=None):
        popup_header = self.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_HEADER).text

        if method is not None:
            return "GET" in popup_header
        if http is not None:
            return "HTTP/1.1" in popup_header
        if response is not None:
            return response in popup_header
        if country is not None:
            return country in popup_header
        if code is not None:
            return code in popup_header
        if txt is not None:
            return "text/simple;charset=UTF-8" in popup_header

    def check_popup_header_contains_correct_data_for_blocked_request(self, method=None, http=None, response=None,
                                                                     country=None, code=None, txt=None):
        popup_header = self.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_HEADER).text

        if method is not None:
            return "GET" in popup_header
        if http is not None:
            return "HTTP/1.1" in popup_header
        if response is not None:
            return response in popup_header
        if country is not None:
            return country in popup_header
        if code is not None:
            return code in popup_header
        if txt is not None:
            return "text/html" in popup_header

    def check_popup_heading_contains_correct_data(self, ip=None, asn=None, host=None, path=None, acl=None, rl=None):
        popup_heading = self.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_HEADING).text
        if ip is not None:
            return ip in popup_heading
        if asn is not None:
            return asn in popup_heading
        if host is not None:
            return host.replace("http://", "") in popup_heading
        if path is not None:
            return path in popup_heading
        if acl is not None:
            return acl in popup_heading
        if rl is not None:
            return rl in popup_heading

    def check_popup_footer_first_row_contains_correct_data(self, ip=None, country=None):
        footer_first_row = self.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER_FIRST_ROW).text
        results = ["aclid:0000" in footer_first_row, "aclname:acl-default" in footer_first_row,
                   ip.replace(".", "-") in footer_first_row,
                   "wafid:0001" in footer_first_row, "wafname:waf-default" in footer_first_row,
                   country.lower() in footer_first_row, "all" in footer_first_row]
        return results

    def check_popup_footer_first_row_contains_correct_data_for_blocked_request(self, ip=None, country=None):
        footer_first_row = self.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER_FIRST_ROW).text
        results = ["aclid:69232315" in footer_first_row, "aclname:block" in footer_first_row,
                   ip.replace(".", "-") in footer_first_row,
                   "wafid:0001" in footer_first_row, "wafname:waf-default" in footer_first_row,
                   country.lower() in footer_first_row, "all" in footer_first_row]
        return results

    def check_popup_footer_contains_correct_data(self, ip=None):
        footer_first_row = self.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER).text
        results = ["connection" in footer_first_row, "Keep-Alive" in footer_first_row, "via" in footer_first_row,
                   "accept-encoding" in footer_first_row, "gzip, deflate" in footer_first_row,
                   "x-cloud-trace-context" in footer_first_row,
                   "x-forwarded-for" in footer_first_row, ip in footer_first_row,
                   "x-forwarded-proto" in footer_first_row, "http" in footer_first_row, "accept" in footer_first_row,
                   "*/*" in footer_first_row]
        return results

    # adds a non header tag to search field
    def double_click_on_tag_by_text(self, text):
        tag = self.find_element(By.XPATH, f"//div[@class='panel-footer']//span[contains(text(), '{text}')]")
        ActionChains(self.driver).double_click(tag).perform()
        time.sleep(0.5)

    # adds a non header tag to search field
    def double_click_on_tag_by_text_in_popup(self, text):
        tag = self.find_element(By.XPATH, f"//span[contains(text(), '{text}')]")
        ActionChains(self.driver).double_click(tag).perform()
        time.sleep(0.5)

    # adds header tag to search field
    def double_click_on_header_row_by_text(self, text):
        tag = self.find_element(By.XPATH, f"//tr/th/pre[contains(text(), '{text}')]")
        ActionChains(self.driver).double_click(tag).perform()
        time.sleep(0.5)

    # For searches by tags or headers, validates all rows have the value
    def check_all_log_lines_have_specific_text(self, value):
        rows = self.find_elements(*ViewLogLocators.LOG_SINGLE_LINE)
        for row in rows:
            if value not in row.text:
                return False
        return True

    # check your request is saved in history
    def check_search_history_contains_desired_value(self, value):
        rows = self.find_elements(*ViewLogLocators.SEARCH_HISTORY_SINGLE_ROW)
        for row in rows:
            if row.text == value:
                return True
        return False

    # click on history row by required value
    def click_history_row_by_value(self, value):
        self.find_element(By.LINK_TEXT, value).click()
        time.sleep(0.5)

    # for logs search, returns a time in strf format minus the required gap.
    # there is an automatic adjustment in case date changes
    def return_time_with_delta(self, gap):
        d = datetime.datetime.today().utcnow() - timedelta(hours=gap)
        return d.strftime("%a %b %d %Y %H:%M:%S")

    def return_day_time_with_delta(self, gap):
        d = datetime.datetime.today().utcnow() - timedelta(days=gap)
        return d.strftime("%y-%m-%d %H:%M:%S")

    def return_day_time_with_delta_for_comparing_range(self, gap):
        d = datetime.datetime.today().utcnow() - timedelta(days=gap)
        strf = str(d.strftime("%Y-%m-%d"))
        return [datetime.datetime.strptime(strf, "%Y-%m-%d")]

    def return_day_time_with_delta_in_search_bar_format(self, gap):
        d = datetime.datetime.today().utcnow() - timedelta(days=gap)
        return d.strftime("%a %b %d %Y %H:%M:%S")

    def return_day_time_with_delta_in_search_bar_format_zero_seconds(self, gap):
        d = datetime.datetime.today().utcnow() - timedelta(days=gap)
        return d.strftime("%a %b %d %Y %H:%M:00")

    # takes the required gaps - from today minus gap_from to today minus gap_to
    def fill_calendar_from_to_fields(self, gap_from, gap_to):
        self.clear_field(*ViewLogLocators.CALENDAR_FROM_INPUT)
        self.clear_field(*ViewLogLocators.CALENDAR_TO_INPUT)
        self.send_value_to_field(self.return_day_time_with_delta(gap_from), *ViewLogLocators.CALENDAR_FROM_INPUT)
        time.sleep(0.5)
        self.send_value_to_field(self.return_day_time_with_delta(gap_to), *ViewLogLocators.CALENDAR_TO_INPUT)
        time.sleep(0.5)
        self.find_element(*ViewLogLocators.CALENDAR).click()
        time.sleep(0.5)

    def check_custom_range_active(self):
        return self.find_element(By.XPATH, "//li[contains(text(), 'Custom Range')]").get_attribute(
            "class") == "active"

    # extracts the exact day in order to locate it in calendar
    def check_from_to_dates_are_active(self, gap_from, gap_to):
        date_from_raw = self.return_day_time_with_delta(gap_from).split()[0]
        day_from = date_from_raw.split("-")[2].lstrip("0")

        date_to_raw = self.return_day_time_with_delta(gap_to).split()[0]
        day_to = date_to_raw.split("-")[2].lstrip("0")

        date_from_actual = self.check_date_from_active()
        date_to_actual = self.check_date_to_active()
        return date_to_actual == day_to and date_from_actual == day_from

    def check_date_from_active(self):
        if self.is_element_present(By.XPATH,
                                   "//div[@class='calendar-table']//tr//td[contains(@class, 'active start-date in-range available')]"):
            date_from = self.find_element(By.XPATH,
                                          "//div[@class='calendar-table']//tr//td[contains(@class, 'active start-date in-range available')]").text
        else:
            date_from = self.find_element(By.XPATH,
                                   "//div[@class='calendar-table']//tr//td[contains(@class, 'active start-date available')]").text
        return date_from

    def check_date_to_active(self):
        if self.is_element_present(By.XPATH,
                                   "//div[@class='calendar-table']//tr//td[contains(@class, 'active end-date in-range available')]"):
            date_to = self.find_element(By.XPATH,
                                          "//div[@class='calendar-table']//tr//td[contains(@class, 'active end-date in-range available')]").text
        else:
            date_to = self.find_element(By.XPATH,
                                   "//div[@class='calendar-table']//tr//td[contains(@class, 'active end-date available')]").text
        return date_to

    def extract_date_from_log_rows(self):
        rows = self.find_elements(*ViewLogLocators.LOG_SINGLE_LINE)
        dates = [datetime.datetime.strptime(r.text.split()[-2:][0].split("T")[0], "%Y-%m-%d") for r in rows]
        return dates

    def check_dates_in_range(self, actual_dates, start, end):
        results = []
        for date in actual_dates:
            if datetime.datetime.date(start[0]) <= datetime.datetime.date(date) <= datetime.datetime.date(end[0]):
                results.append(True)
            else:
                results.append(False)
        return results


