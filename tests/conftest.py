from core import config
from core.config import get_config
from core.driver_manager import get_driver
from core.login_data_vars import *
from core.cleanup import *
from core.mailer import go_mail
from os.path import isfile, join
from pages.login_page import LoginPage
import pytest
import pyotp
import datetime


def pytest_addoption(parser):
    # in terminal, adding --bro chrome|firefox|safari will run the test on the browser of your choice
    parser.addoption(
        "--bro",
        action="store",
        default=None,
        help="please choose browser as a must param: --bro"
    )
    # in terminal, --module acl|tag|ssl|rl will run only the desired module. default is all - all modules
    parser.addoption(
        "--module",
        action="store",
        default="all",
        help="please enter module name as a must param: --module")
    parser.addoption(
        "--mail",
        action="store",
        default="0",
        help="please specify mailing option --mail: 0 do not send, 1 send"
    )


# if module not specified, take value from config file
@pytest.fixture(scope="session")
def module(request):
    module = request.config.getoption("--module")
    if not module:
        with open(os.path.join(config.HOME_PATH, "tests", "config.json")) as config_file:
            config_data = json.load(config_file)
            module = config_data["module"]
    return module


# if module all - merge all data files. else, take specified data file for module
@pytest.fixture(scope="session")
def data(module):
    if module == "all":
        path = os.path.join(config.HOME_PATH, "data")
        only_files = [f for f in os.listdir(path) if isfile(join(path, f))]
        merged_data = {}
        for file in only_files:
            with open(path + "/" + file) as infile:
                loaded = json.load(infile)
                merged_data.update(loaded)
        return merged_data
    else:
        env_data_files = {
            "acl": "acl_data.json",
            "rl": "rl_data.json",
            "tag": "tag_data.json",
            "ssl": "ssl_data.json"
        }
        with open(os.path.join(config.HOME_PATH, "data", env_data_files[module])) as data_file:
            data = json.load(data_file)
        return data


def delete_all_screenshots():
    path = os.path.join(config.HOME_PATH, "assets", "screenshots")
    for f in os.listdir(path):
        if f != ".gitkeep":
            os.remove(os.path.join(path, f))


# loads driver. if --bro not specified: in config.json you can set chrome, safari, firefox and safari too.
@pytest.fixture(scope="session")
def driver(request):
    delete_all_screenshots()
    if request.config.getoption("--bro") is not None:
        config_browser = request.config.getoption("--bro")
    else:
        config_browser = get_config("browser")
    driver = get_driver(config_browser)
    yield driver
    driver.quit()


def pytest_terminal_summary(terminalreporter, exitstatus):
    """add additional section in terminal summary reporting."""
    if not hasattr(terminalreporter.config, 'workerinput'):
        if terminalreporter.config.getoption("--mail").lower() == "1":
            # Initialize the Email_Pytest_Report object
            go_mail()


# @pytest.fixture(scope="session")
# def cleanup(driver):
#     planets = {
#         "rl_names": []
#     }
#     yield planets
#     clean = Cleanup(driver)
#     clean.delete_not_attached_rate_limit_rules()


# loads all settings for running tests
@pytest.fixture(scope="class")
def test_setup(request, data, driver):
    request.cls.data = data
    request.cls.driver = driver


# generates otp for login
def get_otp(module_otp):
    otp = pyotp.TOTP(module_otp).now()
    return otp


# collect acl planet path for cleanup
@pytest.fixture(scope="function")
def acl_site():
    site = {
        "path": []
    }
    return site


# collect cs planet path for cleanup
@pytest.fixture(scope="function")
def cs_site():
    site = {
        "path": []
    }
    return site


# collect waf planet path for cleanup
@pytest.fixture(scope="function")
def waf_site():
    site = {
        "path": []
    }
    return site


# login to waf/acl planet
@pytest.fixture(scope="function")
def acl_login(driver, data, acl_site):
    # loads user, password, otp and passes to login func
    login = LoginPage(driver)
    seed = get_login_data("acl", "otp_seed")
    yield login.login_on_startup(
        data["acl_url"],
        get_login_data("acl", "acl_user"),
        get_login_data("acl", "acl_pass"),
        seed
    )
    # delete created profiles
    clean = Cleanup(driver)
    clean.delete_acl_profiles_and_policies(acl_site)


# login to cs planet
@pytest.fixture(scope="function")
def cs_login(driver, data, cs_site):
    # loads user, password, otp and passes to login func
    login = LoginPage(driver)
    seed = get_login_data("vl", "otp_seed")
    yield login.login_on_startup(
        data["cs_url"],
        get_login_data("vl", "vl_user"),
        get_login_data("vl", "vl_pass"),
        seed
    )
    # delete created profiles
    clean = Cleanup(driver)
    clean.clear_acls_and_custom_sigs(cs_site)


# login to waf/acl planet
@pytest.fixture(scope="function")
def waf_login(driver, data, waf_site):
    # loads user, password, otp and passes to login func
    login = LoginPage(driver)
    seed = get_login_data("acl", "otp_seed")
    yield login.login_on_startup(
        data["acl_url"],
        get_login_data("acl", "acl_user"),
        get_login_data("acl", "acl_pass"),
        seed
    )
    clean = Cleanup(driver)
    clean.delete_waf_rules(waf_site)


# login to dashboard planet
@pytest.fixture(scope="session")
def dashboard_login(driver, data):
    # loads user, password, otp and passes to login func
    login = LoginPage(driver)
    seed = get_login_data("acl", "otp_seed")
    yield login.login_on_startup(
        data["acl_url"],
        get_login_data("acl", "acl_user"),
        get_login_data("acl", "acl_pass"),
        seed
    )


# login to general tests
@pytest.fixture(scope="function")
def general_login(driver, data):
    # loads user, password, otp and passes to login func
    login = LoginPage(driver)
    seed = get_login_data("acl", "otp_seed")
    yield login.login_on_startup(
        data["acl_url"],
        get_login_data("acl", "acl_user"),
        get_login_data("acl", "acl_pass"),
        seed
    )


# login to acl planet
@pytest.fixture(scope="function")
def account_login(driver, data):
    # loads user, password, otp and passes to login func
    login = LoginPage(driver)
    seed = get_login_data("acl", "otp_seed")
    yield login.login_on_startup(
        data["acl_url"],
        get_login_data("acl", "acl_user"),
        get_login_data("acl", "acl_pass"),
        seed
    )


# login to rate limit planet to test accounts user 20 rebauto1@yahoo.com
@pytest.fixture(scope="function")
def org_admin_account_planet_login(driver, data):
    # loads user, password, otp and passes to login func
    login = LoginPage(driver)
    seed = get_login_data("auto1", "otp_seed")
    yield login.login_on_startup(
        data["rl_url"],
        get_login_data("auto1", "auto1_user"),
        get_login_data("auto1", "auto1_pass"),
        seed
    )


# login to acl planet to test accounts user 11 rebauto2@yahoo.com
@pytest.fixture(scope="function")
def account_editor_planet_login(driver, data):
    # loads user, password, otp and passes to login func
    login = LoginPage(driver)
    seed = get_login_data("auto2", "otp_seed")
    yield login.login_on_startup(
        data["acl_url"],
        get_login_data("auto2", "auto2_user"),
        get_login_data("auto2", "auto2_pass"),
        seed
    )


# login to rate limit planet to test accounts user 100 rebauto3@yahoo.com
@pytest.fixture(scope="function")
def account_reblaze_admin_planet_login(driver, data):
    # loads user, password, otp and passes to login func
    login = LoginPage(driver)
    seed = get_login_data("auto3", "otp_seed")
    yield login.login_on_startup(
        data["rl_url"],
        get_login_data("auto3", "auto3_user"),
        get_login_data("auto3", "auto3_pass"),
        seed
    )
    clean = Cleanup(driver)
    clean.delete_test_accounts()


# login to rate limit reblaze default org to test accounts user 100 rebauto4@yahoo.com
@pytest.fixture(scope="function")
def account_reblaze_admin_reb_default_org_login(driver, data):
    # loads user, password, otp and passes to login func
    login = LoginPage(driver)
    seed = get_login_data("auto4", "otp_seed")
    yield login.login_on_startup(
        data["rl_url"],
        get_login_data("auto4", "auto4_user"),
        get_login_data("auto4", "auto4_pass"),
        seed
    )
    clean = Cleanup(driver)
    clean.delete_test_accounts()


# login to rate limit planet
@pytest.fixture(scope="session")
def rl_login(driver, data):
    # loads user, password, otp and passes to login func
    login = LoginPage(driver)
    seed = get_login_data("rl", "otp_seed")
    yield login.login_on_startup(
        data["rl_url"],
        get_login_data("rl", "rl_user"),
        get_login_data("rl", "rl_pass"),
        seed,
        clean=True
    )
    # clean created rate limit rules after test is finished
    clean = Cleanup(driver)
    clean.delete_all_rate_limit_rules()


# login to view log planet
@pytest.fixture(scope="function")
def vl_login(driver, data):
    # loads user, password, otp and passes to login func
    login = LoginPage(driver)
    seed = get_login_data("vl", "otp_seed")
    yield login.login_on_startup(
        data["vl_url"],
        get_login_data("vl", "vl_user"),
        get_login_data("vl", "vl_pass"),
        seed
    )


# login to tag planet
@pytest.fixture(scope="function")
def tag_login(driver, data):
    # loads user, password, otp and passes to login func
    login = LoginPage(driver)
    seed = get_login_data("tag", "otp_seed")
    yield login.login_on_startup(
        data["tag_url"],
        get_login_data("tag", "tag_user"),
        get_login_data("tag", "tag_pass"),
        seed
    )


# login to ssl planet
@pytest.fixture(scope="function")
def ssl_login(driver, data):
    # loads user, password, otp and passes to login func
    login = LoginPage(driver)
    seed = get_login_data("ssl", "otp_seed")
    yield login.login_on_startup(
        data["ssl_url"],
        get_login_data("ssl", "ssl_user"),
        get_login_data("ssl", "ssl_pass"),
        seed
    )


# @pytest.mark.hookwrapper
# def pytest_runtest_makereport(item):
#     """
#     Extends the PyTest Plugin to take and embed screenshot in html report, whenever test fails.
#     :param item:
#     """
#     pytest_html = item.config.pluginmanager.getplugin('html')
#     outcome = yield
#     report = outcome.get_result()
#     extra = getattr(report, 'extra', [])
#
#     if report.when == 'call' or report.when == "setup":
#         xfail = hasattr(report, 'wasxfail')
#         if (report.skipped and xfail) or (report.failed and not xfail):
#             file_name = report.nodeid.replace("::", "_")+".png"
#             _capture_screenshot(file_name)
#             if file_name:
#                 html = '<div><img src="%s" alt="screenshot" style="width:304px;height:228px;" ' \
#                        'onclick="window.open(this.src)" align="right"/></div>' % file_name
#                 extra.append(pytest_html.extras.html(html))
#         report.extra = extra

@pytest.mark.hookwrapper
def pytest_runtest_makereport(item):
    pytest_html = item.config.pluginmanager.getplugin("html")
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, "extra", [])
    if report.when == "call":
        xfail = hasattr(report, "wasxfail")
        if (report.skipped and xfail) or (report.failed and not xfail):
            feature_request = item.funcargs["request"]
            driver = feature_request.getfixturevalue("driver")
            class_name = os.environ.get("PYTEST_CURRENT_TEST").split(":")[2]
            test_name = os.environ.get("PYTEST_CURRENT_TEST").split(":")[-1].replace(" (call)", "")
            img_name = class_name + "_" + test_name + "_" + str(datetime.datetime.strftime(datetime.datetime.now(), "%d-%m-%y")) + ".png"
            img_path = os.path.join(config.HOME_PATH, "assets", "screenshots", img_name)
            driver.save_screenshot(img_path)
        #     if img_name:
        #         html = '<div><img src="%s" alt="screenshot" style="width:300px;height=200px"' \
        #         'onclick="window.open(this.src)" align="right"/><div>'%img_name
        #     # only add additional html on failure
        #     extra.append(pytest_html.extras.html('<img src="%s" width="300px;height=200px" onclick="window.open(this.src)">' % img_path))
        # report.extra = extra