import pytest
from pages.account_page import *
from pages.login_page import *
from locators.account_locators import *
import time


@pytest.mark.usefixtures("test_setup", "account_login")
@pytest.mark.all_modules
@pytest.mark.account_tests
class TestAccount:

    def test_can_navigate_to_account_page(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        assert "account" in account_page.get_current_url()

    def test_your_account_page_components(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_NAME_DIV)
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_NAME_INPUT)
        assert account_page.find_element(By.TAG_NAME, "p").text == "qaautomation@reblaze.com"
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_PHONE_DIV)
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_PHONE_INPUT)
        btns = account_page.find_elements(By.TAG_NAME, "button")
        assert all([True for a in btns if a.text in "Update Reset Password"])
        assert account_page.find_element(*AccountLocators.SHOW_QR_BTN)
        assert account_page.find_element(*AccountLocators.SHOW_API_KEY_BTN)

    def test_can_open_qr_code_div(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert not account_page.is_element_present(*AccountLocators.HIDE_QR_BTN)
        account_page.find_element(*AccountLocators.SHOW_QR_BTN).click()
        assert account_page.is_element_present(*AccountLocators.HIDE_QR_BTN)
        account_page.find_element(*AccountLocators.HIDE_QR_BTN).click()
        assert not account_page.is_element_present(*AccountLocators.HIDE_QR_BTN)
        assert account_page.is_element_present(*AccountLocators.SHOW_QR_BTN)

    def test_can_open_api_key_div(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert not account_page.is_element_present(*AccountLocators.HIDE_API_KEY_BTN)
        account_page.find_element(*AccountLocators.SHOW_API_KEY_BTN).click()
        assert account_page.is_element_present(*AccountLocators.HIDE_API_KEY_BTN)
        account_page.find_element(*AccountLocators.HIDE_API_KEY_BTN).click()
        assert not account_page.is_element_present(*AccountLocators.HIDE_API_KEY_BTN)
        assert account_page.is_element_present(*AccountLocators.SHOW_API_KEY_BTN)

    def test_user_management_page_components(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert len(account_page.find_elements(*AccountLocators.SINGLE_USER_ROW)) == 2
        assert account_page.check_users_exist_with_action_buttons()
        assert "rebauto two" in account_page.get_user_name_from_users_manager_by_name("rebauto two")

    def test_my_account_data(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert "QA automation" in account_page.get_user_name()
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_ORGANIZATION_INPUT)
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_EMAIL_INPUT)

    def test_cannot_edit_my_account_phone(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_number = account_page.generate_random_phone_number(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_PHONE_INPUT)
        account_page.send_value_to_field(random_number, *AccountLocators.ACCOUNT_PHONE_INPUT)
        assert account_page.check_update_account_enabled() is False

    def test_cannot_save_my_phone_number(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_number = account_page.generate_random_phone_number(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_PHONE_INPUT)
        account_page.send_value_to_field(random_number, *AccountLocators.ACCOUNT_PHONE_INPUT)
        assert account_page.check_update_account_enabled() is False

    def test_cannot_edit_my_user_name(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_name = "QA automation " + account_page.generate_random_string(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.ACCOUNT_NAME_INPUT)
        assert account_page.check_update_account_enabled() is False

    def test_my_email_is_correct(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        email = account_page.get_user_email()
        assert email == "qaautomation@reblaze.com"

    def test_cannot_edit_my_account_name(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_name = "rebauto two " + account_page.generate_random_string(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.ACCOUNT_NAME_INPUT)
        assert account_page.check_update_account_enabled() is False

    def test_user_single_sign_on_config_page_components(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Single sign on configuration")
        assert len(account_page.find_elements(By.TAG_NAME, "input")) == 7
        assert account_page.is_element_present(By.TAG_NAME, "button")

    def test_can_edit_account_phone(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        random_number = account_page.generate_random_phone_number(10)
        account_page.press_row_edit_btn_by_name("rebauto two")
        account_page.clear_field(*AccountLocators.MODAL_PHONE_INPUT)
        account_page.send_value_to_field(random_number, *AccountLocators.MODAL_PHONE_INPUT)
        account_page.save_modal_window()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert all(account_page.check_correct_phone_number_in_users_list("rebauto two", random_number))
        account_page.press_row_edit_btn_by_name("rebauto two")
        assert account_page.get_user_phone_from_modal() == random_number

    def test_can_edit_account_name(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        random_name = "rebauto two " + account_page.generate_random_string(10)
        account_page.press_row_edit_btn_by_name("rebauto two")
        account_page.clear_field(*AccountLocators.MODAL_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.MODAL_NAME_INPUT)
        account_page.save_modal_window()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert all(account_page.check_correct_user_name_in_users_list(random_name))
        account_page.press_row_edit_btn_by_name("rebauto two")
        assert account_page.get_user_name_from_modal() == random_name

    # def test_can_create_new_account(self):
    #     account_page = AccountPage(self.driver)
    #     account_page.navigate_to_account_page()
    #     account_page.switch_to_frame()
    #     account_page.navigate_to_tab("Users management")
    #     account_page.press_create_new_user_btn()


@pytest.mark.usefixtures("test_setup", "org_admin_account_planet_login")
@pytest.mark.all_modules
@pytest.mark.account_tests
class TestAccountPlanet:
    def test_user_management_page_components(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert len(account_page.find_elements(*AccountLocators.SINGLE_USER_ROW)) == 2
        assert not account_page.check_planet_users_exist_with_action_buttons()

    def test_my_account_data(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert "rebauto one" in account_page.get_user_name()
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_PLANET_ORGANIZATION_INPUT)
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_PLANET_EMAIL_INPUT)

    def test_can_edit_account_phone(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_number = account_page.generate_random_phone_number(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_PHONE_INPUT)
        account_page.send_value_to_field(random_number, *AccountLocators.ACCOUNT_PHONE_INPUT)
        account_page.save_existing_account()
        account_page.refresh_page()
        account_page.switch_to_frame()
        assert account_page.get_user_phone() == random_number

    def test_phone_number_changes_in_all_places(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_number = account_page.generate_random_phone_number(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_PHONE_INPUT)
        account_page.send_value_to_field(random_number, *AccountLocators.ACCOUNT_PHONE_INPUT)
        account_page.save_existing_account()
        account_page.refresh_page()
        account_page.switch_to_frame()
        assert account_page.get_user_phone() == random_number
        name = account_page.get_user_name()
        account_page.navigate_to_tab("Users management")
        assert all(account_page.check_correct_phone_number_in_users_list(name, random_number))
        account_page.press_row_edit_btn_by_name(name)
        assert account_page.get_user_phone_from_modal() == random_number

    def test_user_name_changes_in_all_places(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_name = "rebauto one " + account_page.generate_random_string(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.ACCOUNT_NAME_INPUT)
        account_page.save_existing_account()
        account_page.refresh_page()
        account_page.switch_to_frame()
        assert account_page.get_user_name() == random_name
        account_page.navigate_to_tab("Users management")
        assert all(account_page.check_correct_user_name_in_users_list(random_name))
        account_page.press_row_edit_btn_by_name(random_name)
        assert account_page.get_user_name_from_modal() == random_name

    def test_email_is_correct_in_all_places(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        user_name = account_page.get_user_name()
        email = account_page.get_user_email()
        assert email != ''
        account_page.navigate_to_tab("Users management")
        assert all(account_page.check_correct_email_in_users_list(user_name, email))
        account_page.press_row_edit_btn_by_name(user_name)
        assert account_page.get_user_email_from_modal() == email

    def test_access_level_is_correct_in_all_places(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        user_name = account_page.get_user_name()
        account_page.navigate_to_tab("Users management")
        access_level = account_page.get_user_access_level(user_name)
        account_page.press_row_edit_btn_by_name(user_name)
        assert account_page.get_user_access_level_from_modal() == access_level

    def test_can_edit_account_name(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_name = "rebauto one " + account_page.generate_random_string(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.ACCOUNT_NAME_INPUT)
        account_page.save_existing_account()
        account_page.refresh_page()
        account_page.switch_to_frame()
        assert account_page.get_user_name() == random_name

    def test_org_admin_has_only_his_org_in_list(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_create_new_user_btn()
        assert all(account_page.check_organization_not_in_organization_list(["rebauto1"]))
        assert not all(account_page.check_organization_not_in_organization_list(["rebauto2", "rebauto3", "rebauto4"]))

    def test_org_admin_cannot_create_reb_admin(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_create_new_user_btn()
        assert not account_page.check_access_level_not_in_access_levl_list("Reblaze Admin")


@pytest.mark.usefixtures("test_setup", "account_editor_planet_login")
@pytest.mark.all_modules
@pytest.mark.account_tests
class TestAccountEditor:
    def test_user_management_page_components(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert not account_page.is_element_present(By.LINK_TEXT, "Users management")
        assert not account_page.is_element_present(By.LINK_TEXT, "Single sign on configuration")

    def test_your_account_page_components(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_NAME_DIV)
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_NAME_INPUT)
        assert account_page.find_element(By.TAG_NAME, "p").text == "rebauto2@yahoo.com"
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_PHONE_DIV)
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_PHONE_INPUT)
        btns = account_page.find_elements(By.TAG_NAME, "button")
        assert all([True for a in btns if a.text in "Update Reset Password"])
        assert account_page.find_element(*AccountLocators.SHOW_QR_BTN)
        assert not account_page.is_element_present(*AccountLocators.SHOW_API_KEY_BTN)

    def test_my_account_data(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert "rebauto two" in account_page.get_user_name()
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_EDITOR_ORGANIZATION_INPUT)
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_EDITOR_EMAIL_INPUT)

    def test_can_edit_account_phone(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_number = account_page.generate_random_phone_number(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_PHONE_INPUT)
        account_page.send_value_to_field(random_number, *AccountLocators.ACCOUNT_PHONE_INPUT)
        account_page.save_existing_account()
        account_page.refresh_page()
        account_page.switch_to_frame()
        assert account_page.get_user_phone() == random_number

    def test_user_name_changes_in_all_places(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_name = "rebauto two " + account_page.generate_random_string(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.ACCOUNT_NAME_INPUT)
        account_page.save_existing_account()
        account_page.refresh_page()
        account_page.switch_to_frame()
        assert account_page.get_user_name() == random_name

    def test_can_edit_account_name(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_name = "rebauto two " + account_page.generate_random_string(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.ACCOUNT_NAME_INPUT)
        account_page.save_existing_account()
        account_page.refresh_page()
        account_page.switch_to_frame()
        assert account_page.get_user_name() == random_name


@pytest.mark.usefixtures("test_setup", "account_reblaze_admin_planet_login")
@pytest.mark.all_modules
@pytest.mark.account_tests
class TestAccountReblazeAdmin:
    def test_can_navigate_to_account_page(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        assert "account" in account_page.get_current_url()

    def test_your_account_page_components(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_NAME_DIV)
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_NAME_INPUT)
        assert account_page.find_element(By.TAG_NAME, "p").text == "rebauto3@yahoo.com"
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_PHONE_DIV)
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_PHONE_INPUT)
        btns = account_page.find_elements(By.TAG_NAME, "button")
        assert all([True for a in btns if a.text in "Update Reset Password"])
        assert account_page.find_element(*AccountLocators.SHOW_QR_BTN)
        assert account_page.find_element(*AccountLocators.SHOW_API_KEY_BTN)

    def test_can_open_qr_code_div(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert not account_page.is_element_present(*AccountLocators.HIDE_QR_BTN)
        account_page.find_element(*AccountLocators.SHOW_QR_BTN).click()
        assert account_page.is_element_present(*AccountLocators.HIDE_QR_BTN)
        account_page.find_element(*AccountLocators.HIDE_QR_BTN).click()
        assert not account_page.is_element_present(*AccountLocators.HIDE_QR_BTN)
        assert account_page.is_element_present(*AccountLocators.SHOW_QR_BTN)

    def test_can_open_api_key_div(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert not account_page.is_element_present(*AccountLocators.HIDE_API_KEY_BTN)
        account_page.find_element(*AccountLocators.SHOW_API_KEY_BTN).click()
        assert account_page.is_element_present(*AccountLocators.HIDE_API_KEY_BTN)
        account_page.find_element(*AccountLocators.HIDE_API_KEY_BTN).click()
        assert not account_page.is_element_present(*AccountLocators.HIDE_API_KEY_BTN)
        assert account_page.is_element_present(*AccountLocators.SHOW_API_KEY_BTN)

    def test_user_management_page_components(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert len(account_page.find_elements(*AccountLocators.SINGLE_USER_ROW)) == 3
        assert account_page.check_users_exist_with_action_buttons()
        assert "rebauto one" in account_page.get_user_name_from_users_manager_by_name("rebauto one")
        assert "rebauto three" in account_page.get_user_name_from_users_manager_by_name("rebauto three")

    def test_my_account_data(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert "rebauto three" in account_page.get_user_name()
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_ORG_AMIN_ORGANIZATION_INPUT)
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_ORG_ADMIN_EMAIL_INPUT)

    def test_cannot_edit_my_account_phone(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_number = account_page.generate_random_phone_number(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_PHONE_INPUT)
        account_page.send_value_to_field(random_number, *AccountLocators.ACCOUNT_PHONE_INPUT)
        assert account_page.check_update_account_enabled() is False

    def test_cannot_save_my_phone_number(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_number = account_page.generate_random_phone_number(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_PHONE_INPUT)
        account_page.send_value_to_field(random_number, *AccountLocators.ACCOUNT_PHONE_INPUT)
        assert account_page.check_update_account_enabled() is False

    def test_cannot_edit_my_user_name(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_name = "rebauto three " + account_page.generate_random_string(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.ACCOUNT_NAME_INPUT)
        assert account_page.check_update_account_enabled() is False

    def test_my_email_is_correct(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        email = account_page.get_user_email()
        assert email == "rebauto3@yahoo.com"

    def test_cannot_edit_my_account_name(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_name = "rebauto three " + account_page.generate_random_string(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.ACCOUNT_NAME_INPUT)
        assert account_page.check_update_account_enabled() is False

    def test_user_single_sign_on_config_page_components(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Single sign on configuration")
        assert len(account_page.find_elements(By.TAG_NAME, "input")) == 7
        assert account_page.is_element_present(By.TAG_NAME, "button")

    def test_can_edit_other_account_phone(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        random_number = account_page.generate_random_phone_number(10)
        account_page.press_row_edit_btn_by_name("rebauto one")
        account_page.clear_field(*AccountLocators.MODAL_PHONE_INPUT)
        account_page.send_value_to_field(random_number, *AccountLocators.MODAL_PHONE_INPUT)
        account_page.save_modal_window()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert all(account_page.check_correct_phone_number_in_users_list("rebauto one", random_number))
        account_page.press_row_edit_btn_by_name("rebauto one")
        assert account_page.get_user_phone_from_modal() == random_number

    def test_can_edit_other_account_name(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        random_name = "rebauto one " + account_page.generate_random_string(10)
        account_page.press_row_edit_btn_by_name("rebauto one")
        account_page.clear_field(*AccountLocators.MODAL_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.MODAL_NAME_INPUT)
        account_page.save_modal_window()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert all(account_page.check_correct_user_name_in_users_list(random_name))
        account_page.press_row_edit_btn_by_name("rebauto one")
        assert account_page.get_user_name_from_modal() == random_name

    def test_admin_can_edit_editor_access_level(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_row_edit_btn_by_name("rebauto one")
        assert account_page.find_element(*AccountLocators.MODAL_ACCESS_LEVEL_DROPDOWN).is_enabled()

    @pytest.mark.parametrize("access", ["Viewer", "Editor"])
    def test_can_create_new_user(self, access):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_create_new_user_btn()
        name = "TEST" + account_page.generate_random_string(10)
        phone = account_page.generate_random_phone_number(9)
        email = account_page.generate_random_email(7)
        account_page.send_value_to_field(name, *AccountLocators.MODAL_NAME_INPUT)
        account_page.send_value_to_field(phone, *AccountLocators.MODAL_PHONE_INPUT)
        account_page.send_value_to_field(email, *AccountLocators.MODAL_EMAIL_INPUT)
        account_page.select_access_level_by_text(access)
        account_page.select_organization_by_text("rebauto1 org")
        account_page.click_save_new_user_btn()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert account_page.check_correct_user_name_in_users_list(name)
        assert account_page.check_correct_email_in_users_list(name, email)
        assert account_page.check_correct_phone_number_in_users_list(name, phone)
        assert account_page.check_correct_organization_in_users_list(name, "rebauto1 org")
        account_page.press_row_edit_btn_by_name(name)
        assert account_page.get_user_name_from_modal() == name
        assert account_page.get_user_phone_from_modal() == phone
        assert account_page.get_user_email_from_modal() == email
        assert account_page.get_user_access_level_from_modal() == access

    def test_can_edit_new_user_name(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_create_new_user_btn()
        name = "TEST" + account_page.generate_random_string(10)
        phone = account_page.generate_random_phone_number(9)
        email = account_page.generate_random_email(7)
        account_page.send_value_to_field(name, *AccountLocators.MODAL_NAME_INPUT)
        account_page.send_value_to_field(phone, *AccountLocators.MODAL_PHONE_INPUT)
        account_page.send_value_to_field(email, *AccountLocators.MODAL_EMAIL_INPUT)
        account_page.select_access_level_by_text("Viewer")
        account_page.select_organization_by_text("rebauto1 org")
        account_page.click_save_new_user_btn()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        random_name = "TEST" + account_page.generate_random_string(10)
        account_page.press_row_edit_btn_by_name(name)
        account_page.clear_field(*AccountLocators.MODAL_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.MODAL_NAME_INPUT)
        account_page.save_modal_window()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert all(account_page.check_correct_user_name_in_users_list(random_name))
        account_page.press_row_edit_btn_by_name(random_name)
        assert account_page.get_user_name_from_modal() == random_name

    def test_can_edit_new_user_phone(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_create_new_user_btn()
        name = "TEST" + account_page.generate_random_string(10)
        phone = account_page.generate_random_phone_number(9)
        email = account_page.generate_random_email(7)
        account_page.send_value_to_field(name, *AccountLocators.MODAL_NAME_INPUT)
        account_page.send_value_to_field(phone, *AccountLocators.MODAL_PHONE_INPUT)
        account_page.send_value_to_field(email, *AccountLocators.MODAL_EMAIL_INPUT)
        account_page.select_access_level_by_text("Viewer")
        account_page.select_organization_by_text("rebauto1 org")
        account_page.click_save_new_user_btn()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        random_number = account_page.generate_random_phone_number(10)
        account_page.press_row_edit_btn_by_name(name)
        account_page.clear_field(*AccountLocators.MODAL_PHONE_INPUT)
        account_page.send_value_to_field(random_number, *AccountLocators.MODAL_PHONE_INPUT)
        account_page.save_modal_window()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert all(account_page.check_correct_phone_number_in_users_list(name, random_number))
        account_page.press_row_edit_btn_by_name(name)
        assert account_page.get_user_phone_from_modal() == random_number

    def test_can_delete_new_user(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_create_new_user_btn()
        name = "TEST" + account_page.generate_random_string(10)
        phone = account_page.generate_random_phone_number(9)
        email = account_page.generate_random_email(7)
        account_page.send_value_to_field(name, *AccountLocators.MODAL_NAME_INPUT)
        account_page.send_value_to_field(phone, *AccountLocators.MODAL_PHONE_INPUT)
        account_page.send_value_to_field(email, *AccountLocators.MODAL_EMAIL_INPUT)
        account_page.select_access_level_by_text("Viewer")
        account_page.select_organization_by_text("rebauto1 org")
        account_page.click_save_new_user_btn()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_row_delete_btn_by_name(name)
        account_page.confirm_user_delete()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert account_page.verify_row_exists_by_name(name) is False


@pytest.mark.usefixtures("test_setup", "account_reblaze_admin_reb_default_org_login")
@pytest.mark.all_modules
@pytest.mark.account_tests
class TestAccountReblazeAdminDefaultOrg:
    def test_can_navigate_to_account_page(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        assert "account" in account_page.get_current_url()

    def test_your_account_page_components(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_NAME_DIV)
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_NAME_INPUT)
        assert account_page.find_element(By.TAG_NAME, "p").text == "rebauto4@yahoo.com"
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_PHONE_DIV)
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_PHONE_INPUT)
        btns = account_page.find_elements(By.TAG_NAME, "button")
        assert all([True for a in btns if a.text in "Update Reset Password"])
        assert account_page.find_element(*AccountLocators.SHOW_QR_BTN)
        assert account_page.find_element(*AccountLocators.SHOW_API_KEY_BTN)

    def test_can_open_qr_code_div(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert not account_page.is_element_present(*AccountLocators.HIDE_QR_BTN)
        account_page.find_element(*AccountLocators.SHOW_QR_BTN).click()
        assert account_page.is_element_present(*AccountLocators.HIDE_QR_BTN)
        account_page.find_element(*AccountLocators.HIDE_QR_BTN).click()
        assert not account_page.is_element_present(*AccountLocators.HIDE_QR_BTN)
        assert account_page.is_element_present(*AccountLocators.SHOW_QR_BTN)

    def test_can_open_api_key_div(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert not account_page.is_element_present(*AccountLocators.HIDE_API_KEY_BTN)
        account_page.find_element(*AccountLocators.SHOW_API_KEY_BTN).click()
        assert account_page.is_element_present(*AccountLocators.HIDE_API_KEY_BTN)
        account_page.find_element(*AccountLocators.HIDE_API_KEY_BTN).click()
        assert not account_page.is_element_present(*AccountLocators.HIDE_API_KEY_BTN)
        assert account_page.is_element_present(*AccountLocators.SHOW_API_KEY_BTN)

    def test_user_management_page_components(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert len(account_page.find_elements(*AccountLocators.SINGLE_USER_ROW)) == 3
        assert account_page.check_users_exist_with_action_buttons()
        assert "rebauto one" in account_page.get_user_name_from_users_manager_by_name("rebauto one")
        assert "rebauto three" in account_page.get_user_name_from_users_manager_by_name("rebauto three")

    def test_my_account_data(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        assert "rebauto four" in account_page.get_user_name()
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_ADMIN_ORGANIZATION_INPUT)
        assert account_page.is_element_present(*AccountLocators.ACCOUNT_REB_ADMIN_EMAIL_INPUT)

    def test_cannot_edit_my_account_phone(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_number = account_page.generate_random_phone_number(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_PHONE_INPUT)
        account_page.send_value_to_field(random_number, *AccountLocators.ACCOUNT_PHONE_INPUT)
        assert account_page.check_update_account_enabled() is False

    def test_cannot_save_my_phone_number(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_number = account_page.generate_random_phone_number(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_PHONE_INPUT)
        account_page.send_value_to_field(random_number, *AccountLocators.ACCOUNT_PHONE_INPUT)
        assert account_page.check_update_account_enabled() is False

    def test_cannot_edit_my_user_name(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_name = "rebauto four " + account_page.generate_random_string(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.ACCOUNT_NAME_INPUT)
        assert account_page.check_update_account_enabled() is False

    def test_my_email_is_correct(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        email = account_page.get_user_email()
        assert email == "rebauto4@yahoo.com"

    def test_cannot_edit_my_account_name(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        random_name = "rebauto four " + account_page.generate_random_string(10)
        account_page.clear_field(*AccountLocators.ACCOUNT_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.ACCOUNT_NAME_INPUT)
        assert account_page.check_update_account_enabled() is False

    def test_user_single_sign_on_config_page_components(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Single sign on configuration")
        assert len(account_page.find_elements(By.TAG_NAME, "input")) == 7
        assert account_page.is_element_present(By.TAG_NAME, "button")

    def test_can_edit_other_account_phone(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        random_number = account_page.generate_random_phone_number(10)
        account_page.press_row_edit_btn_by_name("rebauto one")
        account_page.clear_field(*AccountLocators.MODAL_PHONE_INPUT)
        account_page.send_value_to_field(random_number, *AccountLocators.MODAL_PHONE_INPUT)
        account_page.save_modal_window()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert all(account_page.check_correct_phone_number_in_users_list("rebauto one", random_number))
        account_page.press_row_edit_btn_by_name("rebauto one")
        assert account_page.get_user_phone_from_modal() == random_number

    def test_can_edit_other_account_name(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        random_name = "rebauto one " + account_page.generate_random_string(10)
        account_page.press_row_edit_btn_by_name("rebauto one")
        account_page.clear_field(*AccountLocators.MODAL_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.MODAL_NAME_INPUT)
        account_page.save_modal_window()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert all(account_page.check_correct_user_name_in_users_list(random_name))
        account_page.press_row_edit_btn_by_name("rebauto one")
        assert account_page.get_user_name_from_modal() == random_name

    def test_admin_can_edit_editor_access_level(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_row_edit_btn_by_name("rebauto one")
        assert account_page.find_element(*AccountLocators.MODAL_ACCESS_LEVEL_DROPDOWN).is_enabled()

    @pytest.mark.parametrize("access", ["Viewer", "Editor"])
    def test_can_create_new_user(self, access):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_create_new_user_btn()
        name = "TEST" + account_page.generate_random_string(10)
        phone = account_page.generate_random_phone_number(9)
        email = account_page.generate_random_email(7)
        account_page.send_value_to_field(name, *AccountLocators.MODAL_NAME_INPUT)
        account_page.send_value_to_field(phone, *AccountLocators.MODAL_PHONE_INPUT)
        account_page.send_value_to_field(email, *AccountLocators.MODAL_EMAIL_INPUT)
        account_page.select_access_level_by_text(access)
        account_page.select_organization_by_text("rebauto1 org")
        account_page.click_save_new_user_btn()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert account_page.check_correct_user_name_in_users_list(name)
        assert account_page.check_correct_email_in_users_list(name, email)
        assert account_page.check_correct_phone_number_in_users_list(name, phone)
        assert account_page.check_correct_organization_in_users_list(name, "rebauto1 org")
        account_page.press_row_edit_btn_by_name(name)
        assert account_page.get_user_name_from_modal() == name
        assert account_page.get_user_phone_from_modal() == phone
        assert account_page.get_user_email_from_modal() == email
        assert account_page.get_user_access_level_from_modal() == access

    def test_can_edit_new_user_name(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_create_new_user_btn()
        name = "TEST" + account_page.generate_random_string(10)
        phone = account_page.generate_random_phone_number(9)
        email = account_page.generate_random_email(7)
        account_page.send_value_to_field(name, *AccountLocators.MODAL_NAME_INPUT)
        account_page.send_value_to_field(phone, *AccountLocators.MODAL_PHONE_INPUT)
        account_page.send_value_to_field(email, *AccountLocators.MODAL_EMAIL_INPUT)
        account_page.select_access_level_by_text("Viewer")
        account_page.select_organization_by_text("rebauto1 org")
        account_page.click_save_new_user_btn()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        random_name = "TEST" + account_page.generate_random_string(10)
        account_page.press_row_edit_btn_by_name(name)
        account_page.clear_field(*AccountLocators.MODAL_NAME_INPUT)
        account_page.send_value_to_field(random_name, *AccountLocators.MODAL_NAME_INPUT)
        account_page.save_modal_window()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert all(account_page.check_correct_user_name_in_users_list(random_name))
        account_page.press_row_edit_btn_by_name(random_name)
        assert account_page.get_user_name_from_modal() == random_name

    def test_can_edit_new_user_phone(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_create_new_user_btn()
        name = "TEST" + account_page.generate_random_string(10)
        phone = account_page.generate_random_phone_number(9)
        email = account_page.generate_random_email(7)
        account_page.send_value_to_field(name, *AccountLocators.MODAL_NAME_INPUT)
        account_page.send_value_to_field(phone, *AccountLocators.MODAL_PHONE_INPUT)
        account_page.send_value_to_field(email, *AccountLocators.MODAL_EMAIL_INPUT)
        account_page.select_access_level_by_text("Viewer")
        account_page.select_organization_by_text("rebauto1 org")
        account_page.click_save_new_user_btn()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        random_number = account_page.generate_random_phone_number(10)
        account_page.press_row_edit_btn_by_name(name)
        account_page.clear_field(*AccountLocators.MODAL_PHONE_INPUT)
        account_page.send_value_to_field(random_number, *AccountLocators.MODAL_PHONE_INPUT)
        account_page.save_modal_window()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert all(account_page.check_correct_phone_number_in_users_list(name, random_number))
        account_page.press_row_edit_btn_by_name(name)
        assert account_page.get_user_phone_from_modal() == random_number

    def test_can_delete_new_user(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_create_new_user_btn()
        name = "TEST" + account_page.generate_random_string(10)
        phone = account_page.generate_random_phone_number(9)
        email = account_page.generate_random_email(7)
        account_page.send_value_to_field(name, *AccountLocators.MODAL_NAME_INPUT)
        account_page.send_value_to_field(phone, *AccountLocators.MODAL_PHONE_INPUT)
        account_page.send_value_to_field(email, *AccountLocators.MODAL_EMAIL_INPUT)
        account_page.select_access_level_by_text("Viewer")
        account_page.select_organization_by_text("rebauto1 org")
        account_page.click_save_new_user_btn()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_row_delete_btn_by_name(name)
        account_page.confirm_user_delete()
        account_page.refresh_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert account_page.verify_row_exists_by_name(name) is False

    def test_reb_admin_has_only_his_org_in_list(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_create_new_user_btn()
        assert all(account_page.check_organization_not_in_organization_list(["rebauto1"]))
        assert not all(account_page.check_organization_not_in_organization_list(["rebauto2", "rebauto3", "rebauto4"]))

    def test_reb_admin_cannot_create_reb_admin(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        account_page.press_create_new_user_btn()
        assert not account_page.check_access_level_not_in_access_levl_list("Reblaze Admin")

    def test_reb_admin_does_not_see_his_user_in_manager(self):
        account_page = AccountPage(self.driver)
        account_page.navigate_to_account_page()
        account_page.switch_to_frame()
        account_page.navigate_to_tab("Users management")
        assert account_page.get_user_data_by_name("rebauto four") is None


@pytest.mark.usefixtures("test_setup")
@pytest.mark.all_modules
@pytest.mark.account_tests
class TestAccountNegative:
    def test_cannot_login_to_not_authorized_planet(self):
        login = LoginPage(self.driver)
        login.login_unauth_user()
        assert login.is_element_present(*DashboardLocators.LOGIN_ERROR_MSG)
        assert login.find_element(*DashboardLocators.LOGIN_ERROR_MSG).text == "Authentication Error"


