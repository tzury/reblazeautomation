import time

from core.base_page import *
from locators.rate_limit_locators import *
from locators.view_log_locators import *


attr_values = ["Application Name", "Is Human", "Is Cloud", "Is Tor", "Is Vpn", "Is Anonymizer",
               "Is Proxy", "Is Mobile", "Content Type", "Country Name", "Organization",
               "Protocol", "Remote Address", "Request", "Request Length", "Method", "Request URI",
               "Scheme", "URI", "Tag"]


class RateLimitPage(BasePage):


    # @staticmethod
    # def return_rate_limit_rules_dict():
    #     clean = Cleanup()
    #     return clean.call("XGET", "rl", suffix="/ratelimit/list")

    def extract_required_rule(self, name=None, rule_id=None) -> dict:
        rules = self.return_rate_limit_rules_dict()
        for rule in rules:
            if rule["name"] == name or rule["id"] == rule_id:
                return rule

    def click_save_rule_btn(self):
        self.explicitly_wait_for_element_to_be_clickable(10, *RateLimitFormLocators.SAVE_FORM_BTN)
        self.find_element(*RateLimitFormLocators.SAVE_FORM_BTN).click()
        time.sleep(0.5)

    def open_new_rule_form(self):
        self.switch_to_default_content()
        self.navigate_to_tab("Rate Limiting")
        self.switch_to_frame()
        self.explicitly_wait_for_element(5, *RateLimitMainPageLocators.NEW_RULE_BTN)
        self.find_element(*RateLimitMainPageLocators.NEW_RULE_BTN).click()
        self.explicitly_wait_for_element(5, *RateLimitFormLocators.NEW_RATE_LIMIT_LABEL)
        self.switch_to_frame()

    def click_on_key(self, key_name: str):
        self.find_element(*RateLimitFormLocators.KEY_DROPDOWN).click()
        time.sleep(0.4)
        keys = self.find_elements(By.XPATH, "//option")
        for key in keys:
            if key.get_attribute("value") == key_name:
                key.click()
                time.sleep(0.3)
                break

    def click_on_action(self, action_name: str):
        self.find_element(*RateLimitFormLocators.ACTION_DROPDOWN).click()
        # time.sleep(0.5)
        keys = self.find_elements(By.XPATH, "//option")
        for key in keys:
            if key.get_attribute("value") == action_name:
                key.click()
                time.sleep(0.3)
                break

    def click_on_ban_action(self, ban_action_name: str):
        selects = self.find_elements(*RateLimitFormLocators.ACTION_DROPDOWN)
        selects[1].click()
        # time.sleep(0.5)
        keys = self.find_elements(By.XPATH, f"//option[@value='{ban_action_name}']")
        keys[1].click()
        # for key in keys:
        #     if key.get_attribute("value") == ban_action_name:
        #         key.click()
        #         time.sleep(0.5)
        #         break

    def click_on_include_exclude_option(self, option_name: str):
        selects = self.find_elements(*RateLimitFormLocators.ACTION_DROPDOWN)
        selects[1].click()
        elems = self.find_elements(By.XPATH, f"//option[contains(text(), '{option_name}')]")
        elems[1].click()
        time.sleep(0.3)

    def check_selecting_key_changes_field(self):
        keys = {
            "cookies": "Cookie\'s name",
            "headers": "Header\'s name",
            "args": "Argument\'s name",
            "attrs": "Attribute\'s name"
        }
        results = []
        for k, v in keys.items():
            self.click_on_key(k)
            time.sleep(0.3)
            if k != "attrs":
                results.append(self.find_element(*RateLimitFormLocators.KEY_FIELD).get_attribute("placeholder") == v)
            else:
                results.append(
                    self.find_element(*RateLimitFormLocators.ATTRS_KEY_FIELD).get_attribute("placeholder") == v)
        return results

    def click_attribute_value(self, value_name: str):
        values = self.find_elements(*RateLimitFormLocators.ATTRIBUTES_KEY_DROPDOWN_VALUES)
        for value in values:
            self.clear_field(*RateLimitFormLocators.ATTRS_KEY_FIELD)
            if value.get_attribute("value") == value_name:
                self.send_value_to_field(value_name, *RateLimitFormLocators.ATTRS_KEY_FIELD)
                break

    def check_all_attribute_values_exist(self):
        values = self.find_elements(*RateLimitFormLocators.ATTRIBUTES_KEY_DROPDOWN_VALUES)
        val_text = [val.get_attribute("value") for val in values]
        return sorted(val_text) == sorted(attr_values)

    def attribute_values_clickable(self):
        results = []
        for val in attr_values:
            self.click_on_key("attrs")
            time.sleep(0.3)
            self.click_attribute_value(val)
            results.append(
                self.find_element(*RateLimitFormLocators.ATTRIBUTES_KEY_DROPDOWN_VALUES).get_attribute("value"))
            break
        return results

    def fill_basic_rate_limit_rule(self, name, desc, limit, ttl, key_type, key_value, action_type=''):
        self.clear_field(*RateLimitFormLocators.RULE_NAME_FIELD_EMPTY)
        self.send_value_to_field(name, *RateLimitFormLocators.RULE_NAME_FIELD_EMPTY)
        self.send_value_to_field(desc, *RateLimitFormLocators.RULE_DESCRIPTION_FIELD)
        self.send_value_to_field(limit, *RateLimitFormLocators.RULE_LIMIT_FIELD)
        self.send_value_to_field(ttl, *RateLimitFormLocators.RULE_TIMEFRAME_FIELD)
        self.click_on_key(key_type)
        if key_type == "attrs":
            self.send_value_to_field(key_value, *RateLimitFormLocators.ATTRS_KEY_FIELD)
        else:
            self.send_value_to_field(key_value, *RateLimitFormLocators.KEY_FIELD)
        if action_type != '':
            self.click_on_action(action_type)

    def fill_exclude_key_value(self, key, value):
        self.send_value_to_field(key, *RateLimitFormLocators.EXCLUDE_KEY_INPUT)
        self.send_value_to_field(value, *RateLimitFormLocators.EXCLUDE_VALUE_INPUT)

    def fill_include_key_value(self, key, value):
        self.send_value_to_field(key, *RateLimitFormLocators.INCLUDE_KEY_INPUT)
        self.send_value_to_field(value, *RateLimitFormLocators.INCLUDE_VALUE_INPUT)

    def attach_rule_to_url_by_path_and_rule_name(self, site_name, path_name, rule_name, description=""):
        self.driver.switch_to.default_content()
        self.navigate_to_tab("Planet Overview")
        self.switch_to_frame()
        self.click_on_site_name_by_name(site_name)
        self.switch_to_frame()
        self.expand_path_settings_by_name(path_name)
        self.find_element(*PlanetOverviewLocators.ATTACH_NEW_RULE_TO_URL_BTN).click()
        time.sleep(0.3)
        self.find_element(*PlanetOverviewLocators.RATE_LIMIT_RULES_DROPDOWN).click()
        self.find_element(By.LINK_TEXT, rule_name + " " + description).click()
        self.find_element(*PlanetOverviewLocators.ADD_BTN).click()
        self.find_element(*PlanetOverviewLocators.SAVE_BTN).click()

    def attach_rule_to_site(self, site_name, rule_name, description=""):
        self.driver.switch_to.default_content()
        self.navigate_to_tab("Planet Overview")
        self.switch_to_frame()
        self.click_on_site_name_by_name(site_name)
        self.switch_to_frame()
        self.find_element(*PlanetOverviewLocators.ATTACH_NEW_RULE_TO_SITE_BTN).click()
        self.find_element(*PlanetOverviewLocators.RATE_LIMIT_RULES_DROPDOWN).click()
        time.sleep(0.1)
        self.find_element(By.LINK_TEXT, rule_name + " " + description).click()
        self.find_element(*PlanetOverviewLocators.ADD_BTN).click()
        self.find_element(*PlanetOverviewLocators.SAVE_BTN).click()

    def check_rate_limit_rule_data(self, desc, limit, ttl):
        data = self.find_elements(*PlanetOverviewLocators.PATH_NAME)
        results = []
        for d in data:
            if d.text == desc:
                results.append(True)
            if d.text == str(limit):
                results.append(True)
            if d.text == str(ttl):
                results.append(True)
        return results

    def check_rule_attached_to_url(self, rule_name, limit, ttl, description=''):
        rule = self.extract_required_rule(rule_name)
        if self.is_element_present(By.LINK_TEXT, rule_name):
            if rule["id"] in self.find_element(By.LINK_TEXT, rule_name).get_attribute("href"):
                results = self.check_rate_limit_rule_data(description, limit, ttl)
                if all(results) and len(results) >= 3:
                    return True
        return False

    def open_path_attached_rules_list(self, site_name, path_name):
        self.driver.switch_to.default_content()
        self.navigate_to_tab("Planet Overview")
        self.switch_to_frame()
        self.click_on_site_name_by_name(site_name)
        self.switch_to_frame()
        self.expand_path_settings_by_name(path_name)

    def open_site_attached_rules_list(self, site_name):
        self.driver.switch_to.default_content()
        self.navigate_to_tab("Planet Overview")
        self.switch_to_frame()
        self.click_on_site_name_by_name(site_name)
        self.switch_to_frame()

    def check_rate_limit_action_503(self, url, limit, ttl, path=''):
        for i in range(limit):
            response = self.query(f"{url}{path}")
            assert response.status_code == 200
        response = self.query(f"{url}{path}")
        assert response.status_code == 503
        time.sleep(ttl)
        response = self.query(f"{url}{path}")
        assert response.status_code == 200

    def check_rate_limits_action_503_with_params(self, url, limit, ttl, params, path=''):
        for i in range(limit):
            response = self.query(f"{url}{path}", **params[i])
            assert response.status_code == 200
        response = self.query(f"{url}{path}", **params[limit])
        assert response.status_code == 503
        time.sleep(ttl)
        response = self.query(f"{url}{path}", **params[limit + 1])
        assert response.status_code == 200

    def check_rate_limits_action_503_with_exclude(self, url, limit, ttl, path=''):
        for i in range(limit):
            response = self.query(f"{url}{path}")
            assert response.status_code == 200
        response = self.query(f"{url}{path}")
        assert response.status_code == 200
        time.sleep(ttl)
        response = self.query(f"{url}{path}")
        assert response.status_code == 200

    def check_rate_limits_action_503_with_params_with_exclude(self, url, limit, ttl, params, path=''):
        for i in range(limit):
            response = self.query(f"{url}{path}", **params[i])
            assert response.status_code == 200
        response = self.query(f"{url}{path}", **params[limit])
        assert response.status_code == 200
        time.sleep(ttl)
        response = self.query(f"{url}{path}", **params[limit + 1])
        assert response.status_code == 200

    def check_rate_limit_challenge_action(self, url, limit, ttl, path=''):
        for i in range(limit):
            response = self.query(f"{url}{path}")
            assert response.status_code == 200
            assert not self.verify_pattern_in_html(response.content, "<html><head><meta")
        response = self.query(f"{url}{path}")
        assert response.status_code == 200
        assert self.verify_pattern_in_html(response.content, "<html><head><meta")
        time.sleep(ttl)
        response = self.query(f"{url}{path}")
        assert response.status_code == 200
        assert not self.verify_pattern_in_html(response.content, "<html><head><meta")

    def check_rate_limit_challenge_action_with_params(self, url, limit, ttl, params, path=''):
        for i in range(limit):
            response = self.query(f"{url}{path}", **params[i])
            assert response.status_code == 200
            assert not self.verify_pattern_in_html(response.content, "<html><head><meta")
        response = self.query(f"{url}{path}", **params[limit])
        assert response.status_code == 200
        assert self.verify_pattern_in_html(response.content, "<html><head><meta")
        time.sleep(ttl)
        response = self.query(f"{url}{path}", **params[limit + 1])
        assert response.status_code == 200
        assert not self.verify_pattern_in_html(response.content, "<html><head><meta")

    def find_logs_results_rows_check_text_inside(self, rule_name):
        rows = self.find_elements(*ViewLogLocators.LOG_SINGLE_LINE)
        results = []
        for r in rows:
            results.append(rule_name in r.text)
            r.click()
            time.sleep(1)
            results.append(rule_name in r.text)
            break
        return results

    def check_rate_limit_tag_action(self, url, limit, ttl, rule_name, site_name, path=''):
        for i in range(limit):
            response = self.query(f"{url}{path}")
            assert response.status_code == 200
            time.sleep(22)
            self.select_logs_site_by_name(site_name)
            assert all(self.find_logs_results_rows_check_text_inside(rule_name)) is False
        response = self.query(f"{url}{path}")
        assert response.status_code == 200
        time.sleep(22)
        self.select_logs_site_by_name(site_name)
        logs = str(self.find_logs_results_rows_check_text_inside(rule_name))
        assert logs == "[False, True]"
        time.sleep(ttl - 39)
        response = self.query(f"{url}{path}")
        assert response.status_code == 200
        time.sleep(22)
        self.select_logs_site_by_name(site_name)
        assert all(self.find_logs_results_rows_check_text_inside(rule_name)) is False

    def check_rate_limit_tag_action_with_params(self, url, limit, ttl, rule_name, site_name, params, path=''):
        for i in range(limit):
            response = self.query(f"{url}{path}", **params[i])
            assert response.status_code == 200
            time.sleep(22)
            self.select_logs_site_by_name(site_name)
            assert all(self.find_logs_results_rows_check_text_inside(rule_name)) is False
        response = self.query(f"{url}{path}", **params[limit])
        assert response.status_code == 200
        time.sleep(22)
        self.select_logs_site_by_name(site_name)
        results = str(self.find_logs_results_rows_check_text_inside(rule_name))
        assert results == "[False, True]"
        time.sleep(ttl - 39)
        response = self.query(f"{url}{path}", **params[limit + 1])
        assert response.status_code == 200
        time.sleep(22)
        self.select_logs_site_by_name(site_name)
        assert all(self.find_logs_results_rows_check_text_inside(rule_name)) is False

    def check_rate_limit_redirect_action(self, url, status, limit, ttl, location, path=''):
        for i in range(limit):
            response = self.query(f"{url}{path}")
            assert response.status_code == 200
            assert location not in response.url
            code = response.history
            assert str(code) != "[<Response [301]>]"
        response = self.query(f"{url}{path}")
        time.sleep(0.5)
        code = response.history
        assert str(code) == "[<Response [301]>]"
        assert response.status_code == int(status)
        assert response.url == location
        time.sleep(ttl)
        response = self.query(f"{url}{path}")
        assert response.status_code == 200
        assert location not in response.url
        code = response.history
        assert str(code) != "[<Response [301]>]"

    def check_rate_limit_redirect_action_with_params(self, url, status, limit, ttl, location, params, path=''):
        for i in range(limit):
            response = self.query(f"{url}{path}", **params[i])
            assert response.status_code == 200
            assert location not in response.url
            code = response.history
            assert str(code) != "[<Response [301]>]"
        response = self.query(f"{url}{path}", **params[limit])
        time.sleep(0.5)
        code = response.history
        assert str(code) == "[<Response [301]>]"
        assert response.status_code == int(status)
        assert response.url == location
        time.sleep(ttl)
        response = self.query(f"{url}{path}", **params[limit + 1])
        assert response.status_code == 200
        assert location not in response.url
        code = response.history
        assert str(code) != "[<Response [301]>]"

    def check_rate_limit_action_request_header(self, url, limit, ttl, req_key, req_val, path=''):
        for i in range(limit):
            response = self.query(f"{url}{path}")
            assert response.status_code == 200
            assert req_key.capitalize() not in response.text
            assert req_val not in response.text
        response = self.query(f"{url}{path}")
        assert response.status_code == 200
        assert req_key.capitalize() in response.text
        assert req_val in response.text
        time.sleep(ttl)
        response = self.query(f"{url}{path}")
        assert response.status_code == 200
        assert req_key.capitalize() not in response.text
        assert req_val not in response.text

    def check_rate_limit_action_request_header_with_params(self, url, limit, ttl, req_key, req_val, params, path=''):
        for i in range(limit):
            response = self.query(f"{url}{path}", **params[i])
            assert response.status_code == 200
            assert req_key.capitalize() not in response.text
            assert req_val not in response.text
        response = self.query(f"{url}{path}", **params[limit])
        assert response.status_code == 200
        assert req_key.capitalize() in response.text
        assert req_val in response.text
        time.sleep(ttl)
        response = self.query(f"{url}{path}", **params[limit + 1])
        assert response.status_code == 200
        assert req_key.capitalize() not in response.text
        assert req_val not in response.text

    def fill_response_action_fields(self, rand_key, rand_val, rand_response):
        self.send_value_to_field(rand_key, *RateLimitFormLocators.ACTION_HEADER_KEY)
        self.send_value_to_field(rand_val, *RateLimitFormLocators.ACTION_HEADER_VAL)
        self.send_value_to_field("247", *RateLimitFormLocators.RESPONSE_STATUS)
        self.send_value_to_field(rand_response, *RateLimitFormLocators.ACTION_RESPONSE_CONTENT)

    def check_rate_limit_action_response(self, url, limit, ttl, req_key, req_val, res, rand_res, path=''):
        for i in range(limit):
            response = self.query(f"{url}{path}")
            assert response.status_code == 200
            assert rand_res not in response.text
        response = self.query(f"{url}{path}")
        assert response.status_code == int(res)
        assert response.headers[req_key] == req_val
        assert rand_res in response.text
        time.sleep(ttl)
        response = self.query(f"{url}{path}")
        assert response.status_code == 200
        assert rand_res not in response.text

    def check_rate_limit_action_response_with_params(self, url, limit, ttl, req_key, req_val, res, rand_res, params,
                                                     path=''):
        for i in range(limit):
            response = self.query(f"{url}{path}", **params[i])
            assert response.status_code == 200
            assert rand_res not in response.text
        response = self.query(f"{url}{path}", **params[limit])
        assert response.status_code == int(res)
        assert response.headers[req_key] == req_val
        assert rand_res in response.text
        time.sleep(ttl)
        response = self.query(f"{url}{path}", **params[limit + 1])
        assert response.status_code == 200
        assert rand_res not in response.text

    def click_add_exclude_rule_btn(self):
        elems = self.find_elements(*RateLimitFormLocators.ADD_NEW_KEY_BUTTON)
        for i in range(len(elems)):
            if i == 2:
                elems[i].click()
                time.sleep(0.2)
                break

    def click_add_include_rule_btn(self):
        elems = self.find_elements(*RateLimitFormLocators.ADD_NEW_KEY_BUTTON)
        for i in range(len(elems)):
            if i == 1:
                elems[i].click()
                time.sleep(0.2)
                break

