import os
from core import config
from core.config import get_config
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager


def get_driver(requested_browser):
    browsers_dict = {
        "chrome": __get_local_chrome,
        "chrome_linux": __get_local_chrome_linux,
        "firefox": __get_local_firefox,
        "safari": __get_local_safari,
        "chrome_instance": __get_chrome_instance
    }
    try:
        return browsers_dict[requested_browser]()
    except Exception as e:
        print("Browser not supported, could not load driver: " + str(e))


# takes chromedriver from folder
def __get_local_chrome():
    options = webdriver.ChromeOptions()
    # options.add_argument("--start-maximized")
    options.add_argument('--no-sandbox')
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--start-fullscreen")

    # options.add_argument("--window-size=1920x1080")
    # options.add_argument("--start-maximized")


    options.add_argument('--allow-running-insecure-content')
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--ignore-ssl-errors')
    chrome_driver = webdriver.Chrome(os.path.join(config.HOME_PATH, 'drivers/chrome', 'chromedriver'), options=options)
    return chrome_driver


# takes chromedriver from folder in instance
def __get_chrome_instance():
    print("I am inside CORE module")
    options = webdriver.ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--window-size=1440x900")
    options.add_argument("--start-maximized")
    options.add_argument("--headless")
    options.add_argument("--disable-gpu")
    options.add_argument("--allow-insecure-localhost")
    # options.add_argument("--start-fullscreen")
    options.add_argument('--allow-running-insecure-content')
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--ignore-ssl-errors')
    chrome_driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver", options=options)
    return chrome_driver


# installs chromedriver on startup
def __get_local_chrome_linux():
    options = webdriver.ChromeOptions()
    options.add_argument('--no-sandbox')
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--window-size=1920x1080")
    options.add_argument("--start-maximized")
    options.add_argument("--headless")
    options.add_argument("--disable-gpu")
    options.add_argument("--allow-insecure-localhost")
    # options.add_argument("--start-fullscreen")
    options.add_argument('--allow-running-insecure-content')
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--ignore-ssl-errors')
    # chrome_driver = webdriver.Chrome(config.HOME_PATH + sep + 'drivers' + sep + 'linux_driver' + sep + 'chromedriver', options=options)
    chrome_driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
    return chrome_driver

# def __get_local_firefox():
#     profile = webdriver.FirefoxProfile()
#     profile.accept_untrusted_certs = True
#     firefox_driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
#     return firefox_driver


# takes geckodriver from folder
def __get_local_firefox():
    options = webdriver.FirefoxOptions()
    # options.add_argument('--allow-running-insecure-content')
    # options.add_argument('--ignore-certificate-errors')
    options.add_argument('--ignore-ssl-errors')
    # options.add_argument("--start-maximized")
    # options.add_argument('--headless')
    # options.add_argument("--window-size=1920x1080")
    # options.add_argument('--no-sandbox')
    # options.add_argument('--disable-dev-shm-usage')
    firefox_driver = webdriver.Firefox(os.path.join(config.HOME_PATH, 'drivers', "gecko"), options=options)
    return firefox_driver


# loads safari driver from system
def __get_local_safari():
    safari_driver = webdriver.Safari()
    return safari_driver

