import pytest
from pages.view_log_page import *
from locators.view_log_locators import *
import time


@pytest.mark.usefixtures("test_setup", "vl_login")
@pytest.mark.all_modules
@pytest.mark.view_log_tests
class TestViewLog:
    def test_can_navigate_to_view_log_page(self):
        vl_page = ViewLogPage(self.driver)
        vl_page.navigate_to_tab("View Log")
        assert "zoomlog" in vl_page.get_current_url()

    def test_view_log_page_components(self):
        vl_page = ViewLogPage(self.driver)
        vl_page.navigate_to_view_log_page()
        assert vl_page.is_element_present(*ViewLogLocators.DOMAIN_NAME_INPUT)
        assert vl_page.is_element_present(*ViewLogLocators.SEARCH_BTN)
        assert vl_page.is_element_present(*ViewLogLocators.CALENDAR_BTN)
        assert vl_page.is_element_present(*ViewLogLocators.CLEAR_BTN)
        assert vl_page.is_element_present(*ViewLogLocators.EXPORT_CDN_BTN)
        assert vl_page.is_element_present(*ViewLogLocators.HELP_BTN)
        assert vl_page.is_element_present(*ViewLogLocators.HISTORY_BTN)

    def test_can_switch_site(self):
        vl_page = ViewLogPage(self.driver)
        vl_page.navigate_to_view_log_page()
        vl_page.select_logs_site_by_name("vl.test")
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == \
               "domain_name: vl.test"

    def test_correct_data_in_closed_log_row(self):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        ip = vl_page.get_my_public_ip()
        vl_page.query(self.data["vl_base_url"], f"/{rand_path}")
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        assert vl_page.check_log_row_contains_correct_details(rand_path, ip=ip)
        assert vl_page.check_log_row_contains_correct_details(rand_path, url=self.data["vl_base_url"] + f"/{rand_path}")
        assert vl_page.check_log_row_contains_correct_details(rand_path, date="date")
        assert vl_page.check_log_row_contains_correct_details(rand_path, method="method")
        assert vl_page.check_log_row_contains_correct_details(rand_path, response="200")

    def test_log_body_panel_contains_correct_path(self):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        vl_page.query(self.data["vl_base_url"], f"/{rand_path}")
        time.sleep(22)
        vl_page.select_logs_site_by_name("vl.test")
        assert vl_page.find_element(*ViewLogLocators.PANEL_BODY).text == ""
        vl_page.click_on_log_row_by_path(rand_path)
        assert rand_path in vl_page.find_element(*ViewLogLocators.PANEL_BODY).text

    def test_log_body_footer_contains_correct_data(self):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        ip = vl_page.get_my_public_ip()
        vl_page.query(self.data["vl_base_url"], f"/{rand_path}")
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        country_name, country_code = vl_page.get_my_country_name_and_code()
        current_asn = vl_page.get_my_asn()
        if "Google" in current_asn:
            current_asn = current_asn.upper().replace("LLC", "").rstrip(" ")
        assert vl_page.find_element(*ViewLogLocators.PANEL_FOOTER).text == ""
        vl_page.click_on_log_row_by_path(rand_path)
        assert vl_page.check_log_footer_contains_correct_details(ip=ip)
        assert vl_page.check_log_footer_contains_correct_details(country=country_name)
        assert vl_page.check_log_footer_contains_correct_details(code=country_code)
        assert vl_page.check_log_footer_contains_correct_details(asn=current_asn)
        assert vl_page.check_log_footer_contains_correct_details(method="method")
        assert vl_page.check_log_footer_contains_correct_details(response="200")
        assert vl_page.check_log_footer_contains_correct_details(http="http")

    def test_log_popup_header_contains_correct_data(self):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        vl_page.query(self.data["vl_base_url"], f"/{rand_path}")
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.open_log_details_popup(rand_path)
        country_name, country_code = vl_page.get_my_country_name_and_code()
        assert vl_page.check_popup_header_contains_correct_data(country=country_name)
        assert vl_page.check_popup_header_contains_correct_data(code=country_code)
        assert vl_page.check_popup_header_contains_correct_data(method="method")
        assert vl_page.check_popup_header_contains_correct_data(response="200")
        assert vl_page.check_popup_header_contains_correct_data(http="http")
        assert vl_page.check_popup_header_contains_correct_data(txt="txt")
        assert vl_page.is_element_present(*ViewLogLocators.UPLOAD_BTN)
        assert vl_page.is_element_present(*ViewLogLocators.DOWNLOAD_BTN)

    def test_log_popup_heading_contains_correct_data(self):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        vl_page.query(self.data["vl_base_url"], f"/{rand_path}")
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.open_log_details_popup(rand_path)
        current_asn = vl_page.get_my_asn()
        if "Google" in current_asn:
            current_asn = current_asn.upper().replace("LLC", "").rstrip(" ")
        current_ip = vl_page.get_my_public_ip()
        assert vl_page.check_popup_heading_contains_correct_data(ip=current_ip)
        assert vl_page.check_popup_heading_contains_correct_data(asn=current_asn)
        assert vl_page.check_popup_heading_contains_correct_data(host=self.data["vl_base_url"])
        assert vl_page.check_popup_heading_contains_correct_data(path=rand_path)

    def test_log_popup_footer_first_row_contains_correct_data(self):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        vl_page.query(self.data["vl_base_url"], f"/{rand_path}")
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.open_log_details_popup(rand_path)
        country_name, country_code = vl_page.get_my_country_name_and_code()
        current_ip = vl_page.get_my_public_ip()
        assert len(vl_page.check_popup_footer_first_row_contains_correct_data(current_ip, country_name)) == 7
        assert all(vl_page.check_popup_footer_first_row_contains_correct_data(current_ip, country_name))

    def test_log_popup_footer_contains_correct_data(self):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        vl_page.query(self.data["vl_base_url"], f"/{rand_path}")
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.open_log_details_popup(rand_path)
        current_ip = vl_page.get_my_public_ip()
        assert all(vl_page.check_popup_footer_contains_correct_data(current_ip))
        assert len(vl_page.check_popup_footer_contains_correct_data(current_ip)) == 12

    def test_can_clear_filter_by_pressing_x(self):
        vl_page = ViewLogPage(self.driver)
        vl_page.navigate_to_view_log_page()
        vl_page.select_logs_site_by_name("vl.test")
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == \
               "domain_name: vl.test"
        vl_page.press_clear_filter_btn()
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == ""

    @pytest.mark.parametrize("param", ["cookies", "headers"])
    def test_can_send_args(self, param):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        rand_name = vl_page.generate_random_string(8)
        rand_val = vl_page.generate_random_string(8)
        vl_page.query(self.data["vl_base_url"], f"/{rand_path}", params={param: {rand_name: rand_val}})
        time.sleep(22)
        vl_page.select_logs_site_by_name("vl.test")
        assert vl_page.check_log_row_contains_correct_details(rand_path,
                                                              url=self.data[
                                                                  "vl_base_url"] + f"/{rand_path}" +
                                                                  f"?{param}={rand_name}")
        vl_page.click_on_log_row_by_path(rand_path)
        assert rand_path + f"?{param}={rand_name}" in vl_page.find_element(*ViewLogLocators.PANEL_BODY).text
        vl_page.click_on_view_details_btn()
        assert param in vl_page.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER).text
        assert rand_name in vl_page.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER).text

    @pytest.mark.parametrize("param", ["cookies", "headers"])
    def test_can_send_cookies_and_headers(self, param):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        rand_name = vl_page.generate_random_string(8)
        rand_val = vl_page.generate_random_string(8)
        if param == "cookies":
            cookies = {rand_name: rand_val}
            vl_page.query(self.data["vl_base_url"], f"/{rand_path}",  cookies=cookies)
        else:
            headers = {rand_name: rand_val}
            vl_page.query(self.data["vl_base_url"], f"/{rand_path}",  headers=headers)
        time.sleep(22)
        vl_page.select_logs_site_by_name("vl.test")
        assert vl_page.check_log_row_contains_correct_details(rand_path,
                                                              url=self.data["vl_base_url"] + f"/{rand_path}")
        vl_page.click_on_log_row_by_path(rand_path)
        assert f"/{rand_path}" == vl_page.find_element(*ViewLogLocators.PANEL_BODY).text
        vl_page.click_on_view_details_btn()
        assert rand_name in vl_page.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER).text
        assert rand_val in vl_page.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER).text
        if param == "cookies":
            assert "Cookies" in vl_page.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER).text
        else:
            assert "Headers" in vl_page.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER).text
        assert "GET" in vl_page.find_element(*ViewLogLocators.PANEL_FOOTER).text

    def test_can_search_by_url(self):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        for i in range(5):
            rand_path = vl_page.generate_random_string(8)
            vl_page.query(self.data["vl_base_url"], f"/{rand_path}")
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        vl_page.send_value_to_field_press_enter(f"url:/{rand_path}", *ViewLogLocators.DOMAIN_NAME_INPUT)
        time.sleep(3)
        assert len(vl_page.find_elements(*ViewLogLocators.LOG_SINGLE_LINE)) == 1
        assert vl_page.check_log_row_contains_correct_details(rand_path, url=self.data["vl_base_url"] + f"/{rand_path}")
        vl_page.click_on_log_row_by_path(rand_path)
        assert rand_path in vl_page.find_element(*ViewLogLocators.PANEL_BODY).text
        vl_page.click_on_view_details_btn()
        assert rand_path in vl_page.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_HEADING).text

    @pytest.mark.parametrize("param", ["cookies", "headers"])
    def test_can_search_by_arguments_cookies_headers(self, param):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        rand_name = vl_page.generate_random_string(8)
        rand_val = vl_page.generate_random_string(8)
        vl_page.query(self.data["vl_base_url"], f"/{rand_path}", params={param: {rand_name: rand_val}})
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        vl_page.send_value_to_field_press_enter(f"arg_{param}:{rand_name}", *ViewLogLocators.DOMAIN_NAME_INPUT)
        time.sleep(3)
        assert len(vl_page.find_elements(*ViewLogLocators.LOG_SINGLE_LINE)) == 1
        assert vl_page.check_log_row_contains_correct_details(rand_path,
                                                              url=self.data[
                                                                      "vl_base_url"] + f"/{rand_path}" +
                                                                  f"?{param}={rand_name}")
        vl_page.click_on_log_row_by_path(rand_path)
        assert rand_path + f"?{param}={rand_name}" in vl_page.find_element(*ViewLogLocators.PANEL_BODY).text
        vl_page.click_on_view_details_btn()
        assert param in vl_page.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER).text
        assert rand_name in vl_page.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER).text
        assert "Args", "cookies" in vl_page.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER).text

    @pytest.mark.parametrize("param", ["cookies", "headers"])
    def test_can_search_by_cookies_headers(self, param):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        rand_name = vl_page.generate_random_string(8)
        rand_val = vl_page.generate_random_string(8)
        if param == "cookies":
            cookies = {rand_name: rand_val}
            vl_page.query(self.data["vl_base_url"], f"/{rand_path}", method="POST", cookies=cookies)
        else:
            headers = {rand_name: rand_val}
            vl_page.query(self.data["vl_base_url"], f"/{rand_path}", method="POST", headers=headers)
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        vl_page.send_value_to_field_press_enter(f"{param.replace('s', '')}_{rand_name}:re(.+)", *ViewLogLocators.DOMAIN_NAME_INPUT)
        time.sleep(3)
        assert len(vl_page.find_elements(*ViewLogLocators.LOG_SINGLE_LINE)) == 1
        assert vl_page.check_log_row_contains_correct_details(rand_path,
                                                              url=self.data["vl_base_url"] + f"/{rand_path}")
        vl_page.click_on_log_row_by_path(rand_path)
        assert f"/{rand_path}" == vl_page.find_element(*ViewLogLocators.PANEL_BODY).text
        vl_page.click_on_view_details_btn()
        assert rand_name in vl_page.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER).text
        assert rand_val in vl_page.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER).text
        if param == "cookies":
            assert "Cookies" in vl_page.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER).text
        else:
            assert "Headers" in vl_page.find_element(*ViewLogLocators.REQUEST_DETAILS_POPUP_FOOTER).text
        assert "POST" in vl_page.find_element(*ViewLogLocators.PANEL_FOOTER).text

    def test_blocked_by_acl_request_details(self):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(7)
        vl_page.query(self.data["vl_base_url"], f"/block{rand_path}")
        time.sleep(20)
        ip = vl_page.get_my_public_ip()
        vl_page.select_logs_site_by_name("vl.test")
        assert vl_page.check_log_row_contains_correct_details(rand_path, ip=ip)
        assert vl_page.check_log_row_contains_correct_details(rand_path, url=self.data["vl_base_url"] + f"/block{rand_path}")
        assert vl_page.check_log_row_contains_correct_details(rand_path, date="date")
        assert vl_page.check_log_row_contains_correct_details(rand_path, method="method")
        assert vl_page.check_log_row_contains_correct_details(rand_path, response="403")
        vl_page.click_on_log_row_by_path(rand_path)
        country_name, country_code = vl_page.get_my_country_name_and_code()
        current_asn = vl_page.get_my_asn()
        if "Google" in current_asn:
            current_asn = current_asn.upper().replace("LLC", "").rstrip(" ")
        assert vl_page.check_log_footer_contains_correct_details(ip=ip)
        assert vl_page.check_log_footer_contains_correct_details(country=country_name)
        assert vl_page.check_log_footer_contains_correct_details(code=country_code)
        assert vl_page.check_log_footer_contains_correct_details(asn=current_asn)
        assert vl_page.check_log_footer_contains_correct_details(method="method")
        assert vl_page.check_log_footer_contains_correct_details(response="403")
        assert vl_page.check_log_footer_contains_correct_details(http="http")
        assert vl_page.check_log_footer_contains_correct_details(acl="acl-ip")
        vl_page.click_on_view_details_btn()
        assert vl_page.check_popup_heading_contains_correct_data(ip=ip)
        assert vl_page.check_popup_heading_contains_correct_data(asn=current_asn)
        assert vl_page.check_popup_heading_contains_correct_data(host=self.data["vl_base_url"])
        assert vl_page.check_popup_heading_contains_correct_data(path=rand_path)
        assert vl_page.check_popup_heading_contains_correct_data(acl="acl-ip")
        assert vl_page.check_popup_header_contains_correct_data_for_blocked_request(country=country_name)
        assert vl_page.check_popup_header_contains_correct_data_for_blocked_request(code=country_code)
        assert vl_page.check_popup_header_contains_correct_data_for_blocked_request(method="method")
        assert vl_page.check_popup_header_contains_correct_data_for_blocked_request(response="403")
        assert vl_page.check_popup_header_contains_correct_data_for_blocked_request(http="http")
        assert vl_page.check_popup_header_contains_correct_data_for_blocked_request(txt="txt")
        assert all(vl_page.check_popup_footer_first_row_contains_correct_data_for_blocked_request(
            ip, country_name))

    def test_can_search_for_blocked_403_request(self):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        vl_page.query(self.data["vl_base_url"], f"/block{rand_path}")
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        vl_page.send_value_to_field_press_enter("is:403",
                                                *ViewLogLocators.DOMAIN_NAME_INPUT)
        time.sleep(3)
        assert len(vl_page.find_elements(*ViewLogLocators.LOG_SINGLE_LINE)) > 0
        assert vl_page.check_log_row_contains_correct_details(f"/block{rand_path}", url=self.data["vl_base_url"] + f"/block{rand_path}")

    def test_blocked_by_rate_limit_request_details(self):
        vl_page = ViewLogPage(self.driver)
        rand_path = ''
        for i in range(3):
            rand_path = vl_page.generate_random_string(7)
            vl_page.query(self.data["vl_base_url"], f"/rate-limit{rand_path}")
        time.sleep(23)
        ip = vl_page.get_my_public_ip()
        vl_page.select_logs_site_by_name("vl.test")
        assert vl_page.check_log_row_contains_correct_details(f"/rate-limit{rand_path}", ip=ip)
        assert vl_page.check_log_row_contains_correct_details(f"/rate-limit{rand_path}", url=self.data["vl_base_url"] + f"/rate-limit{rand_path}")
        assert vl_page.check_log_row_contains_correct_details(f"/rate-limit{rand_path}", date="date")
        assert vl_page.check_log_row_contains_correct_details(f"/rate-limit{rand_path}", method="method")
        assert vl_page.check_log_row_contains_correct_details(f"/rate-limit{rand_path}", response="503")
        vl_page.click_on_log_row_by_path(rand_path)
        country_name, country_code = vl_page.get_my_country_name_and_code()
        current_asn = vl_page.get_my_asn()
        if "Google" in current_asn:
            current_asn = current_asn.upper().replace("LLC", "").rstrip(" ")
        assert vl_page.check_log_footer_contains_correct_details(ip=ip)
        assert vl_page.check_log_footer_contains_correct_details(country=country_name)
        assert vl_page.check_log_footer_contains_correct_details(code=country_code)
        assert vl_page.check_log_footer_contains_correct_details(asn=current_asn)
        assert vl_page.check_log_footer_contains_correct_details(method="method")
        assert vl_page.check_log_footer_contains_correct_details(response="503")
        assert vl_page.check_log_footer_contains_correct_details(http="http")
        assert vl_page.check_log_footer_contains_correct_details(rl="DO NOT DELETE, generic")
        vl_page.click_on_view_details_btn()
        assert vl_page.check_popup_heading_contains_correct_data(ip=ip)
        assert vl_page.check_popup_heading_contains_correct_data(asn=current_asn)
        assert vl_page.check_popup_heading_contains_correct_data(host=self.data["vl_base_url"])
        assert vl_page.check_popup_heading_contains_correct_data(path=rand_path)
        assert vl_page.check_popup_heading_contains_correct_data(rl="DO NOT DELETE, generic")
        assert vl_page.check_popup_header_contains_correct_data_for_blocked_request(country=country_name)
        assert vl_page.check_popup_header_contains_correct_data_for_blocked_request(code=country_code)
        assert vl_page.check_popup_header_contains_correct_data_for_blocked_request(method="method")
        assert vl_page.check_popup_header_contains_correct_data_for_blocked_request(response="503")
        assert vl_page.check_popup_header_contains_correct_data_for_blocked_request(http="http")
        assert vl_page.check_popup_header_contains_correct_data_for_blocked_request(txt="txt")

    def test_can_search_for_blocked_503_request(self):
        vl_page = ViewLogPage(self.driver)
        rand_path = ''
        for i in range(3):
            rand_path = vl_page.generate_random_string(7)
            vl_page.query(self.data["vl_base_url"], f"/rate-limit{rand_path}")
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        vl_page.send_value_to_field_press_enter("is:503",
                                                *ViewLogLocators.DOMAIN_NAME_INPUT)
        time.sleep(3)
        assert len(vl_page.find_elements(*ViewLogLocators.LOG_SINGLE_LINE)) > 0
        assert vl_page.check_log_row_contains_correct_details(f"/rate-limit{rand_path}",
                                                              url=self.data["vl_base_url"] + f"/rate-limit{rand_path}")

    @pytest.mark.parametrize("tag", ["403", "acl-ip", "GET", "ip"])
    def test_can_add_tag_to_search_bar_from_expanded_row_by_double_click(self, tag):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        ip = vl_page.get_my_public_ip()
        vl_page.query(self.data["vl_base_url"], f"/block{rand_path}")
        time.sleep(21)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == ''
        vl_page.click_on_log_row_by_path(rand_path)
        assert vl_page.check_log_footer_contains_correct_details(response="403")
        if tag == "ip":
            vl_page.double_click_on_tag_by_text(ip)
        else:
            vl_page.double_click_on_tag_by_text(tag)
        if tag == "403" or tag == "GET":
            assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == f"is: {tag}"
        if tag == "acl-ip":
            assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == f"reason: {tag}"
        if tag == "ip":
            assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == f"ip: {ip}"

    @pytest.mark.parametrize("tag", ["geo", "wafname", "wafid"])
    def test_can_add_tag_to_search_bar_from_log_popup(self, tag):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        country, code = vl_page.get_my_country_name_and_code()
        vl_page.query(self.data["vl_base_url"], f"/block{rand_path}")
        time.sleep(22)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == ''
        vl_page.click_on_log_row_by_path(rand_path)
        assert vl_page.check_log_footer_contains_correct_details(response="403")
        vl_page.click_on_view_details_btn()
        vl_page.double_click_on_tag_by_text_in_popup(tag)
        if tag == "geo":
            assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == f"tag: {tag}:{country.lower()}"
        if tag == "wafname":
            assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == f"tag: {tag}:waf-default"
        if tag == "wafid":
            assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == f"tag: {tag}:0001"

    @pytest.mark.parametrize("tag", ["accept-encoding", "connection", "x-forwarded-for"])
    def test_can_add_header_to_search_bar_from_log_popup(self, tag):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        vl_page.query(self.data["vl_base_url"], f"/block{rand_path}")
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == ''
        vl_page.click_on_log_row_by_path(rand_path)
        vl_page.click_on_view_details_btn()
        vl_page.double_click_on_header_row_by_text(tag)
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == f"header_{tag}: re(.+)"

    @pytest.mark.parametrize("tag", ["ip", "503"])
    def test_correct_filter_by_tag(self, tag):
        vl_page = ViewLogPage(self.driver)
        ip = vl_page.get_my_public_ip()
        rand_path = ''
        for i in range(3):
            rand_path = vl_page.generate_random_string(7)
            vl_page.query(self.data["vl_base_url"], f"/rate-limit{rand_path}")
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        vl_page.click_on_log_row_by_path(rand_path)
        assert vl_page.check_log_footer_contains_correct_details(response="503")
        if tag == "ip":
            vl_page.double_click_on_tag_by_text(ip)
        if tag == "503":
            vl_page.double_click_on_tag_by_text(tag)
        vl_page.press_search_btn()
        assert len(vl_page.get_all_log_lines()) >= 2
        if tag == "ip":
            assert vl_page.check_all_log_lines_have_specific_text(ip)
        if tag == "503":
            assert vl_page.check_all_log_lines_have_specific_text(tag)

    def test_can_filter_by_combination_of_tags(self):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        ip = vl_page.get_my_public_ip()
        vl_page.query(self.data["vl_base_url"], f"/block{rand_path}")
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == ''
        vl_page.click_on_log_row_by_path(rand_path)
        assert vl_page.check_log_footer_contains_correct_details(response="403")
        vl_page.double_click_on_tag_by_text("403")
        vl_page.double_click_on_tag_by_text(ip)
        vl_page.press_search_btn()
        assert len(vl_page.get_all_log_lines()) >= 1
        vl_page.check_all_log_lines_have_specific_text("403")
        vl_page.check_all_log_lines_have_specific_text(ip)

    @pytest.mark.parametrize("param", ["cookies", "headers"])
    def test_can_filter_by_headers_or_cookies(self, param):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        rand_name = vl_page.generate_random_string(8)
        rand_val = vl_page.generate_random_string(8)
        if param == "cookies":
            cookies = {rand_name: rand_val}
            vl_page.query(self.data["vl_base_url"], f"/{rand_path}", method="POST", cookies=cookies)
        else:
            headers = {rand_name: rand_val}
            vl_page.query(self.data["vl_base_url"], f"/{rand_path}", method="POST", headers=headers)
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        vl_page.open_log_details_popup(rand_path)
        vl_page.double_click_on_header_row_by_text(rand_name)
        vl_page.close_details_popup()
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == \
               f"{param.replace('s', '')}_{rand_name}: re(.+)"
        vl_page.press_search_btn()
        assert len(vl_page.get_all_log_lines()) == 1
        vl_page.check_all_log_lines_have_specific_text(rand_path)

    @pytest.mark.parametrize("param", ["cookies", "headers"])
    def test_search_history_saved(self, param):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        rand_name = vl_page.generate_random_string(8)
        rand_val = vl_page.generate_random_string(8)
        if param == "cookies":
            cookies = {rand_name: rand_val}
            vl_page.query(self.data["vl_base_url"], f"/{rand_path}", method="POST", cookies=cookies)
        else:
            headers = {rand_name: rand_val}
            vl_page.query(self.data["vl_base_url"], f"/{rand_path}", method="POST", headers=headers)
        time.sleep(22)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        vl_page.open_log_details_popup(rand_path)
        vl_page.double_click_on_header_row_by_text(rand_name)
        vl_page.close_details_popup()
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == \
               f"{param.replace('s', '')}_{rand_name}: re(.+)"
        vl_page.press_search_btn()
        assert len(vl_page.get_all_log_lines()) == 1
        vl_page.check_all_log_lines_have_specific_text(rand_path)
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == ''
        vl_page.open_search_history_popup()
        assert vl_page.is_element_present(*ViewLogLocators.SEARCH_HISTORY_LIST)
        vl_page.check_search_history_contains_desired_value(f"{param.replace('s', '')}_{rand_name}: re(.+)")

    @pytest.mark.parametrize("param", ["cookies", "headers"])
    def test_can_use_search_history(self, param):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        rand_name = vl_page.generate_random_string(8)
        rand_val = vl_page.generate_random_string(8)
        if param == "cookies":
            cookies = {rand_name: rand_val}
            vl_page.query(self.data["vl_base_url"], f"/{rand_path}", method="POST", cookies=cookies)
        else:
            headers = {rand_name: rand_val}
            vl_page.query(self.data["vl_base_url"], f"/{rand_path}", method="POST", headers=headers)
        time.sleep(22)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        vl_page.open_log_details_popup(rand_path)
        vl_page.double_click_on_header_row_by_text(rand_name)
        vl_page.close_details_popup()
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == \
               f"{param.replace('s', '')}_{rand_name}: re(.+)"
        vl_page.press_search_btn()
        assert len(vl_page.get_all_log_lines()) == 1
        vl_page.check_all_log_lines_have_specific_text(rand_path)
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == ''
        vl_page.open_search_history_popup()
        assert vl_page.is_element_present(*ViewLogLocators.SEARCH_HISTORY_LIST)
        vl_page.check_search_history_contains_desired_value(f"{param.replace('s', '')}_{rand_name}: re(.+)")
        vl_page.click_history_row_by_value(f"{param.replace('s', '')}_{rand_name}: re(.+)")
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == \
               f"{param.replace('s', '')}_{rand_name}: re(.+)"
        vl_page.press_search_btn()
        assert len(vl_page.get_all_log_lines()) == 1
        vl_page.check_all_log_lines_have_specific_text(rand_path)

    def test_can_open_close_help_window_window_has_text(self):
        vl_page = ViewLogPage(self.driver)
        vl_page.select_logs_site_by_name("vl.test")
        assert vl_page.find_element(*ViewLogLocators.HELP_POPUP).is_displayed() is False
        vl_page.press_help_btn()
        assert vl_page.find_element(*ViewLogLocators.HELP_POPUP).is_displayed()
        popup = vl_page.find_element(*ViewLogLocators.HELP_POPUP)
        assert "after" and "asn" and "before" and "status" and "The chosen SSL protocol." and "Methods:" in popup.text
        vl_page.find_element(*ViewLogLocators.HELP_POPUP_CLOSE_BTN).click()
        assert vl_page.find_element(*ViewLogLocators.HELP_POPUP).is_displayed() is False

    def test_can_open_close_calendar(self):
        vl_page = ViewLogPage(self.driver)
        vl_page.select_logs_site_by_name("vl.test")
        assert vl_page.is_element_present(*ViewLogLocators.DATE_RANGE_INPUT)
        assert vl_page.find_element(*ViewLogLocators.DATE_RANGE_INPUT).is_displayed() is False
        vl_page.press_calendar_btn()
        assert vl_page.find_element(*ViewLogLocators.DATE_RANGE_INPUT).is_displayed()
        vl_page.press_open_calendar_button()
        assert vl_page.find_element(*ViewLogLocators.CALENDAR).is_displayed()
        vl_page.press_calendar_cancel_button()
        assert vl_page.find_element(*ViewLogLocators.CALENDAR).is_displayed() is False
        assert vl_page.find_element(*ViewLogLocators.DATE_RANGE_INPUT).is_displayed() is False

    @pytest.mark.parametrize("time_range, gap", [("Last 24 Hours", 24), ("Last 12 Hours", 12), ("Last 4 Hours", 4),
                                                 ("Last 2 Hours", 2), ("Last Hour", 1)])
    def test_can_search_by_dates(self, time_range, gap):
        vl_page = ViewLogPage(self.driver)
        rand_path = vl_page.generate_random_string(8)
        vl_page.query(self.data["vl_base_url"], f"/block{rand_path}")
        time.sleep(20)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        vl_page.press_calendar_btn()
        today = vl_page.format_date_to_string()
        today_minus_gap = vl_page.return_time_with_delta(gap)
        vl_page.press_open_calendar_button()
        vl_page.find_element(By.XPATH, f"//li[contains(text(), '{time_range}')]").click()
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == \
            f"after: {today_minus_gap}, before: {today}"
        vl_page.press_search_btn()
        assert len(vl_page.get_all_log_lines()) >= 1

    def test_can_add_random_dates_to_search_from_calendar(self):
        vl_page = ViewLogPage(self.driver)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        vl_page.press_calendar_btn()
        vl_page.press_open_calendar_button()
        vl_page.fill_calendar_from_to_fields(6, 3)
        assert vl_page.check_custom_range_active()
        assert vl_page.check_from_to_dates_are_active(6, 3)
        vl_page.press_calendar_apply_btn()
        start = vl_page.return_day_time_with_delta_in_search_bar_format_zero_seconds(6)
        end = vl_page.return_day_time_with_delta_in_search_bar_format_zero_seconds(3)
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == \
               f"after: {start}, before: {end}"

    def test_can_filter_by_random_date(self):
        vl_page = ViewLogPage(self.driver)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        vl_page.press_calendar_btn()
        vl_page.press_open_calendar_button()
        vl_page.fill_calendar_from_to_fields(4, 3)
        vl_page.press_calendar_apply_btn()
        start = vl_page.return_day_time_with_delta_in_search_bar_format_zero_seconds(4)
        end = vl_page.return_day_time_with_delta_in_search_bar_format_zero_seconds(3)
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == \
               f"after: {start}, before: {end}"
        vl_page.press_search_btn()
        dates = vl_page.extract_date_from_log_rows()
        start_for_range = vl_page.return_day_time_with_delta_for_comparing_range(4)
        end_for_range = vl_page.return_day_time_with_delta_for_comparing_range(3)
        assert all(vl_page.check_dates_in_range(dates, start_for_range, end_for_range))

    def test_log_rows_count_default_settings(self):
        vl_page = ViewLogPage(self.driver)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        vl_page.press_calendar_btn()
        vl_page.press_open_calendar_button()
        vl_page.fill_calendar_from_to_fields(6, 1)
        vl_page.press_calendar_apply_btn()
        start = vl_page.return_day_time_with_delta_in_search_bar_format_zero_seconds(6)
        end = vl_page.return_day_time_with_delta_in_search_bar_format_zero_seconds(1)
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == \
               f"after: {start}, before: {end}"
        vl_page.press_search_btn()
        assert vl_page.find_element(*ViewLogLocators.ROWS_LIMIT_DROPDOWN).text == "Last 200 Events"
        rows_on_page = vl_page.return_length_of_log_rows()
        displayed_rows_num = vl_page.return_displayed_length_of_log_rows()
        assert rows_on_page == displayed_rows_num and rows_on_page <= 200

    def test_500_log_rows_count_default_settings(self):
        vl_page = ViewLogPage(self.driver)
        vl_page.select_logs_site_by_name("vl.test")
        vl_page.clear_field(*ViewLogLocators.DOMAIN_NAME_INPUT)
        vl_page.press_calendar_btn()
        vl_page.press_open_calendar_button()
        vl_page.fill_calendar_from_to_fields(6, 1)
        vl_page.press_calendar_apply_btn()
        start = vl_page.return_day_time_with_delta_in_search_bar_format_zero_seconds(6)
        end = vl_page.return_day_time_with_delta_in_search_bar_format_zero_seconds(1)
        assert vl_page.find_element(*ViewLogLocators.DOMAIN_NAME_INPUT).get_attribute("value") == \
               f"after: {start}, before: {end}"
        vl_page.press_search_btn()
        vl_page.press_limit_dropdown()
        vl_page.select_limit_max_by_text("Last 500 Events")
        assert vl_page.find_element(*ViewLogLocators.ROWS_LIMIT_DROPDOWN).text == "Last 500 Events"
        rows_on_page = vl_page.return_length_of_log_rows()
        displayed_rows_num = vl_page.return_displayed_length_of_log_rows()
        assert rows_on_page == displayed_rows_num and 200 <= rows_on_page <= 500










