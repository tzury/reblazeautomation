import time

from core.base_page import *
from core.cleanup import *
from core.login_data_vars import *
from locators.dashboard_locators import *
from selenium.webdriver.support import expected_conditions


class LoginPage(BasePage):
    def wait_for_form_fields(self, user, pswd, otp, login):
        ignored_exceptions = (NoSuchElementException, StaleElementReferenceException, TimeoutException)
        user = WebDriverWait(self.driver, 10, ignored_exceptions=ignored_exceptions) \
            .until(expected_conditions.presence_of_element_located(user))
        pswd = WebDriverWait(self.driver, 10, ignored_exceptions=ignored_exceptions) \
            .until(expected_conditions.presence_of_element_located(pswd))
        otp_f = WebDriverWait(self.driver, 10, ignored_exceptions=ignored_exceptions) \
            .until(expected_conditions.presence_of_element_located(otp))
        login = WebDriverWait(self.driver, 10, ignored_exceptions=ignored_exceptions) \
            .until(expected_conditions.presence_of_element_located(login))

    # sends data to fields, logins and validates correct login
    # if login fails tries again
    def login_on_startup(self, url, user_name=None, password=None, seed=None, otp='', sleep=3.0, clean=None):
        success = False
        self.driver.get(url)
        time.sleep(0.5)
        while success is False:
            user_field = (By.NAME, "email")
            pass_field = (By.NAME, "password")
            otp_field = (By.NAME, "reblazekeyotp")
            login_loc = (By.XPATH, "//*[@id='login-form']//button")
            loader = (By.XPATH, "//div[contains(@class, 'loader is-loading')]")
            self.wait_for_form_fields(user_field, pass_field, otp_field, login_loc)
            self.send_value_to_field(user_name, *user_field)
            self.send_value_to_field(password, *pass_field)
            if otp == '':
                otp = self.get_otp(str(seed))
            else:
                otp = otp
            self.send_value_to_field(otp, *otp_field)
            login_btn = self.find_element(*login_loc)
            time.sleep(0.3)
            self.explicitly_wait_for_element_to_be_clickable(20, login_btn)
            login_btn.click()
            time.sleep(0.1)
            for i in range(30):
                if self.is_element_present_no_waits(*loader):
                    time.sleep(0.5)
                break
            time.sleep(1)
            if "signin" in self.get_current_url():
                print("TRYING TO RELOGIN")
                self.wait_for_form_fields(user_field, pass_field, otp_field, login_loc)
                # if self.is_element_present(*loader) is False or self.find_element(*DashboardLocators.LOGIN_ERROR_MSG).is_displayed():
                success = False
                time.sleep(1)
                otp = self.get_otp(str(seed))
                self.clear_field(*user_field)
                self.clear_field(*pass_field)
                self.clear_field(*otp_field)
            else:
                success = True
            time.sleep(sleep)
        login_loc = (By.XPATH, "//*[@id='login-form']//button")
        if clean is not None and self.is_element_present(*login_loc) is False:
            if len(self.get_rate_limit_ids()) > 0:
                clean = Cleanup(self.driver)
                clean.delete_all_rate_limit_rules()

    # this function is used in login tests
    def login_for_testing(self, url, user_name=None, password=None, seed=None, otp='', sleep=3.0):
        self.driver.get(url)
        user_field = (By.NAME, "email")
        pass_field = (By.NAME, "password")
        otp_field = (By.NAME, "reblazekeyotp")
        login_loc = (By.XPATH, "//*[@id='login-form']//button")
        self.explicitly_wait_for_element(60, *user_field)
        self.send_value_to_field(user_name, *user_field)
        self.send_value_to_field(password, *pass_field)
        if otp == '':
            otp = self.get_otp(str(seed))
        else:
            otp = otp
        self.send_value_to_field(otp, *otp_field)
        login_btn = self.find_element(*login_loc)
        time.sleep(0.3)
        login_btn.click()
        time.sleep(0.2)
        time.sleep(sleep)

    # this login is used without a fixture to check invalid data
    def login_with_diff_data(self,
                             url="https://rbzauto1.dev.app.reblaze.io/signin",
                             user_name=get_login_data("rl", "rl_user"),
                             password=get_login_data("rl", "rl_pass"), otp=''):
        self.login_for_testing(url, user_name, password, get_login_data("rl", "otp_seed"), otp, sleep=0.2)

    def login_unauth_user(self,
                          url="https://rbzauto2.dev.app.reblaze.io/signin",
                          user_name=get_login_data("unauth_user", "auto1_user"),
                          password=get_login_data("unauth_user", "auto1_pass"), otp=''):
        self.login_for_testing(url, user_name, password, get_login_data("unauth_user", "otp_seed"), otp, sleep=0.2)
